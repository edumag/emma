CREATE TABLE monitor_test
            (
            id INTEGER PRIMARY KEY,
            mon_precios text,
            mon_tendencias text,
            mon_volumenes text,
            mon_adx text,
            mon_macd text
            );
CREATE TABLE estrategias
            (
            id INTEGER PRIMARY KEY,
            name text,
            mark text,
            descripcion text,
            type string default "SPOT",
            `leverage` int default 1,
            temporalidad string default "1h"
            );
CREATE TABLE senyales
            (
            id INTEGER PRIMARY KEY,
            eid int,
            accion text,
            condicion text
            );
CREATE TABLE locks
            (
            app text,
            date text,
            lock int,
            UNIQUE (app)
            );
CREATE TABLE market
            (
            id INTEGER PRIMARY KEY,
            date text,
            symbol text,
            price decimal(10,5),
            status text,
            mon_precios text,
            mon_tendencias text,
            mon_volumenes text,
            mon_adx text,
            mon_macd text
            );
            CREATE TABLE config_general
            (
              id INTEGER PRIMARY KEY,
              date text,
              compras_activadas int default 1,
              ventas_activadas int default 1,
              estrategias_activadas text default 'M',
              max_trades int DEFAULT 5,
              max_losses int DEFAULT 2,
              descartar_ventas text DEFAULT '');
CREATE TABLE IF NOT EXISTS "config" (
	`id`	INTEGER,
	`eid`	int,
	`date`	text,
	`compras_activadas`	int,
	`ventas_activadas`	int,
	`min_profit_sell`	real,
	`stoploss`	real,
	`follow_losses`	real,
	`stoptrailing`	real,
	`amount_buy`	real,
	`max_trades`	int,
	`max_losses`	int,
	`sell_with_loss`	int DEFAULT 1,
	PRIMARY KEY(`id`)
);
CREATE TABLE IF NOT EXISTS "futures" (
	`id`	INTEGER,
	`date`	text NOT NULL,
	`symbol`	text NOT NULL,
	`amount`	decimal ( 10 , 5 ) NOT NULL,
	`price`	decimal ( 10 , 5 ) NOT NULL,
	`side`	text,
	`info`	text,
	`max_profit`	real,
	`min_profit`	real,
	`date_sell`	text,
	`profit`	decimal ( 10 , 5 ),
	`info_sell`	text,
	`stoploss`	int,
	`stoptrailing`	text,
  `leverage` int default 1,
	PRIMARY KEY(`id`)
);
CREATE VIEW trade_result AS
  SELECT t.*, e.mark,
    (strftime("%s", date_sell) - strftime("%s", date)) / 60 as minutes,
    round((t.amount * t.price) / t.leverage, 2) as amountd,
    round((t.amount * t.price_sell) / t.leverage, 2) as amountd_sell,
    round(((t.amount * t.price) * t.profit) / 100, 2) as resultd
  FROM trades t, estrategias e
  WHERE t.eid = e.id
/* trade_result(id,eid,date,symbol,amount,price,info,status,mon_precios,mon_tendencias,mon_volumenes,mon_adx,mon_macd,max_profit,min_profit,date_sell,price_sell,info_sell,status_sell,mon_precios_sell,mon_tendencias_sell,mon_volumenes_sell,mon_adx_sell,mon_macd_sell,stoploss,stoptrailing,status_market,lock,hide,type,mark,profit,minutes,amountd,amountd_sell,resultd) */;
CREATE VIEW test_trade_result AS
  SELECT t.*, e.mark,
    (strftime("%s", date_sell) - strftime("%s", date)) / 60 as minutes,
    round((t.amount * t.price) / t.leverage, 2) as amountd,
    round((t.amount * t.price_sell) / t.leverage, 2) as amountd_sell,
    round(((t.amount * t.price) * t.profit) / 100, 2) as resultd
  FROM test_trades t, estrategias e
  WHERE t.eid = e.id
/* test_trade_result(id,eid,date,symbol,amount,price,info,status,mon_precios,mon_tendencias,mon_volumenes,mon_adx,mon_macd,max_profit,min_profit,date_sell,price_sell,info_sell,status_sell,mon_precios_sell,mon_tendencias_sell,mon_volumenes_sell,mon_adx_sell,mon_macd_sell,stoploss,stoptrailing,status_market,lock,hide,type,mark,profit,minutes,amountd,amountd_sell,resultd) */;
CREATE TABLE IF NOT EXISTS "trades" (
	`id`	INTEGER,
	`eid`	INTEGER DEFAULT 1,
	`date`	text NOT NULL,
	`symbol`	text NOT NULL,
	`amount`	decimal ( 10 , 5 ) NOT NULL,
	`price`	decimal ( 10 , 5 ) NOT NULL,
	`info`	text,
	`status`	text,
	`mon_precios`	text,
	`mon_tendencias`	text,
	`mon_volumenes`	text,
	`mon_adx`	text,
	`mon_macd`	text,
	`max_profit`	real,
	`min_profit`	real,
	`date_sell`	text,
	`price_sell`	decimal ( 10 , 5 ),
	`profit`	decimal ( 10 , 5 ),
	`info_sell`	text,
	`status_sell`	text,
	`mon_precios_sell`	text,
	`mon_tendencias_sell`	text,
	`mon_volumenes_sell`	text,
	`mon_adx_sell`	text,
	`mon_macd_sell`	text,
	`stoploss`	int,
	`stoptrailing`	text, status_market string DEFAULT '0', lock int DEFAULT 0, hide int DEFAULT 0, type string DEFAULT 'SPOT',
  `leverage` int default 1,
	PRIMARY KEY(`id`)
);
CREATE TABLE IF NOT EXISTS "test_trades" (
	`id`	INTEGER,
	`eid`	INTEGER DEFAULT 1,
	`date`	text NOT NULL,
	`symbol`	text NOT NULL,
	`amount`	decimal ( 10 , 5 ) NOT NULL,
	`price`	decimal ( 10 , 5 ) NOT NULL,
	`info`	text,
	`status`	text,
	`mon_precios`	text,
	`mon_tendencias`	text,
	`mon_volumenes`	text,
	`mon_adx`	text,
	`mon_macd`	text,
	`max_profit`	real,
	`min_profit`	real,
	`date_sell`	text,
	`price_sell`	decimal ( 10 , 5 ),
	`profit`	decimal ( 10 , 5 ),
	`info_sell`	text,
	`status_sell`	text,
	`mon_precios_sell`	text,
	`mon_tendencias_sell`	text,
	`mon_volumenes_sell`	text,
	`mon_adx_sell`	text,
	`mon_macd_sell`	text,
	`stoploss`	int,
	`stoptrailing`	text, status_market string DEFAULT '0', lock int DEFAULT 0, hide int DEFAULT 0, type string DEFAULT 'SPOT',
  `leverage` int default 1,
	PRIMARY KEY(`id`)
);
