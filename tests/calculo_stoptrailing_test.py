#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## @file
## @brief Test sobre calculo_stoptrailing.
##
## @todo No utilizar db de producción.

import sys
sys.path.append('src/')

from base import *

def test_new_calculo_stoptrailing():

    stoptrailings = [
        "1;1;0.2,2;2;3,3;3;4,10;1;20",
        "0;1;0,10;1;20",
        "0;2;0,3;3;5,10;1;12",
        "0;0.5;0",
        "0;2;0.5,3;3;6,6;4;10,10;6;90"
    ]
    profits = [
        -0.3,   # 01
        0.1,    # 02
        -4.5,   # 03
        0.1,    # 04
        1,      # 05
        -1,     # 06
        1.5,    # 07
        -1.5,   # 08
        2.3,    # 09
        4.5,    # 10
        6.5,    # 11
        8.5,    # 12
        12.2,   # 13
        3,      # 14
        1,      # 15
        -1,     # 16
        30      # 17
    ]
    results = [
        False,  # 01 01
        False,  # 01 02
        False,  # 01 03
        False,  # 01 04
        0,      # 01 05
        0,      # 01 06
        0.2,    # 01 07
        0.2,    # 01 08
        0.3,    # 01 09
        1.5,    # 01 10
        3.5,    # 01 11
        4,      # 01 12
        11.2,   # 01 13
        11.2,   # 01 14
        11.2,   # 01 15
        11.2,   # 01 16
        20,     # 01 17
        False,  # 02 01
        -0.9,   # 02 02
        -0.9,   # 02 03
        -0.9,   # 02 04
        0.0,    # 02 05
        0.0,    # 02 06
        0.0,    # 02 07
        0.0,    # 02 08
        0.0,    # 02 09
        0.0,    # 02 10
        0.0,    # 02 11
        0.0,    # 02 12
        11.2,   # 02 13
        11.2,   # 02 14
        11.2,   # 02 15
        11.2,   # 02 16
        20,     # 02 17
        False,  # 03 01
        -1.9,   # 03 02
        -1.9,   # 03 03
        -1.9,   # 03 04
        -1.0,   # 03 05
        -1.0,   # 03 06
        -0.5,   # 03 07
        -0.5,   # 03 08
        0.0,    # 03 09
        1.5,    # 03 10
        3.5,    # 03 11
        5.0,    # 03 12
        11.2,   # 03 13
        11.2,   # 03 14
        11.2,   # 03 15
        11.2,   # 03 16
        12,     # 03 17
        False,  # 04 01
        -0.4,   # 04 02
        -0.4,   # 04 03
        -0.4,   # 04 04
        0.0,    # 04 05
        0.0,    # 04 06
        0.0,    # 04 07
        0.0,    # 04 08
        0.0,    # 04 09
        0.0,    # 04 10
        0.0,    # 04 11
        0.0,    # 04 12
        0.0,    # 04 13
        0.0,    # 01 14
        0.0,    # 04 15
        0.0,    # 04 16
        0.0,    # 04 17
        False,  # 05 01
        -1.9,   # 05 02
        -1.9,   # 05 03
        -1.9,   # 05 04
        -1,     # 05 05
        -1,     # 05 06
        -0.5,   # 05 07
        -0.5,   # 05 08
        0.3,    # 05 09
        1.5,    # 05 10
        2.5,    # 05 11
        4.5,    # 05 12
        6.2,    # 05 13
        6.2,    # 05 14
        6.2,    # 05 15
        6.2,    # 05 16
        24,     # 05 17
    ]

    count_results = 0
    for stoptrailing in stoptrailings:
        print("\n")
        stoploss_actual = False
        for profit in profits:
            stoploss_anterior = stoploss_actual
            stoploss_actual = calculo_stoptrailing(stoptrailing, profit, stoploss_actual)
            if stoploss_actual is not False:
                stoploss_actual = round(stoploss_actual, 2)
            print("StopTrailing: {:<16} Profit: {:<4}  StopLossAnterior: {:<4}  StopLossActual: {}".format(stoptrailing, profit, str(stoploss_anterior), str(stoploss_actual)))
            assert results[count_results] == stoploss_actual
            count_results += 1
    assert True
