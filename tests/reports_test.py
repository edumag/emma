#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## @file
## @brief Test sobre reports.
##
## @todo No utilizar db de producción.

import sys
sys.path.append('src/')

from reports import *
from db import *
import pytest

@pytest.fixture
def settings():
    assert db_insert_buy('TESTCOIN', 1, 50000, 'SPOT', 1, False, None, 0, 'PyTest')
    sell = db_insert_sell('TESTCOIN', 51000, 'SELL T', '0 0 0 0 0', 'pytest')
    db_insert_market('TESTCOIN', 50000, 'TEST', '0 0 0 0 0')
    id = sell[0]
    assert int(id) > 0

    # A test function will be run at this point.
    yield

    # Code that will run after your test.
    assert db_delete_trades(str(id))

def test_report_trades(settings):
    rows = db_trades('all', False, False, False, True, False)
    if rows:
        print(rows[-1]['info'])
        print(rows[-1]['info_sell'])
        assert 'pytest' in rows[-1]['info_sell']

def test_show_symbol():
    res = show_symbol('all', False, False, False, False, False, False, False)
    assert len(res) > 0
