#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## @file
## @brief Test sobre de profit.

import sys
sys.path.append('src/')

from base import *

def test_get_profit():

    ## @var Type, buy price, last price, result
    results = [
        ['SHORT', 100, 105,  -5],
        ['LONG',  100,  80, -20],
        ['LONG',  100, 110,  10],
        ['SPOT',  100, 110,  10],
        ['LONG',  100,  90, -10],
        ['SHORT', 100, 110, -10],
        ['SHORT', 100,  70,  30],
        ['SHORT', 102.69,  103,  -0.3018794429837328],
    ]
    for r in results:
        print("Type: {} BuyPrice: {} LastPrice: {} Result: {}". format(
            r[0], r[1], r[2], r[3]
            )
        )
        result = getProfit(r[0], r[2], r[1])
        assert r[3] == result[0]
