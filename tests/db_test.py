#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## @file
## @brief Test sobre db.
##
## @todo No utilizar db de producción.

import sys
sys.path.append('src/')

from db import *

def test_insert_buy():
    try:
        sell = db_insert_sell('TESTCOIN', 51000, 'SELL T', '0 0 0 0 0', 'pytest')
    except:
        print('Limpieza realizada')
    assert db_insert_buy('TESTCOIN', 1, 50000, 'SPOT')
    is_buy_coin = is_buy('TESTCOIN')
    assert is_buy_coin
    is_buy_coin = is_buy('TESTCOIN', False, 1)
    assert is_buy_coin
    is_buy_coin = is_buy('TESTCOIN', True, False)
    assert is_buy_coin == False
    sell = db_insert_sell('TESTCOIN', 51000, 'SELL T', '0 0 0 0 0', 'pytest')
    print('Sell: {}'.format(sell))
    id = sell[0]
    print('ID: ' + str(id))  # @DEV
    assert sell
    assert db_delete_trades(str(id))

