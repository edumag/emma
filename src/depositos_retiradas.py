#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
La función fetch_withdrawals con kraken no  nos da permiso a no ser que tengamos permisos
para realizar retiradas desde la API, lo cual sería un fallo de seguridad importante.
Descartamos el código hasta que kraken lo solucione.
"""
import os
import sys
import ccxt.async_support as ccxt  # noqa: E402
import argparse

sys.path.append('./src')

from console import *
from precio_compra import precio_compra

import asyncio

__version__ = '0.1.0'
__author__ = u'edu@lesolivex.com'

salida = ''

async def fetch_withdrawals(exchange):

    global CURRENCIES_DEPOSITS
    global withdrawals

    for code in CURRENCIES_DEPOSITS:
        if exchange.id == 'kraken':
            withdrawals = await exchange.fetch_ledger(code, None, 20)
            print(withdrawals)
        elif exchange.has['fetchWithdrawals']:
            withdrawals = await exchange.fetch_withdrawals(code, None, 20)
            # print("\n## CURRENCY: ", code)
            for x in range(withdrawals.__len__()):
                d = withdrawals[x-1]
                if code == 'USDT':
                    precio = 1
                elif code == 'EUR':
                    precio = 1.2  # @todo Corregir.
                else:
                    precio = await precio_compra(exchange, code + '/USDT', d['datetime'])

                # printf("\n- %s: %.2f$ %s (Amount: %s, Precio: %s)", code, d['amount'] * precio, d['datetime'], d['amount'], precio )
                withdrawals = withdrawals + (d['amount'] * precio)

        else:
            raise Exception (exchange.id + ' does not have the fetch_withdrawals method')

    return withdrawals

async def fetch_deposits(exchange):

    global CURRENCIES_DEPOSITS

    for code in CURRENCIES_DEPOSITS:
        if exchange.has['fetchDeposits']:
            deposits = await exchange.fetch_deposits(code, None, 20)
            # print("\n## CURRENCY: ", code)
            for x in range(deposits.__len__()):
                d = deposits[x-1]
                if code == 'USDT':
                    precio = 1
                elif code == 'EUR':
                    precio = 1.2  # @todo Corregir.
                else:
                    precio = await precio_compra(exchange, code + '/USDT', d['datetime'])

                # printf("\n- %s: %.2f$ %s (Amount: %s, Precio: %s)", code, d['amount'] * precio, d['datetime'], d['amount'], precio )

        else:
            raise Exception (exchange.id + ' does not have the fetch_deposits method')

    return deposits
