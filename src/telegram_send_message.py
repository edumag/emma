#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## @file balances.py
## @brief Listado del balance.

__version__ = '0.1.0'
__author__ = u'edu@lesolivex.com'

import sys

sys.path.append('./src')

from base import *  # pylint: disable=W0401

import telegram

def escape_telegrambot_underscore(txt):
    return txt.replace("_", "\\_").replace(".", "\\.").replace("-", "\\-").replace('(', '\(').replace(')', '\)').replace('#', '\#').replace('+', '\+').replace('=', '\=').replace('}', '\}').replace('{', '\{').replace("<", "\<").replace(">", "\>").replace("[", "\[").replace("]", "\]")

def tlg_send(msg):

    if not 'TELEGRAM_TOKEN' in globals() or not TELEGRAM_TOKEN:
        logger.info(msg + '\nSin telegram no enviamos')
        return False

    bot = telegram.Bot(token=TELEGRAM_TOKEN)
    msg = escape_telegrambot_underscore(msg)
    bot.sendMessage(chat_id=TELEGRAM_CHAT_ID, text=msg, parse_mode=telegram.ParseMode.MARKDOWN_V2)
