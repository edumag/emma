#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## @file open_orders.py
## @brief Mostrar las operaciones activas.
##
## Recogemos la lista de los balances y buscamos en el historial el último
## precio al que compramos esa moneda.
## Mostramos la cantidad actual en dolares, fecha de compra, precio de compra
## y el profit actual.

__version__ = '0.1.0'
__author__ = u'edu@lesolivex.com'

import os
import sys
import argparse
import ccxt.async_support as ccxt  # noqa: E402
import asyncio
import console
import pprint
import traceback

sys.path.append('./src')
from base import *
from console import *
from tendencia import *
from binance_calls import *
from telegram_send_message import *
from datetime import datetime
from signal import signal, SIGINT

availableBalance = 0
total_profit = 0
total_result = 0

datos_pantalla_futures= {}

STOPTRAILING = "5;2;7"
STOPLOSS = -30
TAKEPROFIT = 30
STOPS_SEGUN_APALANCAMIENTO = {
    5:  [-4.5, 30, "5;2;7"  ],
    10: [-9,   30, "3;0.5;2"],
    20: [-18,  30, "2;1;1"  ]
    }

def barra_informativa_futures():

    global availableBalance, total_result, total_profit

    if total_profit > 0:
        total_profit_str = green(str(round(total_profit, 2)) + '%')
    else:
        total_profit_str = red(str(round(total_profit, 2)) + '%')

    if total_result > 0:
        total_result_str = green(str(round(total_result, 2)) + '$')
    else:
        total_result_str = red(str(round(total_result, 2)) + '$')

    salida = "\nAvailable Balance[{}] TotalResult[{}] TotalProfit[{}]".format(
        str(round(float(availableBalance), 2)) + '$',
        total_result_str,
        total_profit_str
        )

    print(salida)

def pantalla_futures(symbol=False):

    global datos_pantalla_futures

    os.system('clear')

    if FORMATO_SALIDA != 'telegram':
        format_header = "Id    Symbol [Ap]  Type  Amount$ Amount  Precio Tenden Volume ADX    MACD   LastPrice Status   Profit  Result ePrice     Date        TakePr StopLs  StopTrainig"
        print(format_header)

    for s in datos_pantalla_futures:
        contenido = "{:<243}".format(datos_pantalla_futures[s])
        if symbol and symbol.replace('/', '') == s:
            print(bold("{:<56}".format(contenido)), end='')
        else:
            print("{:<56}".format(contenido), end='')
        print("")

    barra_informativa_futures()

async def show_positions(positions):

    global datos_pantalla_futures_futures, STOPLOSS, STOPTRAILING
    global total_result, total_profit

    signal(SIGINT, end)

    config_general = get_config_general()
    default_strategy = config_general['estrategias_activadas'].split()[0]

    if FORMATO_SALIDA == 'telegram':
        format_contenido = """ID:{:<5}
Symbol:{:8}[{:2}]
Type:{:<5}
Amount$:{:<7}
Amount:{:<7}
Monitor:{:<340}
Profit:{:>16}
Result:{:>16}
Precio entrada:{:<10}
Fecha:{:<11}
Distancia profit:{:<7}
Distancia stoploss:{:<7}
Stoptrailing:{}{}"""
    else:
        format_contenido = "{:<5} {:8}[{:2}] {:<5} {:<7} {:<7} {:<340} {:>16} {:>16} {:<10} {:<11} {:<7} {:<7}{}{}"

    total_result_global = 0
    total_profit_global = 0
    for item in positions:

        logger.debug(item)
        amount = abs(float(item['positionAmt']))

        symbol = item['symbol'].replace('USDT', '/USDT')
        result_tendencia = await tendencia(symbol, default_strategy)
        status_mark, monitor_color, monitor, ultimo_precio = result_tendencia

        if not ultimo_precio:
            logger.error("Último precio no valido")
            continue

        if float(item['positionAmt']) > 0:
            side = 'LONG'
        else:
            side = 'SHORT'

        logger.debug('SIDE: ' + side)

        result = round(float(item['unrealizedProfit']), 2)
        total_result_global += result
        if result > 0:
            result_str = green(str(result) + '$')
        else:
            result_str = red(str(result) + '$')

        profit_actual, profit_str = getProfit(side, ultimo_precio, item['entryPrice'])

        total_profit_global += profit_actual

        date = datetime.fromtimestamp(int(item['updateTime']) / 1000)
        date = date.strftime("%d/%m %H:%M")

        # Insertar/Actualizar info en DB.
        info = info_db_futures(symbol, item['entryPrice'], amount, side, ultimo_precio, item['leverage'])
        id = info['id']
        stop_custom = info['stoploss']
        stoptrailing_custom = info['stoptrailing']

        # Ordenes pendientes en plataforma.
        if 'take_profit_market' in item:
            takeprofit_price = item['take_profit_market']
            take_profit_distance = round((ultimo_precio * 100) / float(takeprofit_price) - 100, 2)
        else:
            take_profit_distance = round(TAKEPROFIT - profit_actual, 2)

        if 'stop_market' in item:
            stop_market_price = item['stop_market']
            stop_loss_distance = str(round(((ultimo_precio * 100) / float(stop_market_price)) - 100, 2)) + '%'
        else:
            stop_loss_distance = round(stop_custom - profit_actual, 2)

        # Cerramos posición si no estamos en vista telegram.
        if FORMATO_SALIDA == 'terminal' and ultimo_precio and ultimo_precio > 0:
            if stop_custom != 0 and stop_custom > profit_actual:
                msg = "[StopLoss][{}] Cerramos posición en futuros".format(symbol)
                print(msg)
                logger.info(msg)
                await close_position(symbol.replace('/USDT', ''), 'StopLoss')
            if profit_actual >= TAKEPROFIT:
                msg = "[TakeProfit][{}]  Cerramos posición en futuros".format(symbol)
                print(msg)
                logger.info(msg)
                await close_position(symbol.replace('/USDT', ''), 'TakeProfit')

        if SIMULA_COMPRAS_VENTAS:
            simula_str = ' ' + green('SIMULA')
        else:
            simula_str = ''

        salida_datos = format_contenido.format(
            id,
            item['symbol'],
            item['leverage'],
            side,
            round(float(item['positionInitialMargin']), 2),
            amount,
            monitor_color,
            profit_str,
            result_str,
            round(float(item['entryPrice']), 2),
            date,
            take_profit_distance,
            stop_loss_distance,
            str(round(stop_custom, 2)) + '%' + ' [' + stoptrailing_custom + ']',
            simula_str,
            # str(round(TAKEPROFIT, 2)) + '%'
        )
        datos_pantalla_futures[item['symbol']] = salida_datos
        if FORMATO_SALIDA == 'terminal':
            pantalla_futures(symbol)

    total_result = total_result_global
    total_profit = total_profit_global

    # pprint.pprint(positions)
    # pprint.pprint(datos_pantalla_futures)
    # Comprobar si se han vendido monedas en trade
    for symbol in datos_pantalla_futures.copy():
        isset = False
        for p in positions:
            if symbol == p['symbol']:
                isset = True
        if isset == False:
            datos_pantalla_futures.pop(symbol)

    pantalla_futures(False)

def info_db_futures(symbol, price, amount, side, ultimo_precio, leverage):

    profit, profit_str = getProfit(side, ultimo_precio, price)

    logger.info(' symbol:' + symbol + ' side:' + side + ' profit:' + str(profit_str))

    leverage = int(leverage)
    if leverage in STOPS_SEGUN_APALANCAMIENTO:
        stoploss     = STOPS_SEGUN_APALANCAMIENTO[leverage][0]
        takeprofit   = STOPS_SEGUN_APALANCAMIENTO[leverage][1]
        stoptrailing = STOPS_SEGUN_APALANCAMIENTO[leverage][2]
    else:
        stoploss     = STOPLOSS
        takeprofit   = TAKEPROFIT
        stoptrailing = STOPTRAILING

    sql = """SELECT * FROM futures WHERE symbol LIKE '{}'
      AND price = {}
      AND date_sell IS NULL""".format(
    symbol,
    price,
    amount,
    side
    )

    f = sql_exec(sql, False, False, True)

    if len(f) > 0:
        order = f[0]
        update = False

        max_profit   = order['max_profit']
        min_profit   = order['min_profit']
        order_stoploss     = order['stoploss']
        order_stoptrailing = order['stoptrailing']

        if not order_stoploss:
            update = True
        else:
            stoploss = order_stoploss
        if not order_stoptrailing:
            update = True
        else:
            stoptrailing = order_stoptrailing

        logger.debug("stoptrailing: {}, profit: {}, stoploss: {}".format(stoptrailing, profit, stoploss))
        if not order['max_profit'] or float(order['max_profit']) < profit:
            max_profit = profit
            stoploss = calculo_stoptrailing(stoptrailing, profit, stoploss)
            update = True
        if not order['min_profit'] or float(order['min_profit']) > profit:
            min_profit = profit
            update = True
        if update:
            sql = "UPDATE futures SET max_profit = {}, min_profit = {}, stoploss = {}, stoptrailing = '{}' WHERE id = {}".format(
                max_profit,
                min_profit,
                stoploss,
                stoptrailing,
                order['id']
            )
            sql_exec(sql)

        return order
    else:
        info = "Manual l:" + str(leverage)
        date = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        sql = """INSERT INTO futures (date, symbol, amount, price, side, info, max_profit, min_profit, stoploss, stoptrailing)
        VALUES ('{}', '{}', {}, {}, '{}', '{}', {}, {}, {}, '{}')""".format(date, symbol, amount, price, side, info, profit, profit, stoploss, stoptrailing)
        i = sql_exec(sql)
        return {
            'id': 0,
            'stoploss': stoploss,
            'stoptrailing': stoptrailing
        }
        # return info_db_futures(symbol, price, amount, side, ultimo_precio)

# def get_dynamic_stop(stoptrailing, stop_custom, profit_actual):
#     ## Sistema de stoptrailing variable, "0.5:0.5 1:1 2:2"
#
#     stoploss = stop_custom
#     for st in str(stoptrailing).split(','):
#         msg = False
#         msg_info = False
#         st_array = st.split(';')
#         if len(st_array) > 1:
#             stop, distance = st_array
#             if not stoploss and profit_actual > float(stop):
#                 # Calculamos stoploss segun profit.
#                 stoploss = profit_actual - float(distance)
#                 msg_info = '[' + str(profit_actual) + '] Añadimos StopLoss de estrategia:' + str(round(stoploss, 2))
#
#             elif stoploss and profit_actual > float(stop) and stoploss < (profit_actual - float(distance)):
#                 stoploss = profit_actual - float(distance)
#                 msg_info = '[' + str(profit_actual) + '] Subimos StopLoss:' + str(round(stoploss, 2))
#
#         else:
#             msg = '[{}] StopTrainig mal definido: [{}]'.format(mark, st)
#             logger.error(msg)
#             print(msg)
#
#         if msg_info:
#             # logger.info(msg_info)
#             print(msg_info)
#
#     return stoploss

async def leverage_market(symbol='BTC/USDT', leverage=False):

    if not leverage:
        leverage =  5

    symbol = symbol.replace('/','')

    print("Symbol:", symbol, "Leverage:", leverage)
    response = await exchange_futures.fapiPrivate_post_leverage({
        'symbol': symbol,
        'leverage': leverage,
    })
    try:
        result_isolate = await change_to_isolated(symbol)
    except Exception as e:
        print(e)
        logger.error(e)
        logger.error('No se pudo cambiar a isolate, no se ejecuta orden')
        await exchange_futures.close()
        return False

    if not result_isolate:
        logger.error('No se pudo cambiar a isolate, no se ejecuta orden')
        await exchange_futures.close()
        return False

    await exchange_futures.close()
    return True

async def create_order_oco_futures(coin, side, amount, price, stop_price, stop_limit_price):

    symbol = coin + 'USDT'
    # market = exchange_futures.market(symbol)

    try:

        response = await exchange_futures.private_post_order_oco({
            # 'symbol': market['id'],
            'symbol': symbol,
            'side': side, # SELL, BUY
            'quantity': amount,
            'price': price,
            'stopPrice': stop_price,
            'stopLimitPrice': stop_limit_price,  # If provided, stopLimitTimeInForce is required
            'stopLimitTimeInForce': 'GTC',  # GTC, FOK, IOC
            # 'listClientOrderId': exchange_futures.uuid(),  # A unique Id for the entire orderList
            # 'limitClientOrderId': exchange_futures.uuid(),  # A unique Id for the limit order
            # 'limitIcebergQty': exchange_futures.amount_to_precision(symbol, limit_iceberg_quantity),
            # 'stopClientOrderId': exchange_futures.uuid()  # A unique Id for the stop loss/stop loss limit leg
            # 'stopIcebergQty': exchange_futures.amount_to_precision(symbol, stop_iceberg_quantity),
            # 'newOrderRespType': 'ACK',  # ACK, RESULT, FULL
        })

    except Exception as e:
        print('e:', e)
        tlg_send('Error en futures:', e)

    await exchange_futures.close()
    print('response:', response)

async def create_order_future(args, open=True, confirmar=True):
    ## open: Si estamos abriendo una posición open = True, si no false,
    ##       para que calcule bien el monto.
    ## confirmar: Preguntamos antes de ejecutar.

    logger.debug('args.amount: ' + str(args.amount))
    logger.debug('args.amount_dolars: ' + str(args.amount_dolars))

    type = 'market'  # or 'limit'
    # price = 0.060154  # or None
    # side = 'sell'  # or 'buy'
    # amount = 1.0
    price = None
    symbol = args.order + '/' + CURRENCY
    amount = False
    amount_dolars = False
    fee = 0
    # amount = args.amount
    # amount_dolars = args.amount_dolars
    # tipo de orden, (SELL, BUY)
    side = args.side
    leverage = args.leverage

    # extra params and overrides if needed
    # test no hace efecto, genera las ordenes en real.
    # params = {
    #     'test': True,  # test if it's valid, but don't actually place it
    # }
    params = {}

    if open and not SIMULA_COMPRAS_VENTAS and leverage:
        result_leverage = await leverage_market(symbol, leverage)
        if not result_leverage:
            logger.error('No se pudo definir leverage [{}] para [{}]'.format(leverage, symbol))
            return False
    elif open and not SIMULA_COMPRAS_VENTAS and leverage is False:
        logger.error('Sin leverage [{}] para [{}] no ejecutamos operación'.format(leverage, symbol))
        return False

    try:
        ticker = await exchange_futures.fetch_order_book(symbol)
        price = float(ticker['asks'][2][0])
    except Exception as e:
        msg = 'Error al buscar precio de moneada [{}].'.format(symbol)
        logger.error(msg)
        print(msg)
        await exchange_futures.close()
        return [False, 'Error al buscar precio de moneada [{}].'.format(symbol)]

    if hasattr(args, 'amount_dolars') and float(args.amount_dolars) > 0:
        amount_dolars = args.amount_dolars
        # Recoger precio actual de la moneda para poder calcular el monto a enviar.
        amount = float(args.amount_dolars) / price
        logger.debug('Amount in dolars: {} Amount: {}'.format(str(round(float(args.amount_dolars),2)) + '$', amount))
    else:
        if hasattr(args, 'amount') and args.amount:
            amount = float(args.amount)
            print(str(amount) + ' ' + symbol.replace('/USDT',''))
            logger.debug('Amount: {}'.format(str(round(float(args.amount),2)) + '$'))
        else:
            logger.error('Error: Es necesario pasar el monto en la moneda a comprar o en dolares')
            await exchange_futures.close()
            # @todo Notificar error con eventos.
            exit(1)

    # Amount * leverage para obtener el monto a invertir.
    if open:
        amount = float(amount) * float(leverage)
    amount = exchange_futures.amount_to_precision(symbol, amount)
    await exchange_futures.close()

    price = price

    print("\n# Orden en futuros")
    print("symbol:", symbol)
    print("type:", type)
    print("price:", price)
    print("amount:", amount)
    print("amount_dolars:", amount_dolars)
    print("side:", side)
    print("leverage:", leverage)

    if confirmar:
        user_input = input("\nConfirmar [S/N]: ")
    else:
        user_input = 's'

    if user_input == 's' or user_input == 'S':

        try:
            if SIMULA_COMPRAS_VENTAS:
                order = ['SIMULA ACTIVADO', 'No ejecutamos orden en market']
                return [True, [
                    111,
                    symbol,
                    amount,
                    price,
                    float(price) * float(amount),
                    datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                    'Filled',
                    side,
                    'market',
                    0,
                    True
                ], ("{'info': 'SIMULA_COMPRAS_VENTAS'}")]
            else:
                order = await exchange_futures.create_order(symbol, type, side, amount, None, params)
                await exchange_futures.close()
        except Exception as e:
            print(e)
            logger.error(e)
            await exchange_futures.close()
            msg = "ERROR [{}][{}][{}] {}".format(symbol, type, amount, e)
            print(msg)
            logger.error(msg)
            if TELEGRAM_CHAT_ID:
                tlg_send(msg)
            return [False, False, False]
        # pprint.pprint(order)
        logger.info(order)
        print("Orden enviada [{}]".format(green('OK')))
    else:
        print("Cancelado")


    # Resouesta.
    # {
    #     'info': {
    #         'orderId':'51429616341',
    #         'symbol': 'BTCUSDT',
    #         'status': 'FILLED',
    #         'clientOrderId':'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
    #         'price': '0',
    #         'avgPrice': '38797.30000',
    #         'origQty': '0.012',
    #         'executedQty': '0.012',
    #         'cumQty': '0.012',
    #         'cumQuote':'465.56760',
    #         'timeInForce': 'GTC',
    #         'type': 'MARKET',
    #         'reduceOnly': False,
    #         'closePosition': False,
    #         'side': 'SELL',
    #         'positionSide': 'BOTH',
    #         'stopPrice':'0',
    #         'workingType': 'CONTRACT_PRICE',
    #         'priceProtect': False,
    #         'origType':'MARKET',
    #         'updateTime': '1650888220916'
    #     },
    #
    #     'id': '51429616341',
    #     'clientOrderId':'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
    #     'timestamp': None,
    #     'datetime': None,
    #     'lastTradeTimestamp': None,
    #     'symbol': 'BTC/USDT',
    #     'type': 'market',
    #     'timeInForce': 'GTC',
    #     'postOnly': False,
    #     'side': 'sell',
    #     'price': 38797.3,
    #     'stopPrice': 0.0,
    #     'amount': 0.012,
    #     'cost': 465.5676,
    #     'average': 38797.3,
    #     'filled': 0.012,
    #     'remaining': 0.0,
    #     'status': 'closed',
    #     'fee': None,
    #     'trades':[],
    #     'fees': []
    # }

    if order['fee']:
        fee = order['fee']['cost']

    return [True, (
        order['info']['orderId'],
        symbol,
        order['amount'],
        order['price'],
        (float(order['cost']) / float(leverage)),
        order['datetime'],
        order['info']['status'],
        order['info']['side'],
        order['info']['type'],
        fee,
        order['filled']
    ), (str(order))]

async def futures_open_orders():

    try:
        balances_account = await exchange_futures.fetch_balance()
    except Exception as e:
        print(e)
        logger.error(e)
        await exchange_futures.close()
        return False, False

    balances = balances_account['info']
    # pprint.pprint(balances_account)

    assets = balances['assets']
    availableBalance = balances['availableBalance']
    positions = balances['positions']

    open_orders = []

    # for a in assets:
    #     if float(a['availableBalance']) > 0:
    #         pprint.pprint(a)

    for p in positions:

        if float(p['entryPrice']) > 0:

            symbol = p['symbol'].replace('USDT', '/USDT')
            try:
                orders = await exchange_futures.fetch_open_orders(symbol)
            except Exception as e:
                print(e)
                logger.error(e)
                await exchange_futures.close()
                return False, False

            for o in orders:
                # pprint.pprint(o)
                if o['type'] == 'stop_market':
                    p['stop_market'] = o['stopPrice']
                if o['type'] == 'take_profit_market':
                    p['take_profit_market'] = o['stopPrice']

            open_orders.append(p)

    await exchange_futures.close()

    return availableBalance, open_orders

def end(signal_received, frame):
    print("\nTerminamos")
    exit(0)

async def show_markets():
    markets = await exchange_futures.load_markets()  # Load the futures markets

    for symbol in markets:
        print(symbol)

    # market = exchange_futures.market('BTC/USDT')
    # pprint.pprint(market)

    await exchange_futures.close()

async def close_position(coin, info='', confirmar=False, leverage=1):

    global datos_pantalla_futures, availableBalance

    symbol = coin + 'USDT'
    availableBalance, positions = await futures_open_orders()

    for p in positions:
        if p['symbol'] == symbol:
            if float(p['positionAmt']) > 0:
                side = 'SELL'
            else:
                side = 'BUY'

            print("\n# Posición")
            print("Amount:", p['positionAmt'])
            print("Side: " + side)
            print("Profit: " + p['unrealizedProfit'] + '$')

            class Args:
                action = 'Close position'

            args = Args()
            args.order = coin
            args.amount = abs(float(p['positionAmt']))
            args.side = side
            args.amount_dolars = False
            args.leverage = leverage

            result_exec = await create_order_future(args, False, False)

            ## @todo Adaptar esta parte a futures independiente o eliminar la posibilidad.
            # if os.getenv('TELEGRAM_CHAT_ID'):
            #     tlg_send('Sell in futures [' + p['symbol'] + "]\n" + 'Profit: ' + str(round(float(p['unrealizedProfit']), 2)) + '$ ' + info)

            # date = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            # sql = "UPDATE futures SET date_sell = '{}', profit = {}, info_sell = '{}' WHERE symbol LIKE '{}' AND date_sell IS NULL".format(
            #     date,
            #     round(float(p['unrealizedProfit']), 2),
            #     info,
            #     coin + '/USDT'
            # )
            # sql_exec(sql)

            datos_pantalla_futures = {}

            return result_exec

    msg = "[{}] No se ha encontrado operación".format(symbol)
    print(msg)
    logger.error(msg)
    return False

def change_stop(coin, stop, stoptrailing=False):

    if stoptrailing:
        sql = "UPDATE futures SET stoploss = {}, stoptrailing = '{}' WHERE symbol LIKE '{}' AND date_sell IS NULL".format(
            stop,
            stoptrailing,
            coin + '/USDT'
        )
    else:
        sql = "UPDATE futures SET stoploss = {} WHERE symbol LIKE '{}' AND date_sell IS NULL".format(
            stop,
            coin + '/USDT'
        )
    return sql_exec(sql)

def get_parser():
    """
    Creates a new argument parser.
    """
    parser = argparse.ArgumentParser('futures')
    version = '%(prog)s ' + __version__
    parser.add_argument('--version', '-v', action='version', version=version)
    parser.add_argument('--refresh',
                        '-r',
                        help='Parada en segundos hasta nuevo refresco, por defecto 60',
                        required=False,
                        default=False
                        )
    parser.add_argument('--telegram',
                        '-tg',
                        help='Formato de salida para telegram',
                        required=False,
                        default=False,
                        action="store_true"
                        )
    parser.add_argument('--markets',
                        '-m',
                        help='Mostrar mercados',
                        required=False,
                        default=False,
                        action="store_true"
                        )
    parser.add_argument('--order',
                        '-o',
                        help='Create order, example: futures.py -o ETH -ad 50 -sd SELL -l 10"',
                        required=False,
                        default=False
                        )
    parser.add_argument('--coin',
                        '-cn',
                        help='Modeda, ejemplo: BTC, ETH, ...',
                        required=False
                        )
    parser.add_argument('--leverage',
                        '-l',
                        help='Fijar apalancaminento en un mercado concreto, example. -l "BTC 5"',
                        required=False,
                        default=False
                        )
    parser.add_argument('--amount',
                        '-a',
                        help='Cantidad, ejemplo: 1000, 100, ...',
                        required=False
                        )
    parser.add_argument('--amount_dolars',
                        '-ad',
                        help='Cantidad a comprar en dolares, ejemplo: 1000, 100, ...',
                        required=False
                        )
    parser.add_argument('--price',
                        '-p',
                        help='Precio.',
                        required=False
                        )
    parser.add_argument('--side',
                        '-sd',
                        help='Ejemplo: buy or sell',
                        default = 'buy',
                        required=False
                        )
    parser.add_argument('--close',
                        '-ce',
                        help='Ejemplo: BTC, ETH, ...',
                        default = False,
                        required=False
                        )
    parser.add_argument('--stop',
                        '-st',
                        help='Personalizar stop y stoptrailing, Ejemplo: "BTC -2 2;2,3;3"',
                        default = False,
                        required=False
                        )
    # parser.add_argument('--oco_order',
    #                     '-oco',
    #                     help='Enviar orden OCO, "COIN SIDE AMOUNT PRICE STOP_PRICE STOP_LIMIT_PRICE", Ejemplo: "BTC SELL 0.0014 35000 33000 33100',
    #                     default = False,
    #                     required=False
    #                     )
    return parser

def main(args=None):

    global availableBalance, FORMATO_SALIDA

    parser = get_parser()
    args = parser.parse_args(args)

    loop = asyncio.get_event_loop()

    if args.stop:
        args_stop = args.stop.split()
        coin = args_stop[0]
        stop = args_stop[1]
        if len(args_stop) > 2:
            stoptrailing = args_stop[2]
        else:
            stoptrailing = False

        change_stop(coin, stop, stoptrailing)
        exit()

    # if args.oco_order:
    #     args_oco = args.oco_order.split()
    #     coin = args_oco[0]
    #     side = args_oco[1]
    #     amount = args_oco[2]
    #     price = args_oco[3]
    #     stop_price = args_oco[4]
    #     stop_limit_price = args_oco[5]
    #     loop.run_until_complete(create_order_oco_futures(coin, side, amount, price, stop_price, stop_limit_price))
    #     exit()

    if args.close:
        loop.run_until_complete(close_position(args.close))
        exit()

    if args.leverage and args.order is False:
        if args.coin:
            coin = args.coin
        else:
            coin = args.order
        leverage = args.leverage
        symbol   = coin + 'USDT'
        result_leverage = loop.run_until_complete(leverage_market(symbol, leverage))
        if not result_leverage:
            exit()
        if not args.order:
            exit()

    if args.order:
        loop.run_until_complete(create_order_future(args))
        exit()

    if args.telegram:
        FORMATO_SALIDA = 'telegram'

    if args.markets:
        loop.run_until_complete(show_markets())
        exit()

    if args.refresh:

        continuar = True
        while continuar:

            try:

                availableBalance_positions = loop.run_until_complete(futures_open_orders())
                if not availableBalance_positions:
                    tlg_send('No podemos conectar con exchange, esperamos 1m')
                    time.sleep(60)
                else:
                    availableBalance, positions = availableBalance_positions
                    # print("Available Balance:", availableBalance)
                    loop.run_until_complete(show_positions(positions))

            except KeyboardInterrupt:
                print('\n**Terminado**')
                loop.stop()
                loop.close()
                continuar = False

            except Exception as e:
                print(e)
                # print(traceback.format_exc())
                tlg_send('Error en futures: {}'.format(e))

            time.sleep(int(args.refresh))

    else:

        availableBalance_positions = loop.run_until_complete(futures_open_orders())
        if not availableBalance_positions:
            print('No se pudo conectar con exchange')
            return False

        availableBalance, positions = availableBalance_positions
        loop.run_until_complete(show_positions(positions))

    logger.info('futures finito')

if __name__ == '__main__':
    main()
