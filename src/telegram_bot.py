#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## @file balances.py
## @brief Listado del balance.

__version__ = '0.1.0'
__author__ = u'edu@lesolivex.com'

import sys
import re
import subprocess

sys.path.append('./src')

from base import *                    # pylint: disable=W0401
from db import *                      # pylint: disable=W0401
from balances import *                # pylint: disable=W0401
from mercado import *                 # pylint: disable=W0401
from reports import *                 # pylint: disable=W0401
from telegram_send_message import *   # pylint: disable=W0401

# mastrobot_example.py
from telegram import ParseMode
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, CallbackContext
# Enable logging
from os import system

def start(update, context):
    update.message.reply_text('start command received')

def help(update, context):
    update.message.reply_text('help command received')

def error(update, context):
    update.message.reply_text('an error occured')
def error_handler(update: object, context: CallbackContext) -> None:
    """Log the error and send a telegram message to notify the developer."""
    # Log the error before we do anything else, so we can see it even if something breaks.
    logger.error(msg="Exception while handling an update:", exc_info=context.error)

def send_message(update, msg, title=False):
    if title:
        # update.message.reply_text(title, parse_mode=telegram.ParseMode.MARKDOWN)
        title = '**' + escape_telegrambot_underscore(title) + "**\n"
    else:
        title = ''

    if msg is False:
        return False

    elif len(msg) > 4000:
        for x in range(0, len(msg), 4000):
            update.message.reply_text(title + "```\n" + escape_telegrambot_underscore(msg[x:x+4000]) + "\n```\n", parse_mode=ParseMode.MARKDOWN_V2, disable_web_page_preview=True)

    else:
        update.message.reply_text(title + "\n```\n" + escape_telegrambot_underscore(msg) + "\n```\n", parse_mode=ParseMode.MARKDOWN_V2, disable_web_page_preview=True)

def text(update, context):
    text_received = update.message.text

    print('Order received: [{}]'.format(text_received))

    if (
        text_received == 'balance' or
        text_received == 'Balance'
    ):

        send_message(update, 'In a moment you will receive the balance.', 'Balance request')
        system("./src/balances.py -tel")

    elif (
        text_received == 'status' or
        text_received == 'Status' or
        re.match('^status *', text_received) or
        re.match('^Status *', text_received)
    ):

        args = text_received.split()[1:]
        test = False
        if len(args) > 0 and args[0] == 'test':
            test = True
        elif len(args) > 0 and args[0] != '':
            update.message.reply_text('Unrecognized parameter [{}]'.format(" ".join(args)))

        salida = db_trades('all', test, True)

        if salida is False:
            salida = "No output"

        print(salida)
        send_message(update, salida, 'last trades')

        cmd = "./src/reports.py -tg -rt | ansi2txt"
        try:
            salida = subprocess.check_output(cmd, cwd="./", shell=True, stderr=subprocess.STDOUT)
        except subprocess.CalledProcessError as grepexc:
            salida = "Error code", grepexc.returncode, grepexc.output
            msg = 'Error executing command'
            update.message.reply_text(msg)
        print(salida)
        # update.message.reply_text(salida.decode())
        send_message(update, salida.decode(), 'Open trades')

    elif re.match('^show config*', text_received) or re.match('^Show config*', text_received):

        strategy = False
        title = "General config"
        args = text_received.split()[2:]
        if len(args) > 0:
            strategy = args[0]
            title = "Config of " + strategy
            salida = show_config(strategy)
        else:
            salida = show_config_general()
            config_general = get_config_general()
            estrategias_activadas = config_general['estrategias_activadas']
            for e in estrategias_activadas.split():
                salida += "Strategy: " + e
                salida += show_config(e)
        print(salida)
        # update.message.reply_text(salida)
        send_message(update, salida, title)

    elif re.match('^trade *', text_received):
        args = text_received.split()[1:]
        if len(args) > 0:
            trade_id = args[0]
        else:
            update.message.reply_text('Unrecognized parameter [{}]'.format(" ".join(args)))
            return

        salida = db_trade(trade_id, False, True)
        print(salida)

        if (salida):
            send_message(update, salida, 'Trade detail [{}]'.format(trade_id))
        else:
            msg = 'Error looking for trade detail'
            update.message.reply_text(msg)

    elif re.match('^CLOSE *', text_received):
        args = text_received.split()[1:]
        if len(args) > 0:
            coin = args[0]
        else:
            update.message.reply_text('Unrecognized parameter [{}]'.format(" ".join(args)))
            return

        class Args:
            close = coin

        cmd = "./src/mercado.py -sc {}".format(coin)

        try:
            salida = subprocess.check_output(cmd, cwd="./", shell=True)
        except subprocess.CalledProcessError as grepexc:
            salida = "Error code", grepexc.returncode, grepexc.output

        print(salida[2])

        if (salida):
            send_message(update, str(salida[2].decode()), 'Venta {}'.format(coin))
        else:
            msg = 'Error in sell'
            update.message.reply_text(msg)

    elif re.match('^OPEN *', text_received):
        args = text_received.split()[1:]
        if len(args) > 0:
            coin = args[0]
            amount = args[1]
        else:
            update.message.reply_text('Unrecognized parameter [{}]'.format(" ".join(args)))
            return

        cmd = "./src/mercado.py -ad {} -bc {}".format(float(amount), coin)

        try:
            salida = subprocess.check_output(cmd, cwd="./", shell=True)
        except subprocess.CalledProcessError as grepexc:
            salida = "Error code", grepexc.returncode, grepexc.output

        print(salida[2])

        if (salida):
            send_message(update, str(salida[2].decode()), 'Open {}: {}$'.format(coin, amount))
        else:
            msg = 'Error opening trade'
            update.message.reply_text(msg)

    elif re.match('^logs', text_received):

        cmd = "tail -n 20 logs/emma.log"

        try:
            salida = subprocess.check_output(cmd, cwd="./", shell=True)
        except subprocess.CalledProcessError as grepexc:
            salida = "Error code", grepexc.returncode, grepexc.output

        print(salida)

        if (salida):
            send_message(update, str(salida.decode()), 'Logs')
        else:
            msg = 'Error executing command'
            update.message.reply_text(msg)

    elif re.match('^restart', text_received):

        if SIMULA_COMPRAS_VENTAS:
            cmd = "./emma update simula"
        else:
            cmd = "./emma update"

        try:
            salida = subprocess.check_output(cmd, cwd="./", shell=True)
        except subprocess.CalledProcessError as grepexc:
            salida = "Error code", grepexc.returncode, grepexc.output

        print(salida)

        if (salida):
            send_message(update, str(salida.decode()), 'restart')
        else:
            msg = 'Error executing command'
            update.message.reply_text(msg)

    elif re.match('^config *', text_received):
        mark = text_received.split()[1]
        if len(mark) > 1:
            ## Configuración general.
            new_config = text_received.split()[1:]
            new_config = " ".join(new_config)
            print('New config:', new_config)
            if set_config_general(new_config):
                msg = 'New configuration accepted.'
            else:
                msg = 'Error with new configuration.'
        else:
            new_config = text_received.split()[2:]
            new_config = " ".join(new_config)
            print('New config:', new_config)
            if set_config(mark, new_config):
                msg = 'New configuration accepted.'
            else:
                msg = 'Error with new configuration.'
        update.message.reply_text(msg)

    elif text_received == 'help' or text_received == 'Help':
        
        msg = "\nOrders:\n"
        msg += "- status [test]: Show trades, last and active.\n"
        msg += "- balance: Show balance.\n"
        msg += "- trade [id]: Show detail trade.\n"
        msg += "- show config [Mark]: Show strategy or general.\n"
        msg += "- logs: Show last logs.\n"
        ## msg += "- restart: Restart app.\n"
        msg += "- OPEN [coin] [amount]: Open trade.\n"
        msg += "- CLOSE [coin]: Close trade\n"
        msg += "- config [Mark] [key:value]: Update strategy config\n"
        msg += "  or general if strategy is not specified\n"

        update.message.reply_text(msg)

    else:
        cmd = text_received.split()[0]
        args = text_received.split()[1:]
        cmd = "./src/{}.py -tg {} | ansi2txt".format(cmd, " ".join(args))

        try:
            salida = subprocess.check_output(cmd, cwd="./", shell=True, stderr=subprocess.STDOUT)
        except subprocess.CalledProcessError as grepexc:
            salida = "Error code", grepexc.returncode, grepexc.output
            msg = 'Error executing command'
            update.message.reply_text(msg)
        print(salida)
        # update.message.reply_text(salida.decode())
        send_message(update, salida.decode())

def main():

    # create the updater, that will automatically create also a dispatcher and a queue to
    # make them dialoge
    updater = Updater(os.getenv('TELEGRAM_TOKEN'), use_context=True)
    dispatcher = updater.dispatcher

    # add handlers for start and help commands
    dispatcher.add_handler(CommandHandler("start", start))
    dispatcher.add_handler(CommandHandler("help", help))

    # add an handler for normal text (not commands)
    dispatcher.add_handler(MessageHandler(Filters.text, text))

    # add an handler for errors
    # dispatcher.add_error_handler(error)
    dispatcher.add_error_handler(error_handler)

    # start your shiny new bot
    updater.start_polling()

    # run the bot until Ctrl-C
    updater.idle()

if __name__ == '__main__':
    main()
