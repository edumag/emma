#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## @file chart.py
## @brief chart in ASCII.

__version__ = '0.1.0'
__author__ = u'edu@lesolivex.com'

import os
import sys
import asciichart
import argparse

from math import floor
from math import ceil
from datetime import datetime


# -----------------------------------------------------------------------------

this_folder = os.path.dirname(os.path.abspath(__file__))
root_folder = os.path.dirname(os.path.dirname(this_folder))
sys.path.append(root_folder + '/python')
sys.path.append(this_folder)

# -----------------------------------------------------------------------------

import ccxt  # noqa: E402

# -----------------------------------------------------------------------------

binance = ccxt.binance()
symbol = 'BTC/USDT'
timeframe = '1d'

# each ohlcv candle is a list of [ timestamp, open, high, low, close, volume ]
index = 4  # use close price from each ohlcv candle

terminal_size = os.get_terminal_size()

height = terminal_size[1] - 10
length = terminal_size[0] - 10

def plot(series, cfg={}):

    minimum = min(series)
    maximum = max(series)

    interval = abs(float(maximum) - float(minimum))
    offset = cfg['offset'] if 'offset' in cfg else 3
    # padding = cfg['padding'] if 'padding' in cfg else '       '
    height = cfg['height'] if 'height' in cfg else interval
    ratio = height / interval
    # print(minimum,ratio,type(minimum))
    min2 = floor(float(minimum) * ratio)
    max2 = ceil(float(maximum) * ratio)

    intmin2 = int(min2)
    intmax2 = int(max2)

    rows = abs(intmax2 - intmin2)
    width = len(series) + offset
    # format = cfg['format'] if 'format' in cfg else lambda x: (padding + '{:.2f}'.format(x))[:-len(padding)]

    result = [[' '] * width for i in range(rows + 1)]

    # axis and labels
    for y in range(intmin2, intmax2 + 1):
        label = '{:8.2f}'.format(float(maximum) - ((y - intmin2) * interval / rows))
        result[y - intmin2][max(offset - len(label), 0)] = label
        result[y - intmin2][offset - 1] = '┼' if y == 0 else '┤'

    y0 = int(series[0] * ratio - min2)
    result[rows - y0][offset - 1] = '┼'  # first value

    for x in range(0, len(series) - 1):  # plot the line
        y0 = int(round(series[x + 0] * ratio) - intmin2)
        y1 = int(round(series[x + 1] * ratio) - intmin2)
        if y0 == y1:
            result[rows - y0][x + offset] = '─'
        else:
            result[rows - y1][x + offset] = '╰' if y0 > y1 else '╭'
            result[rows - y0][x + offset] = '╮' if y0 > y1 else '╯'
            start = min(y0, y1) + 1
            end = max(y0, y1)
            for y in range(start, end):
                result[rows - y][x + offset] = '│'

    return '\n'.join([''.join(row) for row in result])


def print_chart(exchange, symbol, timeframe):

    print("\n" + exchange.name + ' ' + symbol + ' ' + timeframe + ' chart:')

    # get a list of ohlcv candles
    ohlcv = exchange.fetch_ohlcv(symbol, timeframe)

    # get the ohlCv (closing price, index == 4)
    dates = [x[0] for x in ohlcv]

    date1 = datetime.fromtimestamp(int(dates[0]) / 1000)
    date1 = date1.strftime("%d/%m %H:%M")
    date2 = datetime.fromtimestamp(int(dates[len(dates)-1]) / 1000)
    date2 = date2.strftime("%d/%m %H:%M")

    series = [x[index] for x in ohlcv]

    # print the chart
    print("\n" + plot(series[-length:], {'height': height}))  # print the chart

    last = ohlcv[len(ohlcv) - 1][index]  # last closing price
    return last, date1, date2


def get_parser():
    """
    Creates a new argument parser.
    """
    parser = argparse.ArgumentParser('trades')
    version = '%(prog)s ' + __version__
    parser.add_argument('--version', '-v', action='version', version=version)
    parser.add_argument('--timeframe', '-t', help='Timeframe, default: 1d')
    parser.add_argument('--coin', '-c', help='Coin: default: BTC')
    return parser

def main(args=None):

    global timeframe, symbol

    parser = get_parser()
    args = parser.parse_args(args)

    if args.timeframe:
        timeframe = args.timeframe

    if args.coin:
        symbol = args.coin + '/USDT'

    last, date1, date2 = print_chart(binance, symbol, timeframe)
    print("\n    {} = {} [{} / {}]\n".format(symbol, last, date1, date2))

if __name__ == '__main__':
    main()
