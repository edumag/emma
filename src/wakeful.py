#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## @file wakeful.py
## @brief Vigilante.

__version__ = '0.1.0'
__author__ = u'edu@lesolivex.com'

import sys

sys.path.append('./src')

from base import *  # pylint: disable=W0401
from mercado import *  # pylint: disable=W0401
from db import *  # pylint: disable=W0401
from telegram_send_message import *
from strategies import *  # pylint: disable=W0401
from reports import *  # pylint: disable=W0401
from signal import signal, SIGINT

# Tendencia.
relacion_anterior = {}
relacion_open_anterior = {}
tendencia_anterior = {}
tendencia_open_anterior = {}
up_signals = {}
status_market = {}
simula_anterior = {}
status_simula_anterior = {}

# Configuración.
WAKEFUL_EXEC_OPEN = strtobool(os.getenv('WAKEFUL_EXEC_OPEN', False))
WAKEFUL_EXEC_CLOSE = strtobool(os.getenv('WAKEFUL_EXEC_CLOSE', False))
WAKEFUL_DB_SIMULA = os.getenv('WAKEFUL_DB_SIMULA', False)

async def wakeful(make_changes=False):
    ### @brief wakeful Vigilante.
    ###
    ### Vamos adaptando la configuración según resultados.
    ###
    ### @todo Aumentar MIN_MAX_TRADES_STRATEGY a 1 si los resultados en test
    ###       son positivos sino 0.
    ### @todo Estudiar como afecta last_two y si vale la pena mantenerlo.
    ### @todo En caso de que trades.py no este funcionando cerrar operaciones
    ###       abiertas.
    ### @todo Los test de estrategia no reflejan la realidad en los resultados,
    ###       solo los utilizamos para cambiar de stoptrailing. Queda pendiente
    ###       hacer que los test de estrategia sean más reales.
    ### @todo Tener en cuenta los trades reales a la hora de valorar la
    ###       tendencia.

    global relacion_anterior, relacion_open_anterior, tendencia_anterior
    global tendencia_open_anterior, up_signals, simula_anterior, status_simula_anterior

    ## El test tiene que dar un minimo de profit para darlo por bueno.
    MIN_PROFIT_TEST = 5
    THE_BEST = False
    THE_BEST_RESULT = MIN_PROFIT_TEST

    signal(SIGINT, end)

    config_general = get_config_general()
    estrategias_activadas = config_general['estrategias_activadas'].split()
    report = report_status_market()

    color = 0
    for e in estrategias_activadas:

        color += 1
        eid = get_eid(e)
        strategy = get_strategy(eid)
        time_strategy = calcular_tiempo_estrategia(eid)
        sql_status = "SELECT * FROM test_trade_result WHERE eid = {} and date > datetime('now', '{}')".format(eid, time_strategy)
        est = estadisticas_informe(sql_status, False, False)
        sst = status_strategy_in_test(e)
        if sst > 2:
            sst_str = green(round(sst, 1))
        elif sst > 1:
            sst_str= blue(round(sst, 1))
        elif sst > 0:
            sst_str= orange(round(sst, 1))
        else:
            sst_str= red(round(sst, 1))
        total_trades_test, total_losses_strategy_test, buy_symbols_test, last_two_test, total_stoploss_positive_test = await report_status(False, True, False, e)
        # LastTwo
        if last_two_test > 1:
            last_two_test_str = red(last_two_test)
        elif last_two_test > 0:
            last_two_test_str = orange(last_two_test)
        else:
            last_two_test_str = green('0')

        relacion = est['x']['relacion']
        if relacion and e in relacion_anterior:
            tendencia = relacion - relacion_anterior[e]
        else:
            tendencia = 0
        relacion_anterior[e] = relacion
        # No mantenemos la tendencia para poder contabilizarlas.
        # if tendencia:
        #     tendencia_anterior[e] = tendencia
        # elif e in tendencia_anterior:
        #     tendencia = tendencia_anterior[e]
        tendencia_anterior[e] = tendencia

        salida = '[{}] Time: {:<12} Rln: {:>12} SST: {:>12} LT: {:>3}'.format(e, time_strategy, color_result(relacion), sst_str, last_two_test_str)

        # Estadisticas sobre operaciones abiertas en test.
        sql_open = "SELECT * FROM test_trade_result WHERE eid = {} and date_sell IS NULL".format(eid)
        estopen = estadisticas_informe(sql_open, False, False)
        if estopen:
            relacion_open = estopen['x']['relacion']
            rlnopen = color_result(relacion_open)
        else:
            relacion_open = False
            rlnopen = 0

        if relacion_open and e in relacion_open_anterior:
            tendencia_open = relacion_open - relacion_open_anterior[e]
        else:
            tendencia_open = 0
        relacion_open_anterior[e] = relacion_open
        # if tendencia_open:
        #     tendencia_open_anterior[e] = tendencia_open
        # elif e in tendencia_open_anterior:
        #     tendencia_open = tendencia_open_anterior[e]
        tendencia_open_anterior[e] = tendencia_open

        salida += colores(color, " RlnOpen: {:>12}".format(rlnopen))

        # Tendencia
        # salida += colores(color, " Tnd: {:>12}/{:<12}".format(color_result(tendencia), color_result(tendencia_open)))
        tendencia_total = tendencia - tendencia_open
        salida += colores(color, " Tnd: {:>12}".format(color_result(tendencia_total)))

        # Vamos sumando numero de tendencias positivas.
        if tendencia_total > 0 and e in up_signals:
            if up_signals[e] < 0:
                up_signals[e] = 1
            else:
                up_signals[e] = up_signals[e] + 1
        elif tendencia_total < 0 and e in up_signals:
            if up_signals[e] > 0:
                up_signals[e] =  -1
            else:
                up_signals[e] = up_signals[e] - 1

        if not e in up_signals:
            up_signals[e] = 0

        salida += colores(color, " US: {:>11}".format(color_result(up_signals[e])))

        # Valoración mercado.
        status_market = False
        if report:

            precios_general, tendencias_general, macd_general, media_market = report[strategy['temporalidad']]
            precios_general_1m, tendencias_general_1m, macd_general_1m, media_market_1m = report['1m']
            precios_general_5m, tendencias_general_5m, macd_general_5m, media_market_5m = report['5m']

            if media_market_1m > 50 and media_market_5m > 50 and media_market > 50:
                result_status_market = 'UP'
            elif media_market_1m <= 50 and media_market_5m <= 50 and media_market <= 50:
                result_status_market = 'DOWN'
            else:
                result_status_market = 'F'

            if (strategy['type'] == 'LONG' or strategy['type'] == 'SPOT') and result_status_market == 'UP':
                status_market = True
            elif strategy['type'] == 'SHORT' and result_status_market == 'DOWN':
                status_market = True
            else:
                status_market = False

            if status_market:
                status_market_str = green('T')
            else:
                status_market_str = red('F')

            salida += colores(
                color, ' StM: {:<14} {:<14} {:<14} ({:<5})'.format(
                color_status_market(media_market_1m)[1],
                color_status_market(media_market_5m)[1],
                color_status_market(media_market)[1],
                status_market_str
                )
            )

        # Testeamos compras al detectar subida de tendencia.
        if status_market:
            if up_signals[e] > 2:
                test2real(e)
                up_signals[e] = 0

        else:
            if up_signals[e] < -2:
                close_real(e)
                up_signals[e] = 0


        new_config = False
        new_max_trades = DEF_MAX_TRADES_STRATEGY
        if make_changes:

            new_max_trades = MIN_MAX_TRADES_STRATEGY
            new_max_losses = MIN_MAX_LOSSES_STRATEGY

            # Cambio max_trades de estrategia.
            # @todo Tener en cuenta si el mercado va a favor.
            if tendencia_total > 1 and relacion_open > 1 and status_market:

                new_max_trades = MAX_MAX_TRADES_STRATEGY
                new_max_losses = MAX_MAX_LOSSES_STRATEGY
                new_config = True

            elif (tendencia_total >= 0 and relacion_open >= 0) or status_market:

                new_max_trades = DEF_MAX_TRADES_STRATEGY
                new_max_losses = DEF_MAX_LOSSES_STRATEGY
                new_config = True

            elif not status_market:  # (tendencia_total < 0 or relacion_open < 0) and not status_market:

                new_max_trades = MIN_MAX_TRADES_STRATEGY
                new_max_losses = MIN_MAX_LOSSES_STRATEGY
                new_config = True

            config = get_config(e)

            if new_config:


                if change_max_trades(e, new_max_trades):

                    salida += bold(" MT: {}".format(new_max_trades))

                else:
                    salida += colores(color, " MT: {:<2}".format(config['max_trades']))

            else:
                salida += colores(color, " MT: {:<2}".format(config['max_trades']))


        # Resultados en simula.
        if WAKEFUL_DB_SIMULA and WAKEFUL_DB_SIMULA != '':
            result_simula = result_greater_than(WAKEFUL_DB_SIMULA, e)
            if e in simula_anterior and result_simula > simula_anterior[e]:
                status_simula = 1
                status_simula_anterior[e] = status_simula
                logger.info('Simula subiendo en [{}]'.format(e))
            elif e in simula_anterior and result_simula < simula_anterior[e]:
                status_simula = -1
                status_simula_anterior[e] = status_simula
                logger.info('Simula bajando en [{}]'.format(e))
            else:
                if e in status_simula_anterior:
                    status_simula = status_simula_anterior[e]
                else:
                    status_simula = 0

            simula_anterior[e] = result_simula

            salida += " {} ({}) {}".format(colores(color, '[SIM]'), color_result(status_simula), color_result(result_simula))

            if result_simula > 0 and status_simula >= 0:
                if change_max_trades(e, new_max_trades):
                    salida += ' MT: {}'.format(new_max_trades)
            else:
                if change_max_trades(e, 0):
                    salida += ' MT: {}'.format(0)


        print(colores(color, salida))

def change_max_trades(e, new_max_trades):

    config = get_config(e)

    # Comprobar si hay diferencias con la configuración actual.
    if int(config['max_trades']) != int(new_max_trades):
        new_config = "max_trades:{}".format(new_max_trades)
        msg = "[{}] {}".format(e, new_config)
        set_config(e, new_config)
        eventos('CHANGE_CONFIG', msg)
        return True

    else:

        return False

def test2real(mark):
    ### Abre operación en real.

    if not WAKEFUL_EXEC_OPEN:
        logger.info('Ejecutar compras en WakeFul desativado')
        return

    open = False
    strategy = get_strategy(get_eid(mark))
    config = get_config(mark)

    # Buscamos trade en test.
    sql = "SELECT * from test_trade_result WHERE date_sell IS NULL and mark LIKE '{}'".format(mark)
    trades = sql_exec(sql, False, False, True)
    for t in trades:
        coin = t['symbol'].replace('/' + CURRENCY,'')
        print("{} {} {}".format(t['id'], coin, t['profit']))
        if is_buy(coin, False, strategy['id']):
            continue
        else:
            open = True


    if open:
        logger.info('[{}][{}] Abrimos operación desde wakeful'.format(coin, mark))
        class Args:
            side = 'buy'
        argsBuy = Args()
        argsBuy.type = strategy['type']
        argsBuy.leverage = strategy['leverage']
        argsBuy.open = coin
        argsBuy.amount_dolars = config['amount_buy']
        loop = asyncio.get_event_loop()
        ejecucion = loop.run_until_complete(ejecutar_compra(argsBuy, 'WakeFul', 'WAKE ' + mark))
    else:
        logger.info('[{}] WakeFul: Sin operaciones en test no hacemos nada'.format(mark))

def close_real(mark):

    if not WAKEFUL_EXEC_CLOSE:
        logger.info('Ejecutar ventas en WakeFul desativado')
        return

    logger.info('WakeFul: Close all in [{}]'.format(mark))
    strategy = get_strategy(get_eid(mark))
    config = get_config(mark)

    # Buscamos trade.
    sql = "SELECT * from trade_result WHERE date_sell IS NULL and mark LIKE '{}'".format(mark)
    trades = sql_exec(sql, False, False, True)
    for t in trades:
        coin = t['symbol'].replace('/' + CURRENCY,'')
        print("{} {} {}".format(t['id'], coin, t['profit']))

        logger.info('[{}][{}] Cerramos operación desde wakeful'.format(coin, mark))
        class Args:
            side = 'buy'
        argsSell = Args()
        argsSell.type = strategy['type']
        argsSell.leverage = strategy['leverage']
        argsSell.close = coin
        argsSell.amount_dolars = config['amount_buy']
        loop = asyncio.get_event_loop()
        ejecucion = loop.run_until_complete(ejecutar_venta(argsSell, 'WakeFul'))

def color_status_market(media_market):
    if media_market > 70:
        result = 'UP+'
        result_color = green(result)
    elif media_market > 50:
        result = 'UP'
        result_color = blue(result)
    elif media_market > 3:
        result = 'DOWN'
        result_color = orange(result)
    elif media_market > 0:
        result = 'DOWN+'
        result_color = red(result)
    else:
        result = 'F'
        result_color = bold("False")

    return result, result_color

def end(signal_received, frame):
    print("\nTerminamos")
    exit(0)
def get_parser():
    """
    Creates a new argument parser.
    """
    parser = argparse.ArgumentParser('db')
    version = '%(prog)s ' + __version__
    parser.add_argument('--version', '-v', action='version', version=version)
    parser.add_argument('--no_make_changes', '-nmc', help='No ejecutar cambios en configuración', default=True, action='store_true')
    parser.add_argument('--refresh',
                        '-r',
                        help='Parada en minitos hasta nuevo refresco, por defecto no se hace bucle',
                        required=False,
                        default=False
                        )

    return parser

def main(args=None):
    """
    Main entry point for your project.
    Args:
        args : list
            A of arguments as if they were input in the command line. Leave it
            None to use sys.argv.
    """

    parser = get_parser()
    args = parser.parse_args(args)

    make_changes = True
    if args.no_make_changes:
        make_changes = False

    if args.refresh and int(args.refresh) > 0:
        minutos = int(args.refresh)
        while True:
            os.system('clear')
            loop = asyncio.get_event_loop()
            loop.run_until_complete(wakeful(make_changes))
            time_diff = datetime.now() + timedelta(minutes=int(args.refresh))
            time.sleep(minutos)
    else:
        loop = asyncio.get_event_loop()
        loop.run_until_complete(wakeful(make_changes))
    return


if __name__ == '__main__':
    main()
