# -*- coding: utf-8 -*-

import os
import sys
from dotenv import load_dotenv
load_dotenv()

sys.path.append('./src')

from console import *

import ccxt.async_support as ccxt  # noqa: E402

CURRENCY = 'USDT'

TRADES_CURRENCY = ['BTC', 'ETH', 'XRP']

CURRENCIES_DEPOSITS = ['USDT', 'BTC', 'ETH', 'EUR']

root = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
sys.path.append(root + '/python')

exchanges = {
    'binance': [os.getenv('binance_api'), os.getenv('binance_secret')],
    'kraken': [os.getenv('kraken_api'), os.getenv('kraken_secret')]
    }

binance = ccxt.binance({
    'apiKey': exchanges['binance'][0],
    'secret': exchanges['binance'][1],
    'verbose': False,  # switch it to False if you don't want the HTTP log
})

kraken = ccxt.kraken({
    'apiKey': exchanges['kraken'][0],
    'secret': exchanges['kraken'][1],
    'verbose': False,  # switch it to False if you don't want the HTTP log
})
