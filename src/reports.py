#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## @file balances.py
## @brief Listado del balance.

__version__ = '0.1.0'
__author__ = u'edu@lesolivex.com'

import sys
import sqlite3
import argparse
import pprint  # pylint: disable=W0401
# import re

sys.path.append('./src')

from base import *  # pylint: disable=W0401
from db import *  # pylint: disable=W0401
# from tendencia import *  # pylint: disable=W0401
import tendencia
from telegram_send_message import *
from strategies import *  # pylint: disable=W0401
from datetime import datetime, timedelta
from signal import signal, SIGINT

FILTRO_INFORMES        = False
FILTRO_INFORMES_HUMAN  = False
AGRUPAR_INFORMES       = False
FORMATO_SALIDA         = False

def status_market_screen():

    VALORACION_MERCADO_GLOBAL = tendencia.valoracion_mercado()
    barra_informativa = ""
    color = 0
    for temporalidad in TEMPORALIDADES:
        color += 1
        result_market, media_market, monitor_market, result_market_color = VALORACION_MERCADO_GLOBAL[temporalidad]
        barra_informativa += "{} {:>22} {}\n".format(
            colores(color, "{:<5}".format("[" + temporalidad + "]")),
            colores(color, monitor_market),
            result_market_color,
        )
    color += 1
    result_market, media_market, monitor_market, result_market_color = VALORACION_MERCADO_GLOBAL['all']
    barra_informativa += "\n{} {:>27} {} ".format(
        colores(color, "{:<5}".format("[all]")),
        colores(color, monitor_market),
        result_market_color,
    )
    print(barra_informativa)
def show_symbol(pair, report_date=False, search=False, date_search=False, id=False, conn=False, group=False, show=True):
    """
    @return Array con identificadores.

    ./src/db.py -sr "[precios:<:500000] and [precios:>:50000] and [precios:>:5000]  and [precios:>:500]  and [precios:>:50]"
    """

    type = 'SPOT'

    if not report_date:
        report_date = '-24 hours'

    ids = []

    buy_price = False

    if id:
        sql_id = "SELECT symbol, date, price FROM market WHERE id == {}".format(id)
        if conn:
            con = conn
        else:
            con = sqlite3.connect(DATABASE)
        cur_id = con.cursor()
        cur_id.execute(sql_id)
        rows_id = cur_id.fetchall()
        for id in rows_id:
            pair = id[0]
            date_search = id[1]
            buy_price = id[2]

    if search:
        pair = 'all'

    if date_search:
        if pair == 'all':
            sql = "SELECT * FROM market WHERE date > datetime('{}','-4 hours') AND date < datetime('{}','+24 hours')".format(date_search, date_search)
        else:
            sql = "SELECT * FROM market WHERE symbol like '{}' AND date > datetime('{}','-4 hours') AND date < datetime('{}','+24 hours')".format(pair, date_search, date_search)

    elif pair == 'all':
        sql = "SELECT * FROM market WHERE date > datetime('now', '{}')".format(report_date)
        if search:
            try:
                sql += " AND " + human2sql(search)
                # sql += " AND " + search
            except Exception as e:
                logger.error(e)
                return False

    else:
        sql = "SELECT * FROM market WHERE symbol like '{}' AND date > datetime('now','{}')".format(pair, report_date)

    if group:
        sql += " " + group

    # print("\n" + sql + "\n")

    try:

        con = sqlite3.connect(DATABASE)
        cur = con.cursor()
        cur.execute(sql)
        rows = cur.fetchall()

        formato = "{:<8} {:<10} {:<18} {:>12} {:>16} {:<7} {:>29}"
        formatoTitle = "{:<8} {:<10} {:<18} {:>10} {:>10} {:<7} {:<19}"

        title = formatoTitle.format(
            'Id',
            'Symbol',
            'Date',
            'Price',
            'Profit',
            'Status',
            'Prices Tenden Volume ADXs   MACDs'
        )
        if show:
            print(title)

        profit = 0
        profitStr = '0'
        for r in rows:

            (
                id,
                date,
                symbol,
                price,
                status,
                mon_precios,
                mon_tendencias,
                mon_volumenes,
                mon_adx,
                mon_macd

            ) = r

            monitor = "{} {} {} {} {}".format(
                mon_precios,
                mon_tendencias,
                mon_volumenes,
                mon_adx,
                mon_macd
            )

            if (pair != 'all' and buy_price):
                profit, profitStr = getProfit(type, price, buy_price)

            status_str = status.split()
            status0 = status_str[0]
            status1 = ''
            if len(status_str) > 1:
                status1 = status_str[1]


            salida = formato.format(
                id,
                symbol,
                date,
                price,
                profitStr,
                "{:<5} {:>1}".format(status0, status1),
                tendencia.color_monitor(monitor)
            )
            if (date and date == date_search):
                if show:
                    print(green(salida))
            elif status.split()[0] == 'OPEN' or profit > 3:
                if show:
                    print(bold(salida))
            else:
                if show:
                    print(salida)

            if not date_search:
                buy_price = price

            ids.append(id)

        con.commit()
        con.close()
        return ids

    except Exception as e:
        logger.error('ERROR: ' + str(e))
        # raise IOError(e)
        print(e)
        con.close()
        return False

def strategy_test(mark, time=False, show_details=False, test_signals=False, test_configs=False, verbose=False, show_only_result=True, test_close_signals=False, without_stoploss=False):

    best_results = {}
    best_min_profit = {}
    best_positivas = {}
    show = False

    config_general = get_config_general()
    estrategias_activadas = config_general['estrategias_activadas']

    comprobar_estrategia = True

    if mark == 'all':
        strategies = strategies_list(False)
    else:
        comprobar_estrategia = False
        strategies = []
        for e in mark.split():
            strategies.append(
                {
                    'mark': e,
                    'id': get_eid(e),
                    'temporalidad': get_strategy(get_eid(e))['temporalidad']
                }
            )

    # Probamos diferentes configuraciones.
    con = db2memo()

    # Recorremos estrategias.
    for strategy in strategies:

        mark = strategy['mark']

        if comprobar_estrategia and mark not in estrategias_activadas.split():
            continue

        max_profit_strategies = False
        min_profit_strategies = False
        max_result_currency = False
        max_positivas = False

        # Recogemos señales de compra de estrategia.
        signals = get_signals(mark)
        if not signals:
            msg = 'Estrategia [{}], Sin señales'.format(mark)
            logger.debug(msg)
            continue

        # Primero la configuración actual de la estrategia.
        configs = []
        # config_actual = get_config(mark, con)
        config_actual = get_config(mark)

        if time is False:
            time_strategy = calcular_tiempo_estrategia(strategy['id'])
        else:
            time_strategy = time

        if show_details and show:
            print("\nEstrategia: " + mark + " time: ", time_strategy)
            strategy_show(strategy['id'])

        # Configuración actual.
        configs.append("min_profit_sell:{} stoploss:{} follow_losses:{} stoptrailing:{}".format(
            config_actual['min_profit_sell'],
            config_actual['stoploss'],
            config_actual['follow_losses'],
            config_actual['stoptrailing'],
        ))

        if test_configs:
            ###
            ### @todo Recoger configuraciones posibles en un fichero para facilitar la modificación.
            ###       Y adaptarlas a la temporalida y apalancamiento.
            ###
            configs.append("min_profit_sell:0.5 stoploss:-1  follow_losses:-0.5 stoptrailing:0;0.5;50")
            configs.append("min_profit_sell:0.5 stoploss:-1  follow_losses:-0.5 stoptrailing:0;0.5;1.5,3;2;2,5;3;50")
            configs.append("min_profit_sell:0.5 stoploss:-1  follow_losses:-0.5 stoptrailing:0;0.5;1.5,1.5;1;50")

        buy_signals = []
        close_signals = []
        close_signals.append([False, False])

        for sig in signals:
            condicion = sig['condicion']
            accion = sig['accion']
            if accion == 'OPEN':
                buy_signals.append([accion, condicion])

        if test_close_signals:
            # Señales test de cierre.
            for sig in signals:
                condicion = sig['condicion']
                accion = sig['accion']
                if accion == 'TCLOS':
                    close_signals.append([accion, condicion])

        if without_stoploss:
            ### Configuraciones que nos sirven para poner a prueba las señales
            ### de cierre de la opración.
            ### Estas configuraciones no sirven para producción ya que pueden
            ### tener un riesgo excesivo.

            # Activamos señales desde el inicio.
            configs.append("min_profit_sell:0.1  stoploss:-50    follow_losses:-0.1  stoptrailing:0;50;50")

        if test_signals:
            # Cogemos señales test de entrada.
            for sig in signals:
                condicion = sig['condicion']
                accion = sig['accion']
                if accion == 'TOPEN':
                    buy_signals.append([accion, condicion])

        for sig in buy_signals:
            accion = sig[0]
            condicion = sig[1]

            if show_details and show:
                print(bold('\n[{}]{}\n'.format(accion, condicion)))

            counter_configs = 1
            for c in configs:
                set_config(mark, c, False, con)

                if show_details and show:
                    print(bold('\n[Config]{}\n'.format(c)))

                operaciones = True

                for close_signal in close_signals:

                    close_action = close_signal[0]
                    close_condition = close_signal[1]

                    if close_condition and show:
                        print(bold("\n[{}]{}\n".format(close_signal[0], close_signal[1])))

                    # Ejecutamos view_result.
                    result = view_results(condicion, mark, time_strategy, verbose, con, show_only_result, close_signal[1])

                    if not result:
                        operaciones = False
                        if show_details and show:
                            print('Sin operaciones')
                        break

                    if max_result_currency is False or result['result_currency'] > max_result_currency:
                        max_result_currency = result['result_currency']
                        best_results[mark] = result
                        best_results[mark]['counter_configs'] = counter_configs

                    if max_profit_strategies is False or result['max_profit'] > max_profit_strategies:
                        max_profit_strategies = result['max_profit']
                        best_results[mark]['max_profit_strategies'] = max_profit_strategies

                    if min_profit_strategies is False or result['min_profit'] > min_profit_strategies:
                        min_profit_strategies = result['min_profit']
                        best_results[mark]['min_profit_strategies'] = min_profit_strategies
                        best_min_profit[mark] = result
                        best_min_profit[mark]['counter_configs'] = counter_configs

                    if max_positivas is False or (result['positivas'] - result['negativas']) > max_positivas:
                        max_positivas = result['positivas'] - result['negativas']
                        best_positivas[mark] = result
                        best_positivas[mark]['counter_configs'] = counter_configs

                    if mark in best_results:
                        best_results[mark]['close_accion'] = close_action
                        best_results[mark]['close_condition'] = close_condition
                        best_results[mark]['accion'] = accion
                        best_results[mark]['condition'] = condicion

                counter_configs += 1

        # Mejores resultados
        if show_details and show:
            print("\n## Mejores MinProfit")
            for b in best_min_profit:
                print_result_trades(best_min_profit[b])
            print("\n## MaxPositivas")
            for b in best_positivas:
                print_result_trades(best_positivas[b])
            print("\n## Mejores resultados")
            for b in best_results:
                print_result_trades(best_results[b])

    return best_results

def comparar_configuraciones(time=False, make_changes=False, test_signals=False, show_details=False, test_configs=False, strategy_mark='all', test_close_signals=False, without_stoploss=False):

    signal(SIGINT, end)

    config_general = get_config_general()
    estrategias_activadas = config_general['estrategias_activadas'].split()

    best_results = strategy_test(strategy_mark, time, show_details, test_signals, test_configs, False, True, test_close_signals, without_stoploss)

    ## El test tiene que dar un minimo de profit para darlo por bueno.
    MIN_PROFIT_TEST = 0

    # print("\n\n### Estrategias activas en configuración general: " + estrategias_activadas)

    # Recorremos estrategias y si no están en best_results desactivamos compras.
    # Cancelamos, solo cambiamos configuraciones.
    # last_two ya hace la función de no comprar si la estrategia
    # pierde en los tests.
    #
    # strategies = strategies_list(False)
    # for strategy in strategies:
    #
    #     if strategy['mark'] not in estrategias_activadas.split():
    #         continue
    #
    #
    #     msg = False
    #
    #     if best_results and strategy['mark'] in best_results and best_results[strategy['mark']]['profit'] >= MIN_PROFIT_TEST:
    #         activar_compra = True
    #     else:
    #         activar_compra = False
    #     if make_changes:
    #         config_strategy = get_config(strategy['mark'])
    #         if activar_compra:
    #             if not config_strategy['compras_activadas']:
    #                 set_config(strategy['mark'], 'compras_activadas:1')
    #                 msg = 'Activamos compras para [{}]'.format(strategy['mark'])
    #         else:
    #             if config_strategy['compras_activadas']:
    #                 set_config(strategy['mark'], 'compras_activadas:0')
    #                 msg = 'Desactivamos compras para [{}]'.format(strategy['mark'])
    #     if msg:
    #         print("\n" + msg)
    #         tlg_send(msg)

    the_best = False
    the_best_profit = MIN_PROFIT_TEST

    # Mejor estrategia actual.
    for e in best_results:
        if best_results[e]['result_currency'] > the_best_profit:
            the_best_profit = best_results[e]['result_currency']
            the_best = e

    if the_best:
        print("\nMejor estrategia: [{}]\n".format(the_best))

    for e in estrategias_activadas:

        if e in best_results:
            print_result_trades(best_results[e])

        if make_changes:

            if without_stoploss:
                print('No podemos modificar automáticamente las estrategis sin stoploss activado')
                continue

            new_config = False

            # Cambio de configuración.
            if e in best_results and best_results[e]['counter_configs'] != 1:

                new_config = "min_profit_sell:{} follow_losses:{} stoploss:{} stoptrailing:{}".format(
                    best_results[e]['min_profit_sell'],
                    best_results[e]['follow_losses'],
                    best_results[e]['stoploss'],
                    best_results[e]['stoptrailing']
                )

            if new_config:

                msg = "\nCambio de configuración para {}\n".format(e)
                msg += "\n" + new_config
                print(msg)
                set_config(e, new_config)
                eventos('CHANGE_CONFIG', msg)

            # Cambio de señales.
            if e in best_results and best_results[e]['accion'] == 'TOPEN':

                msg = "Cambiamos señal activa de estrategia [{}]".format(e)
                msg += "\n[{}]{}".format(best_results[e]['accion'], best_results[e]['condition'])
                print(msg)
                change_active(e, best_results[e]['condition'])
                eventos('CHANGE_ACTIVE_SIGNAL', msg)

def calcular_tiempo_estrategia(eid, conn=False):

    if not conn:
        con = sqlite3.connect(DATABASE)

    sql = """
    SELECT round(avg(minutes)) as minutes
    FROM trade_result WHERE eid = {} AND NOT date_sell IS NULL AND date > datetime('now', '-24 hours') ORDER By date desc
    """.format(eid)

    cur = con.cursor()
    cur.execute(sql)
    result = cur.fetchone()
    minutes = result[0]
    if not minutes:
        return TIME_TEST[get_strategy(eid)['temporalidad']]

    minutes = minutes + ( minutes / 2 )
    minutes = '-' + str(round(minutes)) + ' minutes'
    return minutes

def view_results(condicionH, mark=False, tiempo=False, show=True, conn=False, show_only_result=False, close_condition=False, manual_config=False):

    type = 'SPOT'

    config_general = get_config_general()
    strategies = config_general['estrategias_activadas']

    if not tiempo:
        tiempo = '-7 days'

    maximos_items = 100
    items_tras_venta = 50

    # con = sqlite3.connect(DATABASE)
    if not conn:
        con = db2memo()
    else:
        con = conn

    condicion = human2sql(condicionH)  # + " AND date < datetime('now','-24hours')"

    if not mark:
        mark = 'M'
        if manual_config:
            print("\n## Configuración manual: {}\n".format(manual_config))
            set_config(mark, manual_config, False, con)
        else:
            print("\n## Recogemos configuración de {} por falta de una.\n".format(mark))

    config = get_config(mark, con)
    strategy = get_strategy(get_eid(mark))

    ids = show_symbol('all', tiempo, condicionH, False, False, con, 'GROUP BY symbol', show)

    if not ids:
        return False

    if show:
        print("\nView results [{}] ({})\n".format(len(ids), condicionH))

    try:

        trades = 0
        positivas = 0
        negativas = 0
        resultado = 0
        tipo_venta = {}
        total_tipo_venta = {}
        total_result_currency = 0
        pair = False
        counter_items = 0
        suma_max_profit = 0
        max_profit_general = 0
        min_profit_general = 0
        total_profit_sin_venta = 0
        total_stop_sin_venta = 0

        # Recorremos señales de compra.
        for i in ids:
            counter_items += 1
            if counter_items > maximos_items:
                break
            trades    += 1
            sql_id = "SELECT symbol, date, price FROM market WHERE id == {}".format(i)
            cur_id = con.cursor()
            cur_id.execute(sql_id)
            rows_id = cur_id.fetchall()
            for identificador in rows_id:
                pair = identificador[0]
                date_search = identificador[1]
                buy_price = identificador[2]

            if pair is False:
                continue

            if show:
                print("\n", '## pair:', pair, 'date_search:', date_search, 'buy_price:', buy_price, "\n")

            sql = "SELECT * FROM market WHERE symbol like '{}' AND date >= '{}'".format(pair, date_search)

            cur = con.cursor()
            cur.execute(sql)
            rows = cur.fetchall()

            if len(rows) < 3:
                if show:
                    print('Sin datos suficientes')
                continue

            resultado = 0
            result_currency = 0
            max_profit = 0
            min_profit = 0
            formato = "{:<8} {:<10} {:<18} {:>12} {:>16} {:<7} {:>29} MaxPr[{}] Stop[{}] {}"
            tras_la_venta = 0
            counter_market = len(rows)
            stoploss = False
            operacion_cerrada = False
            ## Mostramos las siguientes filas tras una venta para poder ver como evoluciona el precio.
            filas_tras_venta = 10

            # Recorremos datos del mercado.
            for r in rows:

                (
                    id,
                    date,
                    symbol,
                    price,
                    status,
                    mon_precios,
                    mon_tendencias,
                    mon_volumenes,
                    mon_adx,
                    mon_macd

                ) = r

                monitor = "{} {} {} {} {}".format(
                    mon_precios,
                    mon_tendencias,
                    mon_volumenes,
                    mon_adx,
                    mon_macd
                )

                counter_market -= 1

                profit, profitStr = getProfit(strategy['type'], price, buy_price)

                if not operacion_cerrada:
                    if profit > 0:
                        if profit > max_profit:
                            max_profit = profit

                    if profit < min_profit:
                        min_profit = profit

                venta_status = tendencia.valorar_datos(
                    mon_macd,
                    mon_tendencias,
                    mon_precios,
                    mon_volumenes,
                    mon_adx,
                    mark,
                    con,
                    close_condition
                )

                stoploss = calculo_stoptrailing(config['stoptrailing'], profit, stoploss)

                venta = tendencia.valorar_venta(symbol, profit, venta_status.split()[0], stoploss, mark, con, True)
                status = venta_status

                # print("{} {} {} Vender: {}".format(symbol, date, profitStr, venta))

                if venta and not operacion_cerrada:
                    venta_str = venta
                    operacion_cerrada = True
                    resultado = profit
                else:
                    # No se ha llegado a vender.
                    venta_str = ''


                if counter_market == len(rows) - 1:
                    venta_str = 'OPENHERE'

                salida = formato.format(
                    id,
                    symbol,
                    date,
                    price,
                    profitStr,
                    "{:<5} {:>1}".format(status.split()[0], status.split()[1]),
                    tendencia.color_monitor(monitor),
                    str(round(max_profit, 2)),
                    str(round(stoploss, 2)),
                    venta_str
                )

                if show:
                    print(salida)

                if operacion_cerrada:
                    filas_tras_venta -= 1

                if filas_tras_venta < 1:
                    break

            if not operacion_cerrada:

                resultado = stoploss
                total_profit_sin_venta += profit
                total_stop_sin_venta += stoploss


            result_currency = ((config['amount_buy'] * strategy['leverage']) * resultado) / 100
            if resultado > 0:
                positivas += 1
            else:
                negativas += 1
            # Tipo de venta.
            if venta in tipo_venta:
                tipo_venta[venta] += 1
            else:
                tipo_venta[venta] = 1

            # Totales en tipo de venta.
            if venta in total_tipo_venta:
                total_tipo_venta[venta] += resultado
            else:
                total_tipo_venta[venta] = resultado

            total_result_currency += result_currency

            suma_max_profit += max_profit
            if max_profit > max_profit_general:
                max_profit_general = max_profit
            if min_profit < min_profit_general:
                min_profit_general = min_profit

            if show:
                print("\n\nAmountBuy: {} Leverage: {} Resultado: {} ResultCurrency: {} MaxProfit: {} MinProfit: {}".format(
                    config['amount_buy'],
                    strategy['leverage'],
                    color_result(round(resultado, 2)),
                    color_result(round(result_currency, 2)),
                    round(max_profit,2),
                    round(min_profit, 2)
                    )
                )

        con.commit()
        if not conn:
            con.close()

        result = {
            'strategy': mark,
            'type': strategy['type'],
            'leverage': strategy['leverage'],
            'time': tiempo,
            'condicion': condicionH,
            'close_condition': close_condition,
            'operaciones': len(ids),
            'positivas': positivas,
            'negativas': negativas,
            'profit': round(resultado, 2),
            'stoptrailing': config['stoptrailing'],
            'min_profit_sell': config['min_profit_sell'],
            'follow_losses': config['follow_losses'],
            'stoploss': config['stoploss'],
            'tipo_ventas': tipo_venta,
            'total_tipo_ventas': total_tipo_venta,
            'max_profit': max_profit_general,
            'min_profit': min_profit_general,
            'result_currency': total_result_currency,
            'total_profit_sin_venta': total_profit_sin_venta,
            'total_stop_sin_venta': total_stop_sin_venta
        }

        if show or show_only_result:
            print_result_trades(result)
        return result

    except Exception as e:
        logger.error(e)
        raise IOError(e)
        print(e)
        con.close()
        return False

def print_result_trades(result):

            # pprint.pprint(result)
            if 'counter_configs' in result:
                actual = red('No')
                if result['counter_configs'] == 1:
                    actual = green('Si')
                config_actual_str = " Configuración actual [{}]".format(actual)
            else:
                config_actual_str = ""

            print('\nEstrategia: {} Type: {} Leverage: {} Resultado en {}: {} Tiempo: {} {}'.format(
                result['strategy'],
                result['type'],
                result['leverage'],
                CURRENCY,
                color_result(round(result['result_currency'], 2), '$'),
                result['time'],
                config_actual_str
                )
            )

            if 'accion' in result:
                print('Señal de entrada: [{}]{}'.format(result['accion'], result['condicion']))
            else:
                print('Señal de entrada: {}'.format(result['condicion']))

            if 'close_condition' in result and result['close_condition']:
                if 'close_accion' in result:
                    print('Señal de cierre: [{}]{}'.format(result['close_accion'], result['close_condition']))
                else:
                    print('Señal de cierre: {}'.format(result['close_condition']))

            print('min_profit_sell:{} stoploss:{} follow_losses:{} stoptrailing:{}'.format(
                result['min_profit_sell'],
                result['stoploss'],
                result['follow_losses'],
                result['stoptrailing']
            ))
            print("Operaciones:{} Positivas:{} Negativas:{} ".format(bold(result['operaciones']), green(result['positivas']), red(result['negativas'])))
            s = 'Ventas: '
            for v in result['tipo_ventas']:
                s += "{}:{} ".format(v, color_result(result['tipo_ventas'][v], '%'))
            print(s)
            print('Totales por tipo de ventas')
            for v in result['total_tipo_ventas']:
                print(" {}:{}".format(v, color_result(result['total_tipo_ventas'][v], '%')))
            if result and (result['positivas'] or result['negativas']):
                print("Media general:{}".format(
                    color_result(result['profit'] / result['operaciones'], '%'),
                    )
                )

            print("MaxProfit:{} MinProfit:{}".format(
                color_result(result['max_profit'], '%'),
                color_result(result['min_profit'], '%')
                )
            )
            print("SinVentas: {:.2f} StopSinVentas: {:<.2f}".format(result['total_profit_sin_venta'], result['total_stop_sin_venta']))

            if 'max_profit_strategies' in result:
                print("MaxProfitStrategies: {}".format(color_result(result['max_profit_strategies'], '%')))

            if 'min_profit_strategies' in result:
                print("MinProfitStrategies: {}".format(color_result(result['min_profit_strategies'], '%')))

def db_trade(id, test=False, telegram=False):
    """
    Mostrar operación en detalle, con su evolución.
    """
    con = sqlite3.connect(DATABASE)
    telegram_salida = ""
    info_sell = 'Sin Venta'

    table = 'trade_result'
    if test:
        table = 'test_trade_result'

    sql_where = "WHERE id = {}".format(id)

    sql = "SELECT * FROM {} {}".format(table, sql_where)

    formatoTitle = "{:<4} {:<4} {:1} {:<5} {:<2} {:>8} {:>3} {:>8} {:>6} {:>6} {} {}"

    title = formatoTitle.format(
        'Id',
        'Coin',
        'S',
        'Type',
        'Tp',
        'ePrice',
        'Min',
        'Amount $',
        'MaxPr',
        'MinProf',
        'Profit',
        'Rt',

    )

    if telegram:
        telegram_salida += title
    else:
        print(title)


    try:

        rows = sql_exec(sql, False, False, True)

        total_profit = 0
        total_result = 0

        formato2 = "{:<8} {} {} {:<10} {:<11}"
        if telegram:
            formato2 = "\n{:<8} {} {:<10} {:<11}"

        for r in rows:

            if r['status_sell']:
                status_sell = r['status_sell']
            else:
                status_sell = 'SinVenta'

            if not r['mon_precios_sell']:
                mon_precios_sell = False
            else:
                mon_precios_sell = r['mon_precios_sell']

            if not r['info_sell']:
                info_sell = 'SinVenta'
            else:
                info_sell = r['info_sell']

            if not r['date_sell']:
                date_sell = 'SinVenta'
            else:
                date_sell = r['date_sell']

            if not r['min_profit']:
                min_profit = 'SinVenta'
            else:
                min_profit = r['min_profit']

            if not r['max_profit']:
                max_profit = 'SinVenta'
            else:
                max_profit = r['max_profit']

            monitor = "{} {} {} {} {}".format(
                r['mon_precios'],
                r['mon_tendencias'],
                r['mon_volumenes'],
                r['mon_adx'],
                r['mon_macd']
            )
            if mon_precios_sell:
                monitor_sell = "{} {} {} {} {}".format(
                    r['mon_precios_sell'],
                    r['mon_tendencias_sell'],
                    r['mon_volumenes_sell'],
                    r['mon_adx_sell'],
                    r['mon_macd_sell']
                )
            else:
                monitor_sell = ""

            # salida1 = formatoTitle.format(
            if mon_precios_sell:
                formato1 = formatoTitle
                profit = r['profit']
                resultat = r['resultd']
                profitStr = str(round(r['profit'], 2)) + '%'
                resultatStr = str(resultat) + '$'
                dt_object_sell = datetime.strptime(date_sell, '%Y-%m-%d %H:%M:%S')
                dt_object_sell = dt_object_sell.strftime("%d/%m %H:%M")
            else:
                formato1 = "{:<4} {:<4} {} {} {} {:>8} {} {} {:>7} {} {}"
                profit = 0
                resultat = 0
                profitStr = ''
                resultatStr = ''
                dt_object_sell = ''

            total_profit += profit
            total_result += resultat

            dt_object_buy = datetime.strptime(r['date'], '%Y-%m-%d %H:%M:%S')
            dt_object_buy = dt_object_buy.strftime("%d/%m %H:%M")

            if telegram:
                profitStr = profitStr
                resultatStr = resultatStr
            elif profit < 0:
                profitStr = red(profitStr)
                resultatStr = red(resultatStr)
            else:
                profitStr = green(profitStr)
                resultatStr = green(resultatStr)

            strategy = get_strategy(r['eid'])
            mark = strategy['mark']
            salida1 = formato1.format(
                r['id'],
                r['symbol'].replace('/' + CURRENCY, ''),
                mark,
                r['type'],
                strategy['temporalidad'],
                r['price'],
                r['minutes'],
                round(r['amount'] * r['price'], 2),
                max_profit,
                min_profit,
                profitStr,
                resultatStr
            )
            salida1 += "\n"

            if telegram:
                salida2 = formato2.format(
                    r['status'],
                    monitor,
                    round(r['price'], 5),
                    r['info'],
                )
            else:
                salida2 = formato2.format(
                    green(r['status']),
                    tendencia.color_monitor(monitor),
                    dt_object_buy,
                    round(r['price'], 5),
                    green(r['info']),
                )
            if telegram:
                salida3 = formato2.format(
                    status_sell,
                    monitor_sell,
                    round(r['price_sell'], 5),
                    info_sell
                )
            else:
                salida3 = formato2.format(
                    status_sell,
                    tendencia.color_monitor(monitor_sell),
                    dt_object_sell,
                    round(r['price_sell'], 5),
                    info_sell
                )

            # Mostrar configuración en el momento de la venta.
            salida1 += "\n{}".format(show_config(mark, False, r['date_sell']))



            if telegram:
                telegram_salida += "\n" + salida1
                telegram_salida += "\n{:<8} {:<6} {:<6} {:<6} {:<6} {:<6} {}".format(
                    'Status',
                    'Prices',
                    'Tendes',
                    'Volums',
                    'ADXs',
                    'MACDs',
                    'Price',
                    'Info/Profit'
                )
            else:
                print(salida1)

                print("\n{:<8} {:<6} {:<6} {:<6} {:<6} {:<6} {:<11} {:<10} {}".format(
                    'Status',
                    'Prices',
                    'Tendes',
                    'Volums',
                    'ADXs',
                    'MACDs',
                    'Date',
                    'Price',
                    'Info/Profit'
                ))

            # Buscar datos de market de una hora antes.
            sql_market = "SELECT * FROM market where symbol like '{}' and date > datetime('{}','-5 hours') AND date < '{}' GROUP BY strftime('%H', date)".format(
                r['symbol'],
                r['date'],
                r['date'],
                r['date']
            )
            rows_market = sql_exec(sql_market, False, False, True)
            for r_market in rows_market:
                date_market = r_market['date']
                price_market = r_market['price']
                status_market = r_market['status']
                mon_precios_market = r_market['mon_precios']
                mon_tendencias_market = r_market['mon_tendencias']
                mon_volumenes_market = r_market['mon_volumenes']
                mon_adx_market = r_market['mon_adx']
                mon_macd_market = r_market['mon_macd']

                date_object_market = datetime.strptime(date_market, '%Y-%m-%d %H:%M:%S')
                date_object_market = date_object_market.strftime("%d/%m %H:%M")
                profit_market, profit_market_str = getProfit(r['type'], price_market, r['price'])

                monitor_market = "{} {} {} {} {}".format(
                    mon_precios_market,
                    mon_tendencias_market,
                    mon_volumenes_market,
                    mon_adx_market,
                    mon_macd_market
                )
                if telegram:
                    telegram_salida += "\n" + "{:<8} {} {:<10} {}".format(
                        status_market,
                        monitor_market,
                        price_market,
                        profit_market
                    )
                else:
                    print("{:<8} {} {} {:<10} {}".format(
                        status_market,
                        tendencia.color_monitor(monitor_market),
                        date_object_market,
                        price_market,
                        profit_market_str
                    ))

            if telegram:
                telegram_salida += salida2
            else:
                print(salida2)

            # Buscar datos de market entre la compra y la venta.
            sql_market = "SELECT * FROM market where symbol like '{}' and date > '{}' and date < '{}' GROUP BY strftime('%H', date)".format(
                r['symbol'],
                r['date'],
                date_sell
            )
            cur_market = con.cursor()
            cur_market.execute(sql_market)
            rows_market = cur_market.fetchall()
            for r_market in rows_market:
                (
                    id_market,
                    date_market,
                    symbol_market,
                    price_market,
                    status_market,
                    mon_precios_market,
                    mon_tendencias_market,
                    mon_volumenes_market,
                    mon_adx_market,
                    mon_macd_market
                ) = r_market
                date_object_market = datetime.strptime(date_market, '%Y-%m-%d %H:%M:%S')
                date_object_market = date_object_market.strftime("%d/%m %H:%M")
                profit_market, profit_market_str = getProfit(r['type'], price_market, r['price'])
                monitor_market = "{} {} {} {} {}".format(
                    mon_precios_market,
                    mon_tendencias_market,
                    mon_volumenes_market,
                    mon_adx_market,
                    mon_macd_market
                )

                if telegram:
                    telegram_salida += "\n{:<8} {} {:<10} {}".format(
                        status_market,
                        monitor_market,
                        price_market,
                        profit_market

                    )
                else:
                    print("{:<8} {} {} {:<10} {}".format(
                        status_market,
                        tendencia.color_monitor(monitor_market),
                        date_object_market,
                        price_market,
                        profit_market_str

                    ))

            if telegram:
                telegram_salida += salida3
            else:
                print(salida3)

            # Buscar datos de market tras la venta.
            # Agrupado por hora es poca información.
            # sql_market = "SELECT * FROM market where symbol like '{}' and date > '{}' GROUP BY strftime('%H', date) LIMIT 5".format(
            sql_market = "SELECT * FROM market where symbol like '{}' and date > '{}' LIMIT 15".format(
                r['symbol'],
                date_sell
            )
            cur_market = con.cursor()
            cur_market.execute(sql_market)
            rows_market = cur_market.fetchall()
            for r_market in rows_market:
                (
                    id_market,
                    date_market,
                    symbol_market,
                    price_market,
                    status_market,
                    mon_precios_market,
                    mon_tendencias_market,
                    mon_volumenes_market,
                    mon_adx_market,
                    mon_macd_market
                ) = r_market

                date_object_market = datetime.strptime(date_market, '%Y-%m-%d %H:%M:%S')
                date_object_market = date_object_market.strftime("%d/%m %H:%M")
                profit_market, profit_market_str = getProfit(r['type'], price_market, r['price'])

                monitor_market = "{} {} {} {} {}".format(
                    mon_precios_market,
                    mon_tendencias_market,
                    mon_volumenes_market,
                    mon_adx_market,
                    mon_macd_market
                )

                if telegram:
                    telegram_salida += "\n{:<8} {} {:<10} {}".format(
                        status_market,
                        monitor_market,
                        price_market,
                        profit_market
                    )
                else:
                    print("{:<8} {} {} {:<10} {}".format(
                        status_market,
                        tendencia.color_monitor(monitor_market),
                        date_object_market,
                        price_market,
                        profit_market_str
                    ))
        con.commit()
        con.close()

        if telegram:
            print(telegram_salida)
            return telegram_salida

        return rows

    except Exception as e:
        logger.error(e)
        raise IOError(e)
        con.close()
        return False

def db_trades(symbol, test=False, telegram=False, report_date=False, only_return=False, strategy=False):

    if report_date is False:
        report_date = '-24 hours'

    print("\nReport {} strategy:{} Test:{} Report Date:{}\n".format(symbol, strategy, test, report_date))
    telegram_salida = "\nReport {} strategy:{} Test:{} Report Date:{}\n".format(symbol, strategy, test, report_date)

    if telegram:
        formato = "{:<4} {:<2} {:<5} {:>7} {:>7} {}"
        formatoTitle = "{:<4} {:<2} {:<5} {:>7} {:>7} {}"
        title = formatoTitle.format(
            'Id',
            'M.',
            'Coin',
            'Profit',
            'Result',
            'Info Sell'
        )
    else:
        formato = "{:<4} {:<2} {:5} {:2} {:<5} {:<11} {:>10} {:>10} {:>10} {:>16} {:>16} {:<10} {:>16} {:>18} {:>6} {:<2} {}"
        formatoTitle = "{:<4} {:<2} {:5} {:2} {:<5} {:<11} {:>10} {:>10} {:>10} {:>7} {:>7} {:<11} {:>7} {:>9} {} {} {}"
        title = formatoTitle.format(
            'Id',
            'M.',
            'Type',
            'LV',
            'Coin',
            'DateSell',
            'Amount',
            'Amount $',
            'Price',
            'Profit',
            'Result',
            'DateBuy',
            'MaxProf',
            'MinProf',
            ' HH:MM',
            'SM',
            'Info Buy/Sell'
        )

    # limit = 100

    table = 'trade_result'
    if test:
        table = 'test_trade_result'

    sql_where = "WHERE date_sell IS NOT NULL AND price_sell IS NOT NULL"
    sql_order = "ORDER BY date_sell asc"

    if symbol != 'all':
        sql_where += " AND symbol like '{}'".format(symbol)

    if strategy:
        sql_where += " AND eid == {}".format(get_eid(strategy))

    if FILTRO_INFORMES_HUMAN:
        condicion = human2sql(FILTRO_INFORMES_HUMAN, True)
        sql_where += " AND " + condicion

    if FILTRO_INFORMES:
        # condicion = human2sql(FILTRO_INFORMES, False)
        sql_where += " AND " + FILTRO_INFORMES

    sql = "SELECT * FROM {} {} AND date_sell > datetime('now', '{}') {}".format(table, sql_where, report_date, sql_order)

    sql_dias = """
    SELECT strftime('%Y-%m-%d', date_sell) as day, avg(profit) as profit, sum(resultd) as result
    from {} {} AND date_sell > datetime('now', '{}') group by day {}
    """.format(
        table,
        sql_where,
        report_date,
        sql_order
    )

    sql_estrategias = """
    SELECT eid,
        avg(profit) as profit,
        sum(resultd) as result,
        avg(minutes) as minutes,
        mark
    from {} {} AND date_sell > datetime('now', '{}') group by eid order by mark
    """.format(
        table,
        sql_where,
        report_date,
    )

    if telegram:
        telegram_salida += title
    else:
        print(title)

    try:

        rows = sql_exec(sql, False, False, True)

        total_profit = 0
        total_result = 0
        total_dia_result = 0
        if rows:
            count_total = len(rows)
        else:
            count_total = 0

        if only_return:
            return rows

        if not rows or len(rows) == 0:
            return False

        for r in rows:

            info_sell = r['info']
            if r['info_sell']:
                info_sell += ' / ' + r['info_sell']
            else:
                info_sell += ' / SinVenta'

            profit = r['profit']
            if profit:
                resultat = r['resultd']
                profitStr = str(round(profit, 2)) + '%'
                resultatStr = str(resultat) + '$'
            else:
                profit = 0
                resultat = 0
            total_profit += profit
            total_result += resultat

            dt_object_sell = datetime.strptime(r['date_sell'], '%Y-%m-%d %H:%M:%S')
            dt_object_sell = dt_object_sell.strftime("%d/%m %H:%M")
            dt_object_buy = datetime.strptime(r['date'], '%Y-%m-%d %H:%M:%S')
            dt_object_buy = dt_object_buy.strftime("%d/%m %H:%M")

            if telegram:
                profitStr = profitStr
                resultatStr = resultatStr
            elif profit < 0:
                profitStr = red(profitStr)
                resultatStr = red(resultatStr)
            else:
                profitStr = green(profitStr)
                resultatStr = green(resultatStr)

            if not r['max_profit']:
                max_profit = False
            else:
                max_profit = r['max_profit']
            if not r['min_profit']:
                min_profit = False
            else:
                min_profit = r['min_profit']

            if not r['status_market']:
                status_market = 0
            else:
                status_market = r['status_market']

            if telegram:
                salida = formato.format(
                    r['id'],
                    r['mark'],
                    r['symbol'].replace('/' + CURRENCY, ''),
                    profitStr,
                    resultatStr,
                    info_sell
                )
                telegram_salida += "\n" + salida
            else:
                salida = formato.format(
                    r['id'],
                    r['mark'],
                    r['type'],
                    r['leverage'],
                    r['symbol'].replace('/' + CURRENCY, ''),
                    dt_object_sell,
                    round(r['amount'], 3),
                    round((r['amount'] * r['price']) / r['leverage'], 2),
                    round(r['price'], 3),
                    profitStr,
                    resultatStr,
                    dt_object_buy,
                    color_result(max_profit),
                    color_result(min_profit),
                    min2hours(r['minutes']),
                    status_market,
                    str(info_sell)

                )
                print(salida)

        if telegram:
            total_profitStr = str(round(total_profit, 2)) + '%'
        elif total_profit > 0:
            total_profitStr = green(str(round(total_profit, 2)) + '%')
        else:
            total_profitStr = red(str(round(total_profit, 2)) + '%')

        if telegram:
            total_resultStr = str(round(total_result, 2)) + '$'
        elif total_result > 0:
            total_resultStr = green(str(round(total_result, 2)) + '$')
        else:
            total_resultStr = red(str(round(total_result, 2)) + '$')

        comisiones = round(((count_total * 100) * 0.0005), 2)

        # Totales.
        salida_total = "\nNumero: {} Total profit: {} Total resultado: {} Comisiones: {}".format(
            count_total,
            total_profitStr,
            total_resultStr,
            comisiones
        )

        if telegram:
            telegram_salida += "\n" + salida_total
        else:
            print(salida_total)

        # Agrupado por días.
        total_dia_profit = 0
        total_dia_result = 0
        rows = sql_exec(sql_dias, False, False, True)
        if rows and len(rows) > 0:
            if telegram:
                telegram_salida += "\n\nPor días\n"
                telegram_salida += "\n{:<10} {:>10} {:>10}".format('Día', 'Profit AVG', 'Result')
            else:
                print("\n{:<10} {:>10} {:>10}".format('Día', 'Profit AVG', 'Result'))

            for r in rows:
                profit_dia = r['profit']
                result_dia = r['result']
                if telegram:
                    profit_diaStr = str(round(profit_dia, 2)) + '%'
                elif profit_dia > 0:
                    profit_diaStr = green(str(round(profit_dia, 2)) + '%')
                else:
                    profit_diaStr = red(str(round(profit_dia, 2)) + '%')
                if telegram:
                    result_diaStr = str(round(result_dia, 2)) + '$'
                elif result_dia > 0:
                    result_diaStr = green(str(round(result_dia, 2)) + '$')
                else:
                    result_diaStr = red(str(round(result_dia, 2)) + '$')
                if telegram:
                    salida = "{:<10} {:>10} {:>10}".format(
                        r['day'],
                        profit_diaStr,
                        result_diaStr
                    )
                    telegram_salida += "\n" + salida
                else:
                    salida = "{:<10} {:>19} {:>19}".format(
                        r['day'],
                        profit_diaStr,
                        result_diaStr
                    )
                    print(salida)

                total_dia_result += result_dia
                total_dia_profit += profit_dia

            if telegram:
                salida_barra = "\n{:<10} {:>9}% {:>9}$".format('Total', round(total_dia_profit, 2), round(total_dia_result, 2))
                telegram_salida += "\n" + salida_barra
                # return telegram_salida
            else:
                salida_barra = "{:<10} {:>9}% {:>9}$".format('Total', round(total_dia_profit, 2), round(total_dia_result, 2))
                print(barra(salida_barra))

        # Agrupado por estrategias.
        # cur.execute(sql_estrategias)
        # rows = cur.fetchall()
        rows = sql_exec(sql_estrategias, False, False, True)
        if rows and len(rows) > 0:
            if telegram:
                telegram_salida += "\n{:<10} {:>10} {:<4} {:>10}".format('Mark', 'Profit AVG', ' HH:MM', 'Result')
            else:
                print("\n{:<10} {:>10} {:<4} {:>10}".format('Mark', 'Profit AVG', ' HH:MM', 'Result'))
            total_dia_profit = 0
            total_dia_result = 0
            for r in rows:
                profit_dia = r['profit']
                result_dia = r['result']
                minutes = r['minutes']
                if telegram:
                    profit_diaStr = str(round(profit_dia, 2)) + '%'
                elif profit_dia > 0:
                    profit_diaStr = green(str(round(profit_dia, 2)) + '%')
                else:
                    profit_diaStr = red(str(round(profit_dia, 2)) + '%')
                if telegram:
                    result_diaStr = str(round(result_dia, 2)) + '$'
                elif result_dia > 0:
                    result_diaStr = green(str(round(result_dia, 2)) + '$')
                else:
                    result_diaStr = red(str(round(result_dia, 2)) + '$')
                if telegram:
                    salida = "{:<10} {:>10} {:>4} {:>10}".format(
                        get_strategy(r['eid'])['mark'],
                        profit_diaStr,
                        min2hours(round(minutes)),
                        result_diaStr
                    )
                    telegram_salida += "\n" + salida
                else:
                    salida = "{:<10} {:>19} {:>4} {:>19}".format(
                        get_strategy(r[0])['mark'],
                        profit_diaStr,
                        min2hours(round(minutes)),
                        result_diaStr
                    )
                    print(salida)

                total_dia_result += r['result']
                total_dia_profit += r['profit']

            if telegram:
                salida_barra = "\n{:<10} {:>9}% {:>9}$".format('Total', round(total_dia_profit, 2), round(total_dia_result, 2))
                telegram_salida += "\n" + salida_barra
                print(telegram_salida)
                return telegram_salida
            else:
                salida_barra = "{:<10} {:>9}% {:>9}$".format('Total', round(total_dia_profit, 2), round(total_dia_result, 2))
                print(barra(salida_barra))

        if AGRUPAR_INFORMES:
            sql_agrupar = """
            SELECT avg(profit) as profit,
                sum(resultd) as result,
                avg(minutes) as minutes,
                {}
            from {} {} AND date_sell > datetime('now', '{}') group by {} {}
            """.format(
                AGRUPAR_INFORMES,
                table,
                sql_where,
                report_date,
                AGRUPAR_INFORMES,
                sql_order
            )
            # AGRUPAR_INFORMES.
            # cur.execute(sql_estrategias)
            # rows = cur.fetchall()
            rows = sql_exec(sql_agrupar, False, False, True)
            if rows and len(rows) > 0:
                if telegram:
                    telegram_salida += "\n{:<10} {:>4} {:<10} {}".format('Profit AVG', ' HH:MM', 'Result', 'Agr.')
                else:
                    print("\n{:<6} {:>4} {:<7} {}".format('ProAVG', ' HH:MM', ' Result', 'Agr.'))
                total_dia_profit = 0
                total_dia_result = 0
                for r in rows:
                    profit_dia = r['profit']
                    result_dia = r['result']
                    minutes = r['minutes']
                    agrupar_field = ''.join(str(r[3:]))
                    if telegram:
                        profit_diaStr = str(round(profit_dia, 2)) + '%'
                    elif profit_dia > 0:
                        profit_diaStr = green(str(round(profit_dia, 2)) + '%')
                    else:
                        profit_diaStr = red(str(round(profit_dia, 2)) + '%')
                    if telegram:
                        result_diaStr = str(round(result_dia, 2)) + '$'
                    elif result_dia > 0:
                        result_diaStr = green(str(round(result_dia, 2)) + '$')
                    else:
                        result_diaStr = red(str(round(result_dia, 2)) + '$')
                    if telegram:
                        salida = "{:>10} {:>10} {:>4} {:>10}".format(
                            profit_diaStr,
                            min2hours(round(minutes)),
                            result_diaStr,
                            agrupar_field,
                        )
                        telegram_salida += "\n" + salida
                    else:
                        salida = "{:>15} {:>4} {:>16} {}".format(
                            profit_diaStr,
                            min2hours(round(minutes)),
                            result_diaStr,
                            agrupar_field,
                        )
                        print(salida)

                    total_dia_result += r['result']
                    total_dia_profit += r['minutes']

                if telegram:
                    salida_barra = "\n{:<10} {:>9}% {:>9}$".format('Total', round(total_dia_profit, 2), round(total_dia_result, 2))
                    telegram_salida += "\n" + salida_barra
                    print(telegram_salida)
                    return telegram_salida
                else:
                    salida_barra = "{:<10} {:>9}% {:>9}$".format('Total', round(total_dia_profit, 2), round(total_dia_result, 2))
                    print(barra(salida_barra))

        estadisticas_informe(sql, 'mark', True)

        return rows

    except Exception as e:
        print(e)
        # raise IOError(e)
        logger.error(e)
        return False

def estadisticas_informe(sql, groupby=False, show=False):
    ### @todo
    ### Agrupamos sql de trade_result o trade_result_test con diferentes
    ### números interesantes.

    if groupby:
        sql_field = "{}, ".format(groupby)
    else:
        sql_field = ""

    sql_informe = """
    SELECT {}SUM(CASE WHEN profit > 0 THEN 1 ELSE 0 END) AS 'positive',
        SUM(CASE WHEN profit <= 0 THEN 1 ELSE 0 END) AS 'negative',
        COUNT(id) as total,
        SUM(CASE WHEN stoploss > 0 THEN 1 ELSE 0 END) as stoplosses
    from ({})
    """.format(sql_field, sql)

    result = {}
    if groupby:
        sql_informe += " GROUP BY {}".format(groupby)

    # AGRUPAR_INFORMES.
    # cur.execute(sql_estrategias)
    # rows = cur.fetchall()
    rows = sql_exec(sql_informe, False, False, True)
    if rows and len(rows) > 0:
        color = 1
        for r in rows:
            if r['positive'] and r['negative']:
                relacion = r['positive'] - r['negative']
            elif r['positive']:
                relacion = r['positive']
            elif r['negative']:
                relacion = (0 - r['negative'])
            else:
                relacion = 0

            if groupby:
                salida_field = r[groupby] + ' '
                salida_field_str = colores(color, salida_field)
                ix = groupby
            else:
                salida_field_str = ""
                ix = 'x'

            result[ix] = {}
            result[ix]['total'] = r['total']
            result[ix]['positivas'] = r['positive']
            result[ix]['negativas'] = r['negative']
            result[ix]['relacion'] = relacion

            if show:
                print("{}Total: {} Positivas: {} Negativas: {} Relación: {} Aseguradas: {}".format(
                    salida_field_str,
                    colores(color, r['total']),
                    colores(color, r['positive']),
                    colores(color, r['negative']),
                    color_result(relacion),
                    color_result(r['stoplosses'])
                    )
                )
            color += 1

        return result

def best_results(min_profit=3):
    """
    Buscar mejores resultados en market.
    Repasar moneda por moneda los datos de mercado de las últimas 24 horas.
    Ir recogiendo profit de cambio de estado a cambio de estado y mostrar los
    que sobrepasen el 3%.
    """

    type = 'SPOT'

    min_profit = int(min_profit)
    list_symbols = []
    best = []
    # resultado[id] = datos
    resultado = []
    # resultado_anterior = False
    resultado_org = False
    id_org = False
    date_org = False
    status_org = False
    sql_list = "select distinct symbol from market"
    con = sqlite3.connect(DATABASE)
    cur_list = con.cursor()
    cur_list.execute(sql_list)
    list_symbols = cur_list.fetchall()
    for symboll in list_symbols:
        pair = symboll[0]
        sql = "SELECT * FROM market WHERE symbol like '{}' AND date > datetime('now','-3 days')".format(pair)

        cur = con.cursor()
        cur.execute(sql)
        rows = cur.fetchall()

        buy_price = False
        profit = 0
        for r in rows:

            (
                id,
                date,
                symbol,
                price,
                status,
                mon_precios,
                mon_tendencias,
                mon_volumenes,
                mon_adx,
                mon_macd

            ) = r

            monitor = "{} {} {} {} {}".format(
                mon_precios,
                mon_tendencias,
                mon_volumenes,
                mon_adx,
                mon_macd
            )

            if buy_price:
                profit, profitStr = getProfit(type, price, buy_price)

            if profit > min_profit:
                best.append(id)
                resultado.append("{} {} {:>16} {:<10} {:<4} {:>20}".format(
                    # resultado_anterior + "\n" + color_monitor(monitor) + ' ' + "{:<5} {:>1}".format(status.split()[0], status.split()[1]),
                    resultado_org + " {:<5} {:>1}".format(status_org.split()[0], status_org.split()[1]),
                    tendencia.color_monitor(monitor) + " {:<5} {:>1}".format(status.split()[0], status.split()[1]),
                    profitStr,
                    pair,
                    id_org,
                    date_org
                ))
            # elif profit > 0:
            #     resultado_anterior += "\n" + color_monitor(monitor) + ' ' + "{:<5} {:>1} {:>44}".format(status.split()[0], status.split()[1], date)
            # else:
            elif profit < 0:
                resultado_org = tendencia.color_monitor(monitor)
                id_org = id
                date_org = date
                status_org = status

            buy_price = price

    con.commit()
    con.close()

    # print(best)
    # for id in best:
    #     show_symbol(False, False, False, False, id)
    for r in resultado:
        print(r)

def total_in_time(test=False, telegram=False):

    total_4h  = 0
    total_24h = 0
    total_1w  = 0
    total_1m  = 0

    table = 'trade_result'

    if test:
        table = 'test_trade_result'

    def exec_sql(table, where):

        try:
            con = sqlite3.connect(DATABASE)
            cur = con.cursor()
            sql = "SELECT sum(profit), sum(resultd) FROM {} WHERE hide = 0 AND {}".format(table, where)
            cur.execute(sql)
            res = cur.fetchone()
            profit = res[0]
            result = res[1]
            if profit:
                profit = round(profit, 2)
                result = round(result, 2)
            else:
                profit = 0
                result = 0

            if not telegram:
                if profit > 0:
                    profit = green(str(profit) + '%')
                elif profit < 0:
                    profit = red(str(profit) + '%')
                else:
                    profit = cero(str(profit) + '%')
                if result > 0:
                    result = green(str(result) + '$')
                elif result < 0:
                    result = red(str(result) + '$')
                else:
                    result = cero(str(result) + '$')
            return (profit, result)
        except Exception as e:
            print(e)
            logger.error(e)
            return (0, 0)

    report_date = '-4 hours'
    where = "date_sell NOT NULL AND date_sell > datetime('now', '{}')".format(report_date)
    total_4h, result_4h = exec_sql(table, where)

    report_date = '-24 hours'
    where = "date_sell NOT NULL AND date_sell > datetime('now', '{}')".format(report_date)
    total_24h, result_24h = exec_sql(table, where)

    report_date = '-7 days'
    where = "date_sell NOT NULL AND date_sell > datetime('now', '{}')".format(report_date)
    total_1w, result_1w = exec_sql(table, where)

    report_date = '-30 days'
    where = "date_sell NOT NULL AND date_sell > datetime('now', '{}')".format(report_date)
    total_1m, result_1m = exec_sql(table, where)

    salida = "4h[{}/{}] 24h[{}/{}] 1w[{}/{}] 1m[{}/{}]".format(
        total_4h,
        result_4h,
        total_24h,
        result_24h,
        total_1w,
        result_1w,
        total_1m,
        result_1m
    )

    salida += " Last[{}]".format(report_last_trade(test, telegram))

    return salida

def report_last_trade(test=False, telegram=False):

    table = 'trade_result'
    if test:
        table = 'test_trade_result'

    try:
        con = sqlite3.connect(DATABASE)
        cur = con.cursor()
        sql = "SELECT symbol, resultd, mark FROM {} ORDER BY date_sell desc LIMIT 1".format(table)
        cur.execute(sql)
        r = cur.fetchall()
        if len(r) == 0:
            return False
        symbol = r[0][0].replace('/' + CURRENCY, '')
        result = r[0][1]
        mark = r[0][2]
        if not result:
            result = 0
        result = round(result, 2)
        if not telegram:
            if result > 0:
                result = green(str(result) + '$')
            elif result < 0:
                result = red(str(result) + '$')
            else:
                result = cero(str(result) + '$')

        return "{}({}) {}".format(bold(symbol), mark, result)

    except Exception as e:
        print(e)
        logger.error(e)
        return 0

def report_status_market():

    numero_items = TOTAL_SYMBOLS
    # sql = "SELECT * FROM market WHERE date > datetime('now','-10 minutes') LIMIT {}".format(numero_items)
    sql = "SELECT * FROM market WHERE date > datetime('now','-10 minutes') GROUP BY symbol ORDER BY date desc LIMIT {}".format(numero_items)
    res = sql_exec(sql, False, False, True)

    if not res:
        return False

    result = {}
    result['all'] = []

    if len(res) < numero_items:
        return False

    for temp in TEMPORALIDADES:

        position_temp = TEMPORALIDADES.index(temp)

        precios    = 0
        tendencias = 0
        volumenes  = 0
        adx        = 0
        macd       = 0

        # print('symbol status mon_precios mon_tendencias mon_macd ')
        for row in res:
            # print('{} p:{} {} {} {} {} {} {} {}'.format(temp, position_temp, row['symbol'], row['status'], row['mon_precios'], row['mon_tendencias'], row['mon_volumenes'], row['mon_adx'], row['mon_macd']))
            # if row['status'][:4] == 'SELL':
            #     sells += 1
            # print("{} {} {} {} {} {}".format(TEMPORALIDADES[position_temp], position_temp, row['symbol'], row['mon_precios'], row['mon_precios'][-3:-2], row['mon_precios'][0:-5]))

            if position_temp == 0:
                value_precios    = row['mon_precios'][0:-5]
                value_tendencias = row['mon_tendencias'][0:-5]
                value_volumenes  = row['mon_volumenes'][0:-5]
                value_adx        = row['mon_adx'][0:-5]
                value_macd       = row['mon_macd'][0:-5]
            elif position_temp == 1:
                value_precios    = row['mon_precios'][-5]
                value_tendencias = row['mon_tendencias'][-5]
                value_volumenes  = row['mon_volumenes'][-5]
                value_adx        = row['mon_adx'][-5]
                value_macd       = row['mon_macd'][-5]
            elif position_temp == 2:
                value_precios    = row['mon_precios'][-4]
                value_tendencias = row['mon_tendencias'][-4]
                value_volumenes  = row['mon_volumenes'][-4]
                value_adx        = row['mon_adx'][-4]
                value_macd       = row['mon_macd'][-4]
            elif position_temp == 3:
                value_precios    = row['mon_precios'][-3]
                value_tendencias = row['mon_tendencias'][-3]
                value_volumenes  = row['mon_volumenes'][-3]
                value_adx        = row['mon_adx'][-3]
                value_macd       = row['mon_macd'][-3]
            elif position_temp == 4:
                value_precios    = row['mon_precios'][-2]
                value_tendencias = row['mon_tendencias'][-2]
                value_volumenes  = row['mon_volumenes'][-2]
                value_adx        = row['mon_adx'][-2]
                value_macd       = row['mon_macd'][-2]
            elif position_temp == 5:
                value_precios    = row['mon_precios'][-1]
                value_tendencias = row['mon_tendencias'][-1]
                value_volumenes  = row['mon_volumenes'][-1]
                value_adx        = row['mon_adx'][-1]
                value_macd       = row['mon_macd'][-1]

            # print(row['mon_precios'], row['mon_tendencias'], row['mon_volumenes'], row['mon_adx'], row['mon_macd'])
            # print("{}: {} {} {} {} {}".format(position_temp, value_precios, value_tendencias, value_volumenes, value_adx, value_macd))


            if value_tendencias and int(value_tendencias) > 5:
                tendencias += 1
            if value_macd and int(value_macd) > 5:
                macd += 1
            if value_precios and int(value_precios) > 5:
                precios += 1
            if value_volumenes and int(value_volumenes) > 5:
                volumenes += 1
            if value_adx and int(value_adx) > 5:
                adx += 1

        macd = (macd / numero_items) * 100
        adx = (adx / numero_items) * 100
        precios = (precios / numero_items) * 100
        volumenes = (volumenes / numero_items) * 100
        tendencias = (tendencias / numero_items) * 100
        # No tenemos en cuenta adx ni volumenes.
        # media = ((adx + macd + precios + volumenes + tendencias) / 5)
        media = ((macd + precios + tendencias) / 3)
        media_all = ((macd + precios + tendencias) / 5)

        # result[temp] = [round(precios), round(tendencias), round(volumenes), round(adx), round(macd), round(media)]
        result[temp] = [round(precios), round(tendencias), round(macd), round(media)]
        result['all'].append(round(media_all))

    return result

async def report_status(show=False, test=False, telegram=False, strategy=False, show_hides=False):

    ## Devolvemos total_trades, total_losess, pares de trades, resultado últimas dos operaciones (0, 1, 2 ganadoras)
    table = 'trade_result'
    if test:
        table = 'test_trade_result'

    if show_hides:
        WHERE = " AND hide = 1"
    else:
        WHERE = " AND hide = 0"

    if FILTRO_INFORMES_HUMAN:
        condicion = human2sql(FILTRO_INFORMES_HUMAN, True)
        WHERE += " AND " + condicion

    if FILTRO_INFORMES:
        # condicion = human2sql(FILTRO_INFORMES, False)
        WHERE += " AND " + FILTRO_INFORMES

    if strategy:
        strategy_id = get_eid(strategy)
        sql = "SELECT * FROM {} WHERE eid = {} AND date_sell IS NULL {} AND lock == 0 ORDER BY date".format(table, strategy_id, WHERE)
    else:
        sql = "SELECT * FROM {} WHERE date_sell IS NULL {} AND lock == 0 ORDER BY date".format(table, WHERE)

    total_trades = 0
    total_stoploss_positive = 0
    total_losses = 0
    last_two = 0
    symbols = []
    telegram_salida = ""

    if telegram:
        title = ""
        formato  = "\nId: {}"
        formato += "\nCoin: {}"
        formato += "\nType: {}"
        formato += "\nStrategy: {}"
        formato += "\nDateBuy: {}"
        formato += "\nAmount: {}"
        formato += "\nAmount$: {}"
        formato += "\nPrice: {}"
        formato += "\nMaxProf: {}"
        formato += "\nMinProf: {}"
        formato += "\nProfAct: {}"
        formato += "\nResulAct: {}"
        formato += "\nStopLoss: {}"
        formato += "\nStopTrainig: {}"
        formato += "\nLock: {}"
        formato += "\nHide: {}"
        formato += "\nInfo: {}"

    else:
        formato      = "{:<10} {:<5} {} {:<8} {:<20} {:>10} {:>10} {:>10} {:>7} {:>7} {:>16} {:>18} {:>6} {:<6} {:<3} {:<4} {}"
        formatoTitle = "{:<10} {:<5} {} {:<8} {:<20} {:>10} {:>10} {:>10} {:<7} {:>7} {:>16} {:>16} {:>6} {:<6} {:<3} {:<4} {}"
        title = formatoTitle.format(
            'Id',
            'Coin',
            'M',
            'Type[L]',
            'DateBuy',
            'Amount',
            'Amount $',
            'Price',
            'MaxProf',
            'MinProf',
            bold('ProfAct'),
            bold('ResulAct'),
            'StLoss',
            'STrai.',
            'LCK',
            'Hide',
            'Monitor'
        )

    try:

        rows = sql_exec(sql, False, False, True)
        if not rows:
            return False, False, False, False, False
        total_trades = len(rows)
        total_stoploss_positive = 0
        total_losses = 0

        if show:
            print(title)

        if telegram:
            telegram_salida += title

        total_profit = 0
        total_result = 0

        for r in rows:

            coin = r['symbol'].replace('/' + CURRENCY, '')
            profitStr = ''
            resultatStr = ''
            strategy_trade = get_strategy(r['eid'])

            if not r['max_profit']:
                max_profit = 0
            else:
                max_profit = r['max_profit']
            if not r['min_profit']:
                min_profit = 0
            else:
                min_profit = r['min_profit']

            if r['price_sell']:
                profit, profitStr = getProfit(r['type'], r['price_sell'], r['price'])
                if profit:
                    total_profit += profit
                    resultat = r['resultd']
                    if not resultat:
                        resultat = ((r['amount'] * r['price']) * profit / 100)

                    resultatStr = str(round(resultat, 2)) + '$'
                    total_result += resultat
                else:
                    resultat = 0
            else:
                profit = 0
                resultat = 0

            symbols.append(coin)

            if telegram:
                profitStr = profitStr
                resultatStr = round(resultat, 2)
            elif profit < 0:
                resultatStr = red(round(resultat, 2))
                total_losses += 1
            else:
                resultatStr = green(round(resultat, 2))
                if r['stoploss'] and r['stoploss'] > 0:
                    total_stoploss_positive += 1

            if r['stoploss']:
                stoploss_str = color_result(r['stoploss'])
            else:
                stoploss_str = ''

            salida = formato.format(
                r['id'],
                coin,
                strategy_trade['mark'],
                "{}[{}]".format(r['type'], strategy_trade['leverage']),
                r['date'],
                round(float(r['amount']), 3),
                round((r['amount'] / r['leverage']) * r['price'], 2),
                round(r['price'], 3),
                round(max_profit, 2),
                round(min_profit, 2),
                profitStr,
                resultatStr,
                stoploss_str,
                str(r['stoptrailing']),
                r['lock'],
                r['hide'],
                r['info']
            )

            if telegram:

                # Tendencia si estamos en telegram.
                monitor_color = ''
                result_tendencia = await tendencia.tendencia(coin + '/' + CURRENCY, r['mark'])
                status_marks, monitor_color, monitor, ultimo_precio = result_tendencia

                telegram_salida += "\n{}\nTendencia: {}".format(salida, monitor_color)

            elif show:
                print(salida)


        # Buscamos las dos últimas operaciones en test.
        if strategy:
            sql_lastwo = "SELECT profit FROM test_trades WHERE eid = {} AND date_sell IS NOT NULL AND price_sell IS NOT NULL AND date_sell > datetime('now', 'localtime', '{}') ORDER BY date_sell desc LIMIT 2".format(strategy_id, '-4 hours')
        else:
            sql_lastwo = "SELECT profit FROM test_trades WHERE date_sell IS NOT NULL AND price_sell IS NOT NULL AND date_sell > datetime('now', 'localtime', '{}') ORDER BY date_sell desc LIMIT 2".format('-4 hours')
        rows_lastwo = sql_exec(sql_lastwo)
        for profit_lt in rows_lastwo:
            if profit_lt[0] < 0:
                last_two += 1

        if telegram:
            total_profit_str = round(total_profit, 2)
            total_result_str = round(total_result, 2)
        else:
            if total_profit > 0:
                total_profit_str = green(round(total_profit, 2))
            else:
                total_profit_str = red(round(total_profit, 2))

            if total_result > 0:
                total_result_str = green(round(total_result, 2))
            else:
                total_result_str = red(round(total_result, 2))

        # Totales.
        salida_total = "\nTrades[{}] TProfit[{}%] TResult[{}$] TLosses[{}] LasTT[{}]".format(
            total_trades,
            total_profit_str,
            total_result_str,
            total_losses,
            last_two
        )

        # Totales agrupados por estrategia.
        if not strategy and show:
            print()
            estadisticas_informe(sql, 'mark', True)


        salida_total += "\n\n" + total_in_time(test, telegram)

        if telegram:
            telegram_salida += "\n" + salida_total

        if show and telegram:
            print(telegram_salida)
        elif show:
            print(salida_total)

        if telegram:
            return telegram_salida

        return total_trades, total_losses, symbols, last_two, total_stoploss_positive

    except Exception as e:
        logger.error(e)
        raise IOError(e)
        return False, False, False, False, False

    return total_trades, total_losses, symbols, last_two, total_stoploss_positive

def status_strategy_in_test(strategy_mark):

    ### @brief status_strategy_in_test
    ### Estado de estrategia en test.
    ### Buscamos información de las compras actualies de una estrategia en test.
    ###
    ### Si hay más positivas que negativas = 1
    ### Si el profit es superior al stoploss (Asegurando que acaba en positivo) += 1
    ### Si no False.
    strategy_id = get_eid(strategy_mark)
    config = get_config(strategy_mark)
    sql = "SELECT * FROM test_trade_result WHERE eid = {} AND date_sell IS NULL ORDER BY date_sell desc".format(strategy_id, '-4 hours')
    rows = sql_exec(sql, False, False, True)
    if not rows:
        return False
    result = 0
    for row in rows:
        if row['price_sell']:
            profit, profit_str = getProfit(row['type'], row['price_sell'], row['price'])
        else:
            profit = 0
        # print("{} {} {} {} {}".format(row['id'], row['symbol'], row['mark'], profit, row['stoploss']))
        if row['stoploss'] and float(row['stoploss'] > 0):
            result += 2
        elif float(profit) > 0:
            result += 1
        # else:
        #     result -= 1

    if result:
        result = result / len(rows)

    return result

def result_greater_than(db=False, mark=False, test=False, report_date=False):
    ###
    ### Devolvemos 0/1
    ###
    table = 'trade_result'
    if test:
        table = 'test_trade_result'
    if report_date is False:
        report_date = '-4 hours'
    con = False
    if db:
        con = sqlite3.connect(db)
    where = "date_sell > datetime('now', '{}')".format(report_date)
    if mark:
        where += " AND mark = '{}'".format(mark)
    sql = "SELECT sum(resultd) as result FROM {} WHERE hide = 0 AND {}".format(table, where)
    rows = sql_exec(sql, con, False, True)

    if rows and len(rows) > 0:
        result = rows[0]['result']
        if result:
            return result

    return False

def end(signal_received, frame):
    print("\nTerminamos")
    exit(0)
def get_parser():
    """
    Creates a new argument parser.
    """
    parser = argparse.ArgumentParser('db')
    version = '%(prog)s ' + __version__
    parser.add_argument('--version', '-v', action='version', version=version)
    parser.add_argument('--database', '-db', help='Path de database a utiliazar', default=False)
    parser.add_argument('--symbol', '-s', help='Muestra los registros del mercado del par, [all] todos."')
    parser.add_argument('--coin', '-c', help='Muestra los registros del mercado de la moneda, [all] todas."')
    parser.add_argument('--strategy', '-sy', help='Estratefia, ejemplo -sy C"', default=False)
    parser.add_argument('--trades', '-t', help='Muestra las operaciones realizadas, [all] todos."')
    parser.add_argument('--test_trades', '-tt', help='Muestra las operaciones realizadas en test, [all] todos."')
    parser.add_argument('--trade', '-td', help='Muestra detalle de trade, [id] Identificador"')
    parser.add_argument('--show_hides', '-sh', help='Muestra operaciones ocultas', default=False, action='store_true')
    parser.add_argument('--trade_test', '-tdt', help='Muestra detalle de trade en test, [id] Identificador"')
    parser.add_argument('--report_trades', '-rt', help='Reporte de operecaciones abiertas.', default=False, action='store_true')
    parser.add_argument('--report_trades_test', '-rtt', help='Reporte de operecaciones abiertas en test.', default=False, action='store_true')
    parser.add_argument('--report_date', '-rd', help='Especificar fecha inicio de informe, por defecto "-24 hours", ejemplo: "-7 days"o "-1 months"', default=False)
    parser.add_argument('--search', '-sr', help='Condición a añadir para buscar patrones en market, ejemplo. "cast(substr(mon_adx, 3, 5) as integer) > 500"', default=False)
    parser.add_argument('--date', '-dt', help='Mostrar market dos horas para arriba y abajo de la fecha dada, ejemplo. "2021-05-17 12:42:31"', default=False)
    parser.add_argument('--identificador', '-id', help='Mostrar market de la moneda del registro especificado, dos horas para arriba y abajo, ejemplo. "56"', default=False)
    parser.add_argument('--identificadores', '-ids', help='Lista de identificadores, ejemplo. "56 34 23"', default=False)
    parser.add_argument('--best_results', '-br', help='Mostrar mejores resultados sobre market, [MIN PROFIT]', default=False)
    parser.add_argument('--view_results', '-vr', help='Mostrar resultados sobre una condición del mercado', default=False)
    parser.add_argument('--manual_config', '-mcf', help='Configuración manual, ejemplo: "min_profit_sell:1.0 stoploss:-2.0 follow_losses:-1.0 stoptrailing:0;2;30"', default=False)
    parser.add_argument('--strategy_test', '-st', help='Mostrar resultados de estrategia en el pasado, podemos pasar --report_date, ejemplo: reports.py -st R -rd "-1 day"', default=False)
    parser.add_argument('--test_signals', '-ts', help='Añadimos configuración sin stoploss ni stoptrailing ara poder ver resultados de las señales, ejemplo: reports.py -st R -rd "-1 day" -ts', default=False, action='store_true')
    parser.add_argument('--test_close_signals', '-tcs', help='Testeamos las señales TSELL y TCLOSE de las estrategias', default=False, action='store_true')
    parser.add_argument('--compare_settings', '-ce', help='Comparamos configuraciones en estrategias, se puede combinar con "-r 30" en minutos', action='store_true')
    parser.add_argument('--test_configs', '-tc', help='Testeamos configuraciones posibles para estrategias. ejemplo reports.py -ce -mc -tc -r 30', default=False, action='store_true')
    parser.add_argument('--make_changes', '-mc', help='Ejecutamos cambios en configuración automáticamente, ejemplo reports.py -ce -mc -r 30', default=False, action='store_true')
    parser.add_argument('--without_stoploss', '-wc', help='Añadimos configuración sin stoploss para probar señales de cierre', default=False, action='store_true')
    parser.add_argument('--verbose', '-vv', help='Mostrar detalles.', default=False, action='store_true')
    parser.add_argument('--refresh',
                        '-r',
                        help='Parada en minitos hasta nuevo refresco, por defecto no se hace bucle',
                        required=False,
                        default=False
                        )
    parser.add_argument('--total_in_time',
                        '-tit',
                        help='Diferentes totales en diferentes tiempos',
                        required=False,
                        default=False,
                        action='store_true'
                        )
    parser.add_argument('--status_strategy_test',
                        '-sst',
                        help='Estado de las operaciones de una estrategia en test. 0: Sin ganancias, 1: Con ganancias, 2: Ganacias y aseguradas',
                        required=False,
                        default=False,
                        )
    parser.add_argument('--total_in_time_test',
                        '-titt',
                        help='Diferentes totales en diferentes tiempos en test',
                        required=False,
                        default=False,
                        action='store_true'
                        )
    parser.add_argument('--telegram',
                        '-tg',
                        help='Formato de salida para telegram',
                        required=False,
                        default=False,
                        action="store_true"
                        )
    parser.add_argument('--agrupar',
                        '-ar',
                        help='Agrupamos por campos para los informes, ejemplo: reports -t all -ar "market_status"',
                        required=False,
                        default=False,
                        )
    parser.add_argument('--filtrar',
                        '-fr',
                        help='Condición sql para los informes, ejemplo: reports -t all -fr "resultd > 5"',
                        required=False,
                        default=False,
                        )
    parser.add_argument('--filter_human',
                        '-frh',
                        help='Condición en formato humano para los informes, ejemplo: reports -t all -fr "[tendecias:>:500]"',
                        required=False,
                        default=False,
                        )
    parser.add_argument('--status_market_screen',
                        '-sms',
                        help='Mostrar estado de mercado en las diferentes temporalidades.',
                        required=False,
                        default=False,
                        action="store_true"
                        )
    parser.add_argument('--resultgreaterthan',
                        '-rgt',
                        help='Devuelve resultado. Ejemplo: -rgt -rd "-3 hours"',
                        required=False,
                        default=False,
                        action="store_true"
                        )


    return parser

def main(args=None):
    """
    Main entry point for your project.
    Args:
        args : list
            A of arguments as if they were input in the command line. Leave it
            None to use sys.argv.
    """

    global FORMATO_SALIDA, FILTRO_INFORMES, FILTRO_INFORMES_HUMAN, AGRUPAR_INFORMES, DATABASE

    parser = get_parser()
    args = parser.parse_args(args)

    if args.database:
        DATABASE = args.database

    if args.telegram:
        FORMATO_SALIDA = 'telegram'

    if args.filter_human:
        FILTRO_INFORMES_HUMAN = args.filter_human

    if args.filtrar:
        FILTRO_INFORMES = args.filtrar

    if args.agrupar:
        AGRUPAR_INFORMES = args.agrupar

    if args.compare_settings:
        if args.without_stoploss and args.make_changes:
            print('\nNo podemos cambiar configuraciones con test de señales.')
            print('Las configuaciones para los test de señales pueden ser altamente arriesgadas')
            exit(1)
        if args.strategy:
            strategy = args.strategy
        else:
            strategy = 'all'
        show_details = True
        if args.verbose is False:
            show_details = False
        if args.refresh and int(args.refresh) > 0:
            minutos = int(args.refresh) * 60
            while True:
                comparar_configuraciones(args.report_date, args.make_changes, args.test_signals, show_details, args.test_configs, strategy, args.test_close_signals, args.without_stoploss)
                time_diff = datetime.now() + timedelta(minutes=int(args.refresh))
                print("\nPróxima actualización a las {}".format(time_diff))
                time.sleep(minutos)
        else:
            comparar_configuraciones(args.report_date, args.make_changes, args.test_signals, show_details, args.test_configs, strategy, args.test_close_signals, args.without_stoploss)
        return

    if args.strategy_test:
        strategy_test(args.strategy_test, args.report_date, True, args.test_signals, args.test_configs, args.verbose, True, args.test_close_signals, args.without_stoploss)
        return

    if args.best_results:
        best_results(args.best_results)

    if args.status_strategy_test:
        print(status_strategy_in_test(args.status_strategy_test))
        return

    if args.total_in_time_test:
        print(total_in_time(True))
        return

    if args.total_in_time:
        print(total_in_time())
        return

    if args.coin:
        if args.coin == 'all':
            symbol = 'all'
        else:
            symbol = args.coin + '/' + CURRENCY
        show_symbol(symbol, args.report_date, args.search, args.date)
        return

    if args.symbol:
        show_symbol(args.symbol, args.report_date, args.search, args.date)
        return

    if args.identificadores:
        for id in args.identificadores.split():
            show_symbol(args.symbol, args.report_date, args.search, args.date, id)
        return

    if args.identificador or args.date:
        show_symbol(args.symbol, args.report_date, args.search, args.date, args.identificador)
        return

    if args.search:
        show_symbol('all', args.report_date, args.search)
        return

    if args.show_hides:
        loop = asyncio.get_event_loop()
        loop.run_until_complete(report_status(True, False, args.telegram, args.strategy, True))

    if args.trades:
        if args.refresh and int(args.refresh) > 0:
            minutos = int(args.refresh) * 60
            while True:
                os.system('clear')
                db_trades(args.trades, False, args.telegram, args.report_date, False, args.strategy)
                time_diff = datetime.now() + timedelta(minutes=int(args.refresh))
                time.sleep(minutos)
        else:
            db_trades(args.trades, False, args.telegram, args.report_date, False, args.strategy)

    if args.test_trades:
        if args.refresh and int(args.refresh) > 0:
            minutos = int(args.refresh) * 60
            while True:
                os.system('clear')
                db_trades(args.test_trades, True, args.telegram, args.report_date, False, args.strategy)
                time_diff = datetime.now() + timedelta(minutes=int(args.refresh))
                time.sleep(minutos)
        else:
            db_trades(args.test_trades, True, args.telegram, args.report_date, False, args.strategy)

    if args.trade:
        db_trade(args.trade, False, args.telegram)

    if args.trade_test:
        db_trade(args.trade_test, True, args.telegram)

    if args.view_results:
        view_results(args.view_results, args.strategy, args.report_date, True, False, False, False, args.manual_config)

    if args.report_trades:
        loop = asyncio.get_event_loop()
        loop.run_until_complete(report_status(True, False, args.telegram, args.strategy))

    if args.report_trades_test:
        loop = asyncio.get_event_loop()
        if args.refresh and int(args.refresh) > 0:
            minutos = int(args.refresh)
            while True:
                os.system('clear')
                loop.run_until_complete(report_status(True, True, args.telegram, args.strategy))
                time_diff = datetime.now() + timedelta(minutes=int(args.refresh))
                time.sleep(minutos)
        else:
            loop.run_until_complete(report_status(True, True, args.telegram, args.strategy))

    if args.status_market_screen:
        if args.refresh and int(args.refresh) > 0:
            minutos = int(args.refresh)
            while True:
                os.system('clear')
                status_market_screen()
                time_diff = datetime.now() + timedelta(minutes=int(args.refresh))
                time.sleep(minutos)
        else:
            status_market_screen()

    if args.resultgreaterthan:
        result = result_greater_than( args.database, args.strategy, False, args.report_date)
        print(result)

if __name__ == '__main__':
    main()
