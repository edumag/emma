#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## @file balances.py
## @brief Listado del balance.

__version__ = '0.1.0'
__author__ = u'edu@lesolivex.com'

salida = ''

import os
import sys
import ccxt.async_support as ccxt  # noqa: E402
import argparse
import json
import time
import pprint

sys.path.append('./src')

from base import *
from tendencia import *
from console import *
from binance_calls import *
from open_orders import *
from mercado import *
from db import *
from reports import *
from history import *
from futures import *
from telegram_send_message import *
from datetime import datetime, timedelta
from signal import signal, SIGINT

import asyncio
import nest_asyncio
nest_asyncio.apply()

datos_pantalla_trade = {}
total_exchanges_usd = 0
total_trades=0

# Pantalla
barra_informativa = ''
datos_barrainformativa = {
    'balance_usdt': 0,
    'suma_profit': 0,
    'suma_result': 0,
    'total_usd': 0
}
msg = []

balance_usdt = 0

# Tiempo de espera para activar ventas
# en segundos.
tiempo_espera = time.time() + 60 # 1m
SHELL_ACTIVE = False

# Recoger los trades de los balances.
RECOGER_BALANCES = False

# Avisos trade parado.
aviso_mercado_parado = False

# Estrategias de los trades.
estrategias_trades = []

# Mostrar trades ocultos.
SHOW_HIDES=False

async def pantalla_barra():

    global SHELL_ACTIVE

    barra_informativa = ''

    config_general = get_config_general()
    strategies = config_general['estrategias_activadas']

    total_trades, total_losses, buy_symbols, last_two, total_stoploss_positive = await report_status()
    test_total_trades, test_total_losses, test_buy_symbols, test_last_two, test_total_stoploss_positive = await report_status(False, True)

    if SIMULA_COMPRAS_VENTAS:
        simulate_compra_venta = bold(green("[SIMULA]"))
    else:
        simulate_compra_venta = bold(orange("[REAL]"))

    if total_losses > 0:
        total_lossesStr = red(total_losses)
    else:
        total_lossesStr = green(total_losses)

    if test_total_losses > 0:
        test_total_lossesStr = red(test_total_losses)
    else:
        test_total_lossesStr = green(test_total_losses)

    # Mostramos todas las activadas.
    # for estrategia_trade in estrategias_trades:
    color = 1
    for estrategia_trade in strategies.split():
        salida_sell = red('F')
        config_estrategia_trade = get_config(estrategia_trade)
        eid = get_eid(estrategia_trade)
        strategy = get_strategy(eid)
        if config_estrategia_trade['ventas_activadas'] == 1 or config_estrategia_trade['ventas_activadas'] == True:
            if round(tiempo_espera - time.time()) < 0:
                SHELL_ACTIVE = True
                salida_sell = green('T')
            else:
                salida_sell = orange(str(round(tiempo_espera - time.time())) + 's')

        if config_estrategia_trade['sell_with_loss']:
            salida_swl = green('T')
        else:
            salida_swl = red('F')

        # Información estrategias.
        barra_informativa += (
            "\n[{}] {} MSProfit:[{}] FollowLos[{}] StopLos[{}] STrailing[{}] MaxTr[{}] MaxLos[{}] SWL[{}] SELL[{}]".format(
                colores(color, estrategia_trade),
                "{}[{}]".format(
                    colores(color, strategy['type']),
                    strategy['leverage']
                ),
                colores(color, str(config_estrategia_trade['min_profit_sell']) + '%'),
                colores(color, str(config_estrategia_trade['follow_losses']) + '%'),
                colores(color, str(config_estrategia_trade['stoploss']) + '%'),
                colores(color, str(config_estrategia_trade['stoptrailing'])),
                colores(color, str(config_estrategia_trade['max_trades'])),
                colores(color, str(config_estrategia_trade['max_losses'])),
                salida_swl,
                salida_sell,
            )
        )
        color = color + 1

    suma_profit = datos_barrainformativa['suma_profit']
    if suma_profit > 0:
        suma_profitStr = green(str(round(suma_profit, 2)) + '%')
    else:
        suma_profitStr = red(str(round(suma_profit, 2)) + '%')

    suma_result = datos_barrainformativa['suma_result']
    if suma_result > 0:
        suma_resultStr = green(str(round(suma_result, 2)) + '$')
    else:
        suma_resultStr = red(str(round(suma_result, 2)) + '$')

    # Información totales.
    barra_informativa += (
        "\nTotalProfit[{}] TotalResult[{}]".format(suma_profitStr, suma_resultStr)
    )

    # Información general.
    barra_informativa += (
        " TotalTradess[{}] Losses[{}] TotalTests[{}] LossTest[{}] {}".format(
            bold(total_trades),
            total_lossesStr,
            bold(test_total_trades),
            test_total_lossesStr,
            simulate_compra_venta
            )
    )

    barra_informativa += "\n" + total_in_time()

    print(barra_informativa)

async def pantalla_trade(asset=False):

    global barra_informativa

    os.system('clear')

    format = "{:6} {:<5} {:1} {:<8} {:>9} {:>7} {:<5} {:<5} {:<46} {} {} {:<7}"
    titles = bold(format.format('Id', 'Co[' + str(total_trades) + ']', 'S', 'Type[L]', 'Balance', 'Profit', 'OrderPrice', '  HH:MM', 'Precis Tendes Volums ADXs   MACDs   LastPric Status ', 'MaxProf', 'MinProf', 'SellIn'))
    print("{:<56}".format(titles), end='')
    print("")

    for id in datos_pantalla_trade:
        contenido = "{:<243}".format(datos_pantalla_trade[id])
        s = contenido.split()[1]
        if asset == s:
            print(bold("{:<56}".format(contenido)), end='')
        else:
            print("{:<56}".format(contenido), end='')
        print("")

    # Barra informativa.
    await pantalla_barra()

    if len(msg) > 0:
        counter_msg = 1
        print("\n")
        for m in msg:
            print(bold(m), end=' ')
            counter_msg += 1
            if counter_msg > 3:
                break

async def profit(exchange, code):

    # Primero buscamos en base de datos.
    t = db_open_trades(code)

    if len(t) > 0 and t[0]:
        dt_object = datetime.strptime(t[0]['date'], '%Y-%m-%d %H:%M:%S')
        dt_object = dt_object.strftime("%d/%m %H:%M")
        return [t[0]['price'], dt_object, '', t[0]['amount']]

    l = await last_buy(code)
    if l and len(l) > 0:
        dt_object = datetime.fromtimestamp(int(l['time']) / 1000)
        dt_object = dt_object.strftime("%d/%m %H:%M")
    else:
        return False

    if float(l['executedQty']) > 0:
        amount = l['executedQty']
    elif 'origQty' in l and float(l['origQty']) > 0:
        amount = l['origQty']
    else:
        amount = l['executedQty']

    return [float(l['price']), dt_object, l['orderId'], amount]

async def trades(exchange):

    config_general = get_config_general()
    if config_general:
        strategies = config_general['estrategias_activadas']
        if not strategies:
            logger.error('Sin estrategia definida')
            print('Sin estrategia definida')
            time.sleep(15)
            return
    else:
        logger.error('No se pudo recoger config_general')
        print('No se pudo recoger config general')
        time.sleep(15)
        return

    total_usd = 0
    datos = ""
    # Primera estrategia activada por defecto.
    sesion = strategies.split()[0]

    global total_exchanges_usd, barra_informativai, total_trades
    global datos_pantalla_trade, balance_usdt
    global RECOGER_BALANCES, aviso_mercado_parado, estrategias_trades

    # Guardar lock de la app.
    db_locks('trades')

    # Vigilar mercado
    if not get_lock('mercado'):
        logger.info('Mercado parado')
        if not aviso_mercado_parado or datetime.now() > aviso_mercado_parado:
            tlg_send('**Mercado parada**\n\nSi solo tenemos trade en funcionamiento y mercado parado no se ejecutarán las compras')
            aviso_mercado_parado = datetime.now() + timedelta(hours = 4)

    try:
        if not RECOGER_BALANCES:
            # Buscamos compras en DB.
            balances = db_open_trades('all', False, SHOW_HIDES)
        else:
            balances_account = await exchange.fetch_balance()
            if exchange.name == 'Binance':
                balances = balances_account['info']['balances']
            else:
                balances = balances_account
            # RECOGER_BALANCES = False
    except ccxt.NetworkError as e:
        print('## ', exchange.id, 'fetch_balance failed due to a network error:', str(e))
        await exchange.close()
        return False
    except ccxt.ExchangeError as e:
        print('## ', exchange.id, 'fetch_balance failed due to exchange error:', str(e))
        await exchange.close()
        return False
    except Exception as e:
        print('## ', exchange.id, 'fetch_balance failed with:', str(e))
        await exchange.close()
        return False

    # print("\n\n## ", exchange.id, "\n")

    count_trades = 0
    suma_profit = 0
    suma_result = 0
    salida_usdt = ''
    datos_pantalla = {}
    counter_id = 0

    total_trades = len(balances)

    # Dejar solo los trade que hemos recogido.
    for id in datos_pantalla_trade.copy():
        coin = datos_pantalla_trade[id].split()[1]
        is_coin_in_balance=False
        for balance_coin in balances:
            if balance_coin['symbol'].replace('/' + CURRENCY,'') == coin:
                is_coin_in_balance=True
        if not is_coin_in_balance:
            datos_pantalla_trade.pop(id)


    for b in balances:

        type = 'UNKNOW'
        counter_id += 1
        openOrderStr = ''
        monitor_color = ''
        status = 'unknown'
        order_profit = 0
        price = False
        date = ''
        monitor_color = ''
        buy_status = ''
        buy = False
        symbol = ''
        monitor = ''
        openOrderDistance = False
        openOrderDistanceStr = ''

        if not RECOGER_BALANCES:

            symbol = b['symbol']
            id = b['id']
            asset = symbol.replace('/' + CURRENCY,'')
            price = b['price']
            amount = b['amount']
            usdt = price * amount
            datetrade = b['date']
            dt_object = datetime.strptime(datetrade, '%Y-%m-%d %H:%M:%S')
            date = dt_object.strftime("%d/%m %H:%M")
            sesion = strategies.split()[0]

            try:

                eid = b['eid']
                mark = get_strategy(eid)['mark']
                config = get_config(mark)

            except Exception as e:

                msg = "ERROR recogiendo información de estrategia [{}] para {}: {}".format(b['eid'], symbol, e)
                print(msg)
                logger.error(msg)
                break

            buys = is_buy(id)

            if buys:
                buy = buys[0]
            else:
                logger.debug('Sin venta registrada')
                continue

            type = buy['type']
            buy_status = buy['status']
            buy_stoploss = buy['stoploss']
            if buy_stoploss:
                openOrderDistance = round(buy_stoploss, 2)
                if openOrderDistance > 0:
                    openOrderDistanceStr += "{:<6}".format(green(str(openOrderDistance) + '%'))
                elif openOrderDistance < 0:
                    openOrderDistanceStr += "{:<6}".format(red(str(openOrderDistance) + '%'))
            else:
                openOrderDistance = False

            if buy['stoptrailing']:
                if openOrderDistance:
                    openOrderDistance = str(openOrderDistance) + " " + str(buy['stoptrailing'])
                else:
                    openOrderDistance = str(buy['stoptrailing'])

            sesion = get_strategy(buy['eid'])['mark']

            status_mark = False
            result_tendencia = await tendencia(symbol, sesion)
            status_marks, monitor_color, monitor, ultimo_precio = result_tendencia

            if status_marks and sesion in status_marks:
                status_mark = status_marks[sesion]
                if status_mark:
                    status = status_mark.split()[0]
                else:
                    status = False
            else:
                status = False
                continue

            if buy['hide']:
                continue

            if status_mark and monitor:
                status = status_mark.split()[0]
                # Añadimos registro en market.
                result = db_insert_market(symbol, ultimo_precio, status_mark, monitor)
                if result and result != '':
                    logger.info(result)

            # Si esta bloqueado no hace falta buscar ordenes abiertas, mostramos LOCK.
            if buy['lock']:
                openOrderDistanceStr += 'LOCK '

            if buy['hide']:
                openOrderDistanceStr += 'HIDE '

            if price > 0:
                order_profit, order_profit_str = getProfit(buy['type'], ultimo_precio, price)
                order_result = ((amount * price) * order_profit / 100)
                amount = amount / buy['leverage']
                usdt_old = price * amount
                usdt = ultimo_precio * amount  # @bug No es valido en SHORT
                if buy['type'] == 'SHORT':
                    usdt = (usdt_old + order_result)
                result_in_dolars = usdt - usdt_old
            else:
                order_profit = False
                order_result = False
                result_in_dolars = False

            order_profit = round(order_profit, 2)
            suma_profit += order_profit
            suma_result += order_result

            # Registrar max_profit y min_profit en trade.
            if ultimo_precio:
                try:
                    buy_update = db_update_open_trade(id, ultimo_precio)
                except Exception as e:
                    print(e)
                    raise IOError(e)
                    logger.error(e)
            else:
                buy_update = False

            if buy['lock']:
                logger.debug("[{}] bloqueado, no valoramos venta".format(asset))
                info = False
            elif buy['hide']:
                logger.debug("[{}] oculto, no valoramos venta".format(asset))
                info = False
            elif not config['ventas_activadas']:
                logger.debug("[{}] Ventas desactivadas, no hacemos nada".format(asset))
                info = False
            else:
                info = valorar_venta(asset, order_profit, status, buy_stoploss, sesion)
                if info and not SHELL_ACTIVE:
                    logger.info('Se venderá [{}] en estrategia [{}] cuanto se activen las ventas'.format(asset, buy['mark']))
                elif info:
                    class Args:
                        side = 'sell'
                    argsSell = Args()
                    argsSell.close = buy['id']
                    argsSell.tipo_sesion = sesion
                    argsSell.type = buy['type']
                    loop = asyncio.get_event_loop()
                    profit_sell = loop.run_until_complete(ejecutar_venta(argsSell, info, status_mark, datos ))
                    if not profit_sell:
                        logger.info('[trades] Error vendiendo ' + asset)
                    else:
                        logger.info('[trades] Vendemos ' + asset + ' Profit: ' + str(profit_sell))
                        # Mostramos todas las activadas.
                        # estrategias_trades = []

            if openOrderDistance  == float:
                if openOrderDistance > 0:
                    openOrderDistanceStr += "{:<6}".format(green(str(openOrderDistance) + '%'))
                elif openOrderDistance < 0:
                    openOrderDistanceStr += "{:<6}".format(red(str(openOrderDistance) + '%'))
                else:
                    openOrderDistanceStr += "{:<6}".format(blue(str(openOrderDistance) + '%'))
            else:
                openOrderDistanceStr += "".format(blue(str(openOrderDistance)))

            if buy and buy['stoptrailing']:
                openOrderDistanceStr = openOrderDistanceStr + " " + bold(buy['stoptrailing'])

            # Tiempo transcurrido.
            unix_time = datetime.timestamp(dt_object)
            unix_time_now = datetime.timestamp(datetime.now())
            time_trade = unix_time_now - unix_time
            hours = time_trade / 3600
            minutes = hours % 60
            time_format = "{:>4}:{:<2}".format(round(hours), round(minutes))


            salida_datos = "{:<6} {:5} {:1} {:9} {:>7}$ {:>16} {:>10} {:<7} {:<195} {:>16} {:>16} {:<20}".format(
                id,
                asset,
                buy['mark'],
                "{}[{}]".format(type, buy['leverage']),
                round(float(usdt), 2),
                order_profit_str,
                str(round(price, 5)) + '$',
                time_format,
                monitor_color,
                color_result(buy['max_profit'], '%'),
                color_result(buy['min_profit'], '%'),
                openOrderDistanceStr
                )

            datos_pantalla_trade[id] = salida_datos
            await pantalla_trade(asset)

        elif exchange.name == 'Binance':
            ###
            ### Balances desde binance.
            ###

            if b != "free" and "free" in b and (float(b['free']) + float(b['locked'])  > 0.001 ):

                asset = str(b['asset'])

                if asset == CURRENCY:
                    ultimo_precio = 1
                else:

                    symbol = asset + '/' + CURRENCY

                    buys = is_buy(symbol)

                    if buys:
                        buy = buys[0]
                        buy_status = buy['status']
                        buy_stoploss = buy['stoploss']
                        if len(buy_status.split()) > 1:
                            sesion = buy_status.split()[1]
                        else:
                            sesion = 'M'
                    else:
                        buy = False

                    result_tendencia = await tendencia(symbol, sesion)
                    status_marks, monitor_color, monitor, ultimo_precio = result_tendencia

                    status_mark = False
                    if status_marks:
                        for sm in status_marks:
                            if status_marks[sm]:
                                status_mark = status_marks[sm]
                                status = status_mark.split()[0]

                        if status_mark and monitor:
                            # Añadimos registro en market.
                            result = db_insert_market(symbol, ultimo_precio, status_mark, monitor)
                            if result and result != '':
                                logger.info(result)

                config = get_config('M')


                free = float(ultimo_precio * float(b['free']))
                locked = float(ultimo_precio * float(b['locked']))
                amount = float(b['free']) + float(b['locked'])

                if (free + locked) > 9:
                    usdt = free + locked
                    total_usd = total_usd + usdt

                    # Buscamos precio de compra y fecha.
                    # o lo cogemos de cache.
                    order_price = False
                    if asset != CURRENCY:
                        order_price = getCache(exchange.id + '-' + asset + '-' + str(amount))
                        if not order_price:
                            order_price = await profit(exchange, symbol)
                            if order_price and order_price[0] > 0:
                                setCache(exchange.id + '-' + asset + '-' + str(amount), order_price)

                        openOrder = []

                        counter_open_orders = 0
                        for o in openOrder:
                            if counter_open_orders > 0:
                                espacio = ' '
                            else:
                                espacio = ''
                            openOrderStr = openOrderStr + ' ' + o['side'] + ' ' \
                            + str(o['price']) + '$ ' \
                            + str(o['amount']) + ' ' \
                            + str(round(o['price'] * o['amount'],2)) + '$'
                            # En caso de tener una orden abierta sobre la moneda
                            # Añadimos solo la información sobre el porcentaje que
                            # falta para llegar.
                            if o['price'] > 0 and order_price[0] > 0:
                                openOrderDistance = round(((o['price'] * 100) / ultimo_precio ) - 100,2)
                            elif buy_stoploss:
                                openOrderDistance = round(((buy_stoploss * 100) / ultimo_precio ) - 100,2)
                            else:
                                openOrderDistance = False

                            if openOrderDistance > 0:
                                openOrderDistanceStr = openOrderDistanceStr + espacio + "{:<16}".format(green(str(openOrderDistance) + '%'))
                            elif openOrderDistance < 0:
                                openOrderDistanceStr = openOrderDistanceStr + espacio + "{:<16}".format(red(str(openOrderDistance) + '%'))
                            else:
                                openOrderDistanceStr = openOrderDistanceStr + espacio + "{:<16}".format(blue(str(openOrderDistance) + '%'))

                            counter_open_orders += 1

                    else:
                        order_price = False

                    if not buy and monitor:
                        # Si no tenemos la compra registrada en base de datos, la
                        # añadimos.
                        buy_price = ultimo_precio
                        if order_price and order_price[0] != 0:
                            buy_price = order_price[0]
                        db_insert_buy(symbol, amount, buy_price, 'SPOT', 1, False, monitor, False, 'Add in trade', False, True, False)

                    if order_price and asset != CURRENCY:

                        price = order_price[0]
                        date = order_price[1]
                        order_id = order_price[2]
                        amount = order_price[3]

                        if price > 0:
                            order_profit = ((ultimo_precio * 100) / price) - 100
                        else:
                            order_profit = False

                        order_profit = round(order_profit, 2)

                        # Registrar max_profit y min_profit en trade.
                        buy_update = False
                        try:
                            buy_update = db_update_open_trade(symbol, ultimo_precio, False, 1)
                        except Exception as e:
                            print(e)
                            logger.error(e)
                            raise IOError(e)
                        if buy_update:
                            buy_update = buy_update[0]
                            buy_stoploss = buy_update['stoploss']
                            if buy_stoploss:
                                openOrderDistanceStr += green(str(round(buy_stoploss, 2)) + '%')
                        else:
                            buy_stoploss = False

                        info = False

                        suma_profit += order_profit

                        if order_profit > config['min_profit_sell']:
                            order_profit = green(str(order_profit) + '%')
                        elif order_profit > 0:
                            order_profit = blue(str(order_profit) + '%')
                        elif order_profit < config['follow_losses']:
                            order_profit = red(str(order_profit) + '%')
                        elif order_profit < 0:
                            order_profit = orange(str(order_profit) + '%')
                        else:
                            order_profit = bold(str(order_profit) + '%')

                    if b['asset'] == CURRENCY:
                        salida_usdt = bold(CURRENCY + ": ") + str(round(usdt,2)) + '$'
                        balance_usdt = usdt

                    else:

                        salida_datos = "{:6}{:10,.2f}$ {:>16} {:>10} {:<10} {:<175} {:<20}".format(
                            b['asset'],
                            usdt,
                            order_profit,
                            str(round(price, 5)) + '$',
                            date,
                            monitor_color,
                            openOrderDistanceStr
                            )

                        datos_pantalla_trade[counter_id] = salida_datos

                    if b['asset'] != CURRENCY:
                        count_trades += 1
                        await pantalla_trade(asset)

        else:

            # Balances desde kraken.
            asset = balances[b]
            if "free" in asset and float(asset['total']) > 0:
                total_asset = float(asset['total'])
                if total_asset > 0:
                    asset = str(b)
                    if asset == CURRENCY:
                        ultimo_precio = 1
                        total_asset_usd = total_asset
                    elif asset == 'EUR':
                        ultimo_precio = eurusdt_price
                        total_asset_usd = total_asset * ultimo_precio
                    else:
                        symbol = asset + '/' + CURRENCY
                        ultimo_precio = await fetch_ticker(exchange, symbol)
                        ultimo_precio = float(ultimo_precio['close'])
                        total_asset_usd = total_asset * ultimo_precio


                    if total_asset_usd > 3:
                        total_usd = total_usd + total_asset_usd
                        print("{} {} (price: {}, euros: {}€)\n".format(
                            asset,
                            total_asset_usd,
                            ultimo_precio,
                            total_asset_usd / eurusdt_price
                            ))

    await exchange.close()

    RECOGER_BALANCES = False

    if exchange.name == 'Binance':

        datos_barrainformativa['balance_usdt'] = balance_usdt
        datos_barrainformativa['suma_profit'] = suma_profit
        datos_barrainformativa['suma_result'] = suma_result
        datos_barrainformativa['total_usd'] = total_usd
        await pantalla_trade()

    total_exchanges_usd = total_exchanges_usd + total_usd

    # Comprobar si se han vendido monedas en trade
    for id in datos_pantalla_trade.copy():
        coin = datos_pantalla_trade[id].split()[1]
        if not is_buy(coin + '/' + CURRENCY):
            datos_pantalla_trade.pop(id)

    return total_usd

def total(signal_received, frame):
    print("\n\n## Total global: {}$ ({}€)\n".format(total_exchanges_usd, total_exchanges_usd / eurusdt_price))
    print(salida)
    exit(0)

def get_parser():
    """
    Creates a new argument parser.
    """
    parser = argparse.ArgumentParser('trades')
    version = '%(prog)s ' + __version__
    parser.add_argument('--version', '-v', action='version', version=version)
    parser.add_argument('--exchange', '-e', help='Exchange, si no definimos todos')
    parser.add_argument('--balances', '-bl', help='Recogemos balances del exchanges', action="store_true")
    parser.add_argument('--sellall', '-sa', help='Vender todo', action="store_true")
    parser.add_argument('--hides', '-hi', help='Mostrar trades ocultos', action="store_true")
    return parser

def main(args=None):
    """
    Main entry point for your project.
    Args:
        args : list
            A of arguments as if they were input in the command line. Leave it
            None to use sys.argv.
    """

    global salida, exchange, BTC, btc_price, EURUSDT
    global eurusdt_price, total_exchanges_usd, datos_pantalla_trade
    global RECOGER_BALANCES, SHOW_HIDES

    config_general = get_config_general()
    strategies = config_general['estrategias_activadas']
    strategies = config_general['estrategias_activadas']
    if not strategies:
        logger.error('Sin estrategia definida')
        print('Sin estrategia definida')
        return

    parser = get_parser()
    args = parser.parse_args(args)

    if args.hides:
        SHOW_HIDES = True

    if args.sellall:
        balances = db_open_trades('all', False, SHOW_HIDES)
        for b in balances:
            symbol = b['symbol']
            asset = symbol.replace('/' + CURRENCY,'')
            mark = get_strategy(b['eid'])['mark']
            info = "Manual SELL"
            status_mark = False
            monitor = False
            print("{} {}".format(b['id'], b['symbol']))
            class Args:
                side = 'sell'
            argsSell = Args()
            argsSell.close = asset
            argsSell.tipo_sesion = mark
            loop = asyncio.get_event_loop()
            profit_sell = False
            try:
                profit_sell = loop.run_until_complete(ejecutar_venta(argsSell, info, status_mark, monitor ))
            except Exception as e:
                print(e)
                raise IOError(e)

            if not profit_sell:
                logger.info('[trades] Error vendiendo ' + asset)
                print('[trades] Error vendiendo ' + asset)
            else:
                logger.info('[trades] Vendemos ' + asset + ' Profit: ' + str(profit_sell))
                print('[trades] Vendemos ' + asset + ' Profit: ' + str(profit_sell))

        exit()

    if args.balances:
        # Borramos trades sin cerrar de la DB.
        db_delete_trades('all')
        RECOGER_BALANCES = True

    loop = asyncio.get_event_loop()

    BTC = asyncio.get_event_loop().run_until_complete(fetch_ticker(exchange, 'BTC/USDT'))
    if BTC: btc_price = float(BTC['close'])

    EURUSDT = asyncio.get_event_loop().run_until_complete(fetch_ticker(exchange, 'EUR/USDT'))
    if EURUSDT:
        eurusdt_price = float(EURUSDT['close'])
    else:
        eurusdt_price = 1.2

    if args.exchange:
        print("Exchange: ", args.exchnage)

    print("# Informe exchanges\n")
    print('Precio actual BTC: ', btc_price)
    print('Precio del euro respecto al dolar: ', eurusdt_price)

    signal(SIGINT, total)

    os.system('clear')

    salida_telegram = ''
    for s in strategies.split():
        salida_telegram += "\n" + "Estrategia: " + s
        salida_telegram += show_config(s)

    if os.getenv('TELEGRAM_CHAT_ID'):
        tlg_send("**Inicamos trade**\n```\nConfig general:\n{}\n{}\nTestNet: {}\n```".format(
            show_config_general(),
            salida_telegram,
            TESTNET
            ))

    if SIMULA_COMPRAS_VENTAS:
        logger.info('Simulación activada')

    while True:
        total_exchanges_usd = 0
        start_time = time.time()
        loop.run_until_complete(trades(exchange))
        end_time = time.time()
        diff_time = end_time - start_time
        segundos_entre_ciclos = 5
        if diff_time < segundos_entre_ciclos:
            time.sleep(segundos_entre_ciclos - diff_time)

    # loop.run_until_complete(balance(kraken))


if __name__ == '__main__':
    main()
