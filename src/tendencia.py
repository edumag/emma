#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## @file market.py
## @brief Mostrar tendencia del precio.

import sys
import ccxt.async_support as ccxt  # noqa: E402
import argparse
from datetime import datetime
import matplotlib.pyplot as plt
import asyncio
import nest_asyncio
nest_asyncio.apply()

sys.path.append('./src')

from base import *
from console import *
from db import *
from reports import *
from Tendencias import *

__version__ = '0.1.0'
__author__ = u'edu@lesolivex.com'

FORMATO_SALIDA  = False

def valoracion_mercado():
    ### Valoramos datos a nivel general.

    results = {}

    report = report_status_market()
    if not report:
        return False

    for temp in report:

        # precios_general, tendencias_general, volumenes_general, adx, macd_general, media_market = report[temp]
        if temp == 'all':
            temp1, temp2, temp3, temp4, temp5, media_market= report[temp]
            monitor = "{}:{}:{}:{}:{}={}".format(temp1, temp2, temp3, temp4, temp5, media_market)
        else:
            precios_general, tendencias_general, macd_general, media_market = report[temp]
            monitor = "{}:{}:{}={}".format(precios_general, tendencias_general, macd_general, media_market)

        if media_market > 70:
            result = 'UP+'
            result_color = green(result)
        elif media_market > 50:
            result = 'UP'
            result_color = blue(result)
        elif media_market > 3:
            result = 'DOWN'
            result_color = orange(result)
        elif media_market > 0:
            result = 'DOWN+'
            result_color = red(result)
        else:
            result = 'F'
            result_color = bold("False")

        results[temp] =  [result, media_market, monitor, result_color]

    return results

async def valorar_compra(coin, mark):

    config_general = get_config_general()
    config = get_config(mark)
    strategy = get_strategy(get_eid(mark))

    total_trades, total_losses, buy_symbols, last_two, total_stoploss_positive = await report_status(False, False, False, False)
    total_trades_strategy, total_losses_strategy, buy_symbols_strategy, last_two_strategy, total_stoploss_positive_strategy = await report_status(False, False, False, mark)
    total_trades_strategy_test, total_losses_strategy_test, buy_symbols_strategy_test, last_two_strategy_test, total_stoploss_positive_strategy_test = await report_status(False, True, False, mark)

    # Miramos que no estemos en barrena.
    if FOLLOW_LAST_TWO and int(FOLLOW_LAST_TWO) > 0:
        if last_two_strategy_test and last_two_strategy_test > 1:
            logger.info('[{}][{}] Valoración de compra negativa, last_two_strategy_test [{}] > 1'.format(coin, mark, last_two_strategy_test))
            return False
        if last_two_strategy_test is False:
            logger.info('[{}][{}] Valoración de compra negativa, Sin last_two_strategy_test [{}]'.format(coin, mark, last_two_strategy_test))
            return False

    ## status_strategy_in_test.
    if FOLLOW_STATUS_IN_TEST and int(FOLLOW_STATUS_IN_TEST) > 0:
        status_strategy = status_strategy_in_test(mark)
        if status_strategy < 1:
            logger.info('[{}][{}] Valoración de compra negativa, status_strategy_in_test [{:.2f}] < 1'.format(coin, mark, status_strategy))
            return False

    ###
    ### @todo Si status_strategy_in_test es mayor a 2 podríamos devolver el 2
    ### para que pueda por ejemplo subir el monto o el numero de operaciones
    ### por estrategia.
    ###

    trades_sin_asegurar = total_trades - total_stoploss_positive
    trades_sin_asegurar_strategy = total_trades_strategy - total_stoploss_positive_strategy

    # Valoración de mercado.
    # @todo Anulamos valoración mercado hasta mejorar resultados.
    # valoracion_mercado_value = False
    # results_valoracion_mercado = await valoracion_mercado()

    # if results_valoracion_mercado:
    #     result_strategy, media_market, monitor_market, result_strategy_color = results_valoracion_mercado[strategy['temporalidad']]
    #     if result_strategy[:2] == 'UP':
    #         valoracion_mercado_value = True
    # else:
    #     result_strategy, media_market, monitor_market, result_strategy_color = [False, False, False, False]

    # if not valoracion_mercado_value and strategy['type'] != 'SHORT':
    #     logger.info('[{}][{}] La valoración del mercado no es positiva [{}]'.format(coin, mark, result_strategy_color))
    #     return False

    if not config['compras_activadas'] or config['compras_activadas'] != 1:
        logger.info('[' + coin + '][' + mark + '] Compras desactivadas en configuración de estrategia')

    elif config_general['max_trades'] == 0:
        logger.info('[{}][{}] max_trades general [{}], no compramos'.format(coin, mark, config_general['max_trades']))

    elif config['max_trades'] == 0:
        logger.info('[{}][{}] max_trades de estrategia [{}], no compramos'.format(coin, mark, config['max_trades']))

    elif not config_general['compras_activadas'] or config_general['compras_activadas'] != 1:
        logger.info('[' + coin + '][' + mark + '] Compras desactivadas en configuración de general')

    elif total_trades != 0 and trades_sin_asegurar >= config_general['max_trades']:
        logger.info('[{}][{}] max_trades general [{}], no compramos, trades_sin_asegurar [{}]'.format(coin, mark, config_general['max_trades'], trades_sin_asegurar))

    elif total_trades_strategy != 0 and trades_sin_asegurar_strategy >= config['max_trades']:
        logger.info('[{}][{}] max_trades de estrategia [{}], no compramos, trades_sin_asegurar_strategy [{}]'.format(coin, mark, config['max_trades'], trades_sin_asegurar_strategy))

    elif total_losses != 0 and total_losses >= config_general['max_losses']:
        logger.info('[{}][{}] Total losses [{}], no compramos por config general, max_losses [{}]'.format(coin, mark, total_losses, config_general['max_losses']))

    elif total_losses != 0 and total_losses_strategy >= config['max_losses']:
        logger.info('[{}][{}] Total losses [{}], no compramos, max_losses de estrategia [{}]'.format(coin, mark, total_losses, config['max_losses']))

    elif (not total_trades and total_trades != 0 ) or ( not total_losses and total_losses != 0):
        logger.error('[{}][{}] Sin información de estado de trades actuales no compramos {}/{}'.format(coin, mark, total_trades, total_losses))

    else:
        logger.info('[{}][{}] Valoración de compra positiva'.format(coin, mark))
        return True

    return False

def valorar_venta(coin, profit, status, stoploss=False, mark=False, conn=False, test=False):
    """
    @todo Separar test.
    @todo Repasar prioridades y documentar.
    """

    config_general = get_config_general()
    strategies = config_general['estrategias_activadas']

    if not mark:
        mark = strategies.split()[0]

    config = get_config(mark, conn)

    profit = round(profit, 2)

    if not test:
        if not config['ventas_activadas']:
            logger.info('[' + coin + '][' + mark + '] ventas desactivadas en configuración de estrategia')
            return False

        if not config_general['ventas_activadas']:
            logger.info('[' + coin + '][' + mark + '] ventas desactivadas en configuración de general')
            return False

        if coin in config_general['descartar_ventas'].split():
            logger.info('[' + coin + '][' + mark + '] Profit: [' + str(profit) +  '] moneda descartada de vender en config general')
            return False

    # Si tenemos definido un stopLoss en trade.
    if stoploss:
        if profit < stoploss:
            if not test:

                if profit < 0 and not config['sell_with_loss']:
                    logger.debug('[StopLoss][' + coin + '][' + mark + '] Profit[' + str(profit) +  '] stoploss[' + str(config['stoploss'])  + '] Status[' + str(status) + '][' + red('No') + '] stoploss de trade superado, sell_with_loss desactivado')
                    return False

                logger.info('[StopLossTrade] [' + coin + '][' + mark + '] Profit: [' + str(profit) +  ']  Status: [' + str(status) + '] [' + green('Si') + ']')
            else:
                logger.debug('[StopLossTrade][Test][' + coin + '][' + mark + '] Profit: [' + str(profit) +  ']  Status: [' + str(status) + '] [' + green('Si') + ']')
            return 'StopLossTrade'
        else:
            logger.debug('[StopLossTrade] [' + coin + '][' + mark + '] Profit: [' + str(profit) +  ']  Status: [' + str(status) + '] [' + red('No') + ']')

    if not stoploss and config['stoploss'] and profit < config['stoploss']:
        if not test:
            if profit < 0 and not config['sell_with_loss']:
                logger.debug('[StopLoss][' + coin + '][' + mark + '] Profit[' + str(profit) +  '] stoploss[' + str(config['stoploss'])  + '] Status[' + str(status) + '][' + red('No') + '] stoploss de estrategia sobrepasado, sell_with_loss desactivado')
                return False
            logger.info('[StopLoss][' + coin + '][' + mark + '] Profit[' + str(profit) +  '] stoploss[' + str(config['stoploss'])  + '] Status[' + str(status) + '][' + green('Si') + ']')
        else:
            logger.debug('[StopLoss][Test][' + coin + '][' + mark + '] Profit[' + str(profit) +  '] stoploss[' + str(config['stoploss'])  + '] Status[' + str(status) + '][' + green('Si') + ']')

        return 'StopLoss'

    if config['follow_losses'] and config['follow_losses'] > profit:

        # Estado de estrategia en test.
        status_strategy = status_strategy_in_test(mark)

        # Si hemos llegado a follow_losses vendemos solo con SELL- o SELL, con SELL+ aún puede subir con el tiempo.
        if status == 'CLOSE-' or status == 'CLOSE':
            if not test:
                if profit < 0 and not config['sell_with_loss']:
                    logger.debug('[FollowLosses][' + coin + '][' + mark + '] Profit[' + str(profit) +  '] FollowLosses:[' + str(config['follow_losses']) + '] Status[' + str(status) + '][' + red('No') + '] No vendemos con perdidas')
                    return False
                logger.info('[FollowLosses][' + coin + '][' + mark + '] Profit[' + str(profit) +  '] FollowLosses:[' + str(config['follow_losses']) + '] Status[' + str(status) + '][' + green('Si') + ']')
            else:
                logger.debug('[FollowLosses][Test][' + coin + '][' + mark + '] Profit[' + str(profit) +  '] FollowLosses:[' + str(config['follow_losses']) + '] Status[' + str(status) + '][' + green('Si') + ']')
            return 'FollowLosses'

        elif status_strategy < 0:
            logger.info('[{}][{}] FollowLosses y status_strategy_in_test [{:.2f}] < 0 [{}]'.format(coin, mark, status_strategy, green('Si')))
            return 'FollowLossesSST'

        else:
            logger.debug('[FollowLosses] [' + coin + '][' + mark + '] Profit: [' + str(profit) +  ']  Status: [' + str(status) + '] [' + red('No') + ']')

    if config['min_profit_sell'] and profit > config['min_profit_sell']:
        # SI hemos llegado a TakeProfit vendemos en una señal de venta.
        if status == 'CLOSE+' or status == 'CLOSE-' or status == 'CLOSE':
            if not test:
                logger.info('[TakeProfit][' + coin + '][' + mark + '] Profit[' + str(profit) +  '] MinPrSell[' + str(config['min_profit_sell']) + '] Status[' + str(status) + '][' + green('Si') + ']')
            else:
                logger.debug('[TakeProfit][Test][' + coin + '][' + mark + '] Profit[' + str(profit) +  '] MinPrSell[' + str(config['min_profit_sell']) + '] Status[' + str(status) + '][' + green('Si') + ']')
            return 'TakeProfit'
        else:
            logger.debug('[TakeProfit] [' + coin + '][' + mark + '] Profit: [' + str(profit) +  ']  Status: [' + str(status) + ']  [' + red('No') + ']')

    if test:
        logger.debug('[Test][' + coin + '][' + mark + '] Profit: [' + str(profit) +  ']  Status: [' + str(status) + '] [' + red('No') + ']')
    else:
        logger.debug('[' + coin + '][' + mark + '] Profit: [' + str(profit) +  ']  Status: [' + str(status) + '] [' + red('No') + ']')

    return False

def valorar_datos(macd, tendencia, precio, volumen, adx, mark=False, conn=False, condition_signal=False):
    """
    CLOSE+ CLOSE-
    Diferenciamon entre venta cuando estamos en TakeProfit y cuando estamos por debajo del StopLoss.
    Por debajo del StopLoss si tenemos la tendencia alcista esperamos, sino vendemos en TakeProfit.
    """

    if not mark:
        mark = DEFAULT_SESION

    if condition_signal:

        signals = [{'accion': 'CLOSE', 'condicion': condition_signal}]

    else:
        # Buscamos señales de estrategia.
        signals = get_signals(mark, conn)
        if not signals:
            msg = 'Estrategia [{}], Sin señales'.format(mark)
            logger.debug(msg)
            return 'NULL' + ' ' + mark

    # recorremos señales para ver si alguna coincide.
    result = False
    for signal in signals:
        condicion = signal['condicion']
        if monitor_test(precio, tendencia, volumen, adx, macd, condicion, conn):
            return signal['accion'] + ' ' + mark

    return 'HOLD' + ' ' + mark

def color_monitor(data, type='macd'):

    datatypes = 'precios tendencias volumenes adx macd'
    data = str(data)

    if data == 'None' or data == 'None None None None None':
        return ''

    def color_data_type(data, type):

        salida = ''
        if len(data) != 6:
            for x in range(0, 6 - len(data)):
                data = '0' + data

        for d in data:
            di = int(d)

            if type == 'macd':
                if di == 5:
                    salida += cero(d)
                elif di > 8:
                    salida += green(d)
                elif di > 5:
                    salida += blue(d)
                elif di > 1:
                    salida += orange(d)
                else:
                    salida += red(d)

            if type == 'tendencias':
                if di == 0 or di == 5:
                    salida += cero(d)
                elif di > 7:
                    salida += green(d)
                elif di > 5:
                    salida += blue(d)
                elif di > 3:
                    salida += red(d)
                else:
                    salida += red(d)

            if type == 'volumenes':
                if di == 5:
                    salida += cero(d)
                elif di > 8:
                    salida += green(d)
                elif di > 5:
                    salida += blue(d)
                elif di > 1:
                    salida += orange(d)
                else:
                    salida += red(d)

            if type == 'adx':
                if di == 5:
                    salida += cero(d)
                elif di > 8:
                    salida += green(d)
                elif di > 5:
                    salida += blue(d)
                elif di > 0:
                    salida += orange(d)
                else:
                    salida += red(d)

            if type == 'precios':
                if di == 5:
                    salida += cero(d)
                elif di > 8:
                    salida += green(d)
                elif di > 5:
                    salida += blue(d)
                elif di > 1:
                    salida += orange(d)
                else:
                    salida += red(d)

        return salida

    if len(data.split()) > 1:
        # Todo junto
        counter = 0
        color = ''
        for t in datatypes.split():
            color += ' ' + color_data_type(data.split()[counter], t)
            counter += 1

    else:
        color = color_data_type(data, type)

    return(color.strip())

def monitor_adx(t, adx_global, multiplo):

    adx_valor = t.adx()

    adx_global = adx_global + (adx_valor * multiplo)

    return adx_global

def monitor_volumen(t, volumen_global, multiplo, precio_subiendo):

    volumen = t.volumen(precio_subiendo)

    volumen_global = volumen_global + (volumen * multiplo)

    return volumen_global

def monitor_precios(t, precios_global, multiplo):

    precio, precio_subiendo, ultimo_precio = t.precio()
    precios_global = precios_global + (precio * multiplo)

    return precios_global, precio_subiendo, ultimo_precio

def monitor_macd(t, macd_global, multiplo):

    macd_puntuacion = t.macd()

    # Puntuación global del par.
    macd_global = int(macd_global) + (macd_puntuacion * multiplo)

    return macd_global

def monitor_mediamovil(t, tend_global, multiplo):
    ### Recoger media movil exponencial de 100 periodos y calcular dirección.

    try:

        tendencia = t.media_movil()
        tend_global = tend_global + (tendencia * multiplo)
        # print("tend_global[{}]".format(tend_global))

    except Exception as e:

        eventos('ERROR_TENDENCIAS', 'Error al buscar tendencia: {}'.format(e), 60)
        return False

    return tend_global

async def tendencia(symbol, sesion=False):

    config_general = get_config_general()
    strategies = config_general['estrategias_activadas']

    global exchange

    if sesion is False:
        sesion = strategies.split()[0]

    status_marks = {}

    # Añadimos TEMPORALIDADES como constante en base.py.
    # temporalidades = ['1d', '4h', '1h', '15m', '5m', '1m']
    temp_multiplos = [100000, 10000, 1000, 100, 10, 1]
    tend_precio_global_srt = ""
    status = 'HOLD'
    count_timeframe = 0
    macd_global = 0
    tend_global = 0
    price_global = 0
    volumen_global = 0
    adx_global = 0

    for timeframe in TEMPORALIDADES:

        try:
            ohlcv = await exchange.fetch_ohlcv(symbol, timeframe)
        except Exception as e:
            logger.info('Error recogiendo información para ' + symbol)
            logger.debug(e)
            await exchange.close()
            return [False, False, False, False]

        if len(ohlcv) < 5:
            await exchange.close()
            print('Error sin datos suficientes en ohlcv')
            return [False, False, False, False]

        # Instancia de Tendencias.
        t = Tendencias(ohlcv)
        tend_global     = monitor_mediamovil(t, tend_global, temp_multiplos[count_timeframe])
        macd_global     = monitor_macd(t, macd_global, temp_multiplos[count_timeframe])
        adx_global      = monitor_adx(t, adx_global, temp_multiplos[count_timeframe])
        price_global, precio_subiendo, ultimo_precio = monitor_precios(t, price_global, temp_multiplos[count_timeframe])
        volumen_global  = monitor_volumen(t, volumen_global, temp_multiplos[count_timeframe], precio_subiendo)

        count_timeframe += 1

    # Recorer estrategias y devolver array de status_marks.
    estrategias = sesion.split()
    if len(estrategias) > 1:
        for eMark in estrategias:
            status_marks[eMark] = valorar_datos(macd_global, tend_global, price_global, volumen_global, adx_global, eMark)

    else:
        status_marks[sesion] = valorar_datos(macd_global, tend_global, price_global, volumen_global, adx_global, sesion)

    if not status_marks:
        status = 'ERROR'
    else:

        status_format = []
        for eMark in status_marks:
            status_mark = status_marks[eMark]
            if not status_mark:
                logger.error('Sin status_mark en status_marks')
                continue
            status = status_mark.split()[0]

            if 'TOPEN' in status:
                status_mark_color = greenup("{:<5} {}".format(status_mark.split()[0], status_mark.split()[1]))
            elif 'TCLOSE' in status:
                status_mark_color = orange("{:<5} {}".format(status_mark.split()[0], status_mark.split()[1]))
            elif 'OPEN' in status:
                status_mark_color = green("{:<5} {}".format(status_mark.split()[0], status_mark.split()[1]))
            elif 'CLOSE' in status:
                status_mark_color = red("{:<5} {}".format(status_mark.split()[0], status_mark.split()[1]))
            else:
                status_mark_color = blue("{:<5} {}".format(status_mark.split()[0], status_mark.split()[1]))

            status_format.append(status_mark_color)

    status_format = " ".join(status_format)
    if precio_subiendo:
        lastPriceStr = green(ultimo_precio)
    else:
        lastPriceStr = red(ultimo_precio)

    await exchange.close()
    monitor = "{} {} {} {} {}".format(price_global, tend_global, volumen_global, adx_global, macd_global)
    monitor_color = "{} {:>18} {}".format(color_monitor(monitor), lastPriceStr, status_format)
    # print(monitor)
    # print(monitor_color)

    return (
        status_marks,
        monitor_color,
        monitor,
        ultimo_precio
    )

def get_parser():
    """
    Creates a new argument parser.
    """
    parser = argparse.ArgumentParser('tendencia')
    version = '%(prog)s ' + __version__
    parser.add_argument('--version', '-v',
                        action='version',
                        version=version
                        )
    parser.add_argument('--symbol',
                        '-s',
                        help='Simbolo, ejemplo ETH/USDT',
                        required=False
                        )
    parser.add_argument('--coin',
                        '-c',
                        help='Moneda, ejemplo ETH',
                        required=False
                        )
    parser.add_argument('--tipo_sesion',
                        '-ts',
                        help='Tipo de sessión, ejemplo: [corta, larga, rapida]. Definidas en configuración',
                        required=False,
                        default=False
                        )
    # parser.add_argument('--timeframe', '-t',
    #                     help='Temporalidad, ejemplo 1h, 4h, 1d',
    #                     required=True
    #                     )
    parser.add_argument('--telegram',
                        '-tg',
                        help='Formato de salida para telegram',
                        required=False,
                        default=False,
                        action="store_true"
                        )

    return parser

def main(args=None):
    """
    Main entry point for your project.
    Args:
        args : list
            A of arguments as if they were input in the command line. Leave it
            None to use sys.argv.
    """

    global FORMATO_SALIDA

    parser = get_parser()
    args = parser.parse_args(args)

    if args.telegram:
        FORMATO_SALIDA = 'telegram'

    if args.symbol:
        symbol = args.symbol

    if args.coin:
        symbol = args.coin + '/' + CURRENCY

    loop = asyncio.get_event_loop()
    salida = loop.run_until_complete(tendencia(symbol, args.tipo_sesion))
    print(salida[1])

if __name__ == '__main__':
    main()
