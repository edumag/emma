#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## @file open_orders.py
## @brief Mostrar las operaciones activas.
##
## Recogemos la lista de los balances y buscamos en el historial el último
## precio al que compramos esa moneda.
## Mostramos la cantidad actual en dolares, fecha de compra, precio de compra
## y el profit actual.

import os
import sys
import argparse
import ccxt.async_support as ccxt  # noqa: E402
import asyncio
import console

sys.path.append('./src')
from base import *
from console import *

async def open_order(exchange, symbol):
    since  = None
    limit  = 20

    orders = await exchange.fetch_open_orders(symbol, since, limit)
    return orders


async def open_orders(exchange, symbols):

    for symbol in symbols:

        orders = await open_order(exchange, symbol)
        for o in orders:
            print("\n## ", o['symbol'])
            print("Type: ", o['type'])
            print("Side: ", o['side'])
            print("Price: ", o['price'])
            print("StopPrice: ", o['stopPrice'])
            print("Amount: ", o['amount'])
            print(CURRENCY + ": ", o['amount'] * o['price'], "$")

            # print(o)

    await exchange.close()


if __name__ == '__main__':

    loop = asyncio.get_event_loop()
    loop.run_until_complete(open_orders(exchange, symbols))
