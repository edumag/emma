#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## @file create_order.py
## @brief Crear ordenes.

__version__ = '0.1.0'
__author__ = u'edu@lesolivex.com'

import asyncio
import os
import sys
import ccxt.async_support as ccxt  # noqa: E402
import argparse
from pprint import pprint
sys.path.append('./src')
from base import *
from db import *
from console import *

def create_order_oco(exchange, symbol, amount, price, stop_price, stop_limit_price):

    market = exchange.market(symbol)

    response = exchange.private_post_order_oco({
        'symbol': market['id'],
        'side': 'SELL',  # SELL, BUY
        'quantity': exchange.amount_to_precision(symbol, amount),
        'price': exchange.price_to_precision(symbol, price),
        'stopPrice': exchange.price_to_precision(symbol, stop_price),
        'stopLimitPrice': exchange.price_to_precision(symbol, stop_limit_price),  # If provided, stopLimitTimeInForce is required
        'stopLimitTimeInForce': 'GTC',  # GTC, FOK, IOC
        # 'listClientOrderId': exchange.uuid(),  # A unique Id for the entire orderList
        # 'limitClientOrderId': exchange.uuid(),  # A unique Id for the limit order
        # 'limitIcebergQty': exchangea.amount_to_precision(symbol, limit_iceberg_quantity),
        # 'stopClientOrderId': exchange.uuid()  # A unique Id for the stop loss/stop loss limit leg
        # 'stopIcebergQty': exchange.amount_to_precision(symbol, stop_iceberg_quantity),
        # 'newOrderRespType': 'ACK',  # ACK, RESULT, FULL
    })

    return response

def cancel_order(id):
    exchange.cancel_order (id)

async def create_order(args, confirmar=False):
    ## symbol = 'ETH/BTC'
    ## type = 'limit'  # or 'market', other types aren't unified yet
    ## side = 'sell'
    ## amount = 123.45  # your amount
    ## price = 54.321  # your price
    ## # overrides
    ## params = {
    ##     'stopPrice': 123.45,  # your stop price
    ##     'type': 'stopLimit',
    ## }
    ## order = exchange.create_order(symbol, type, side, amount, price, params)
    ## @todo Aplicar confirmar

    global exchange

    if hasattr(args, 'exchange') and args.exchange:
        exchange = args.exchange

    # symbol, price, amount, side, type
    symbol = args.symbol
    price_user = None
    if hasattr(args, 'price') and args.price:
        price_user = args.price
    side = 'buy'
    type = 'market'
    fee = 0
    params = {}
    order = None

    if hasattr(args, 'side') and args.side: side = args.side
    if hasattr(args, 'type') and args.type: type = args.type

    try:
        ticker = await exchange.fetch_order_book(symbol)
        price = float(ticker['asks'][2][0])
    except Exception as e:
        logger.error('Error al buscar precio de moneada [{}].'.format(symbol))
        return [False, 'Error al buscar precio de moneada [{}].'.format(symbol), False]


    if hasattr(args, 'amount_dolars') and float(args.amount_dolars) > 0:
        # Recoger precio actual de la moneda para poder calcular el monto a enviar.
        amount = float(args.amount_dolars) / price
        print(str(round(float(args.amount_dolars),2)) + '$')
    else:
        if hasattr(args, 'amount') and args.amount:
            amount = float(args.amount)
            print(str(amount) + ' ' + symbol.replace('/' + CURRENCY,''))
        else:
            print('Error: Es necesario pasar el monto en la moneda a comprar o en dolares')
            await exchange.close()
            exit(1)

    if side == 'buy':
        # Es una compra.
        # params = {
        #     'quoteOrderQty': exchange.cost_to_precision(symbol, amount)
        # }
        params = {'quoteOrderQty': round(amount,5)}
        # amount = exchange.cost_to_precision(symbol, amount)
        amount = round(amount,5)

    print("{}: {} Amount: {} {}$ Price: {}".format(
        side,
        symbol,
        str(amount),
        str(round((price * float(amount)), 2)),
        str(price)
    ))

    # pprint(exchange.markets[symbol])
    # exit()

    try:
        # type = 'limit'  # or market
        # const buyTransaction = await binance.createOrder(currencyPair, 'market', 'buy', undefined, undefined, params)
        # order = await exchange.create_order(symbol, type, side, amount, price, params)
        # pprint(order)

        if side == 'sell':
            if SIMULA_COMPRAS_VENTAS:
                return [True, [
                    111,
                    symbol,
                    amount,
                    price,
                    price * amount,
                    datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                    'Filled',
                    'sell',
                    'market',
                    fee,
                    True
                ], ("{'info': 'SIMULA_COMPRAS_VENTAS'}")]
            else:

                venta_realizada = False
                try:
                    order = await exchange.create_order(symbol=symbol, type='market', side='sell', amount=amount)
                except ccxt.InsufficientFunds as e:
                    msg = "Fondos insuficientes, probamos con un 0.3% menos"
                    logger.error(e)
                    logger.info(msg)
                    print(msg)
                    porcentaje = (0.3/100) * amount
                    amount = amount - porcentaje
                    round(amount, 5)
                    order = await exchange.create_order(symbol=symbol, type='market', side='sell', amount=amount)

        else:
            # order = await exchange.create_market_buy_order(symbol, amount)
            if SIMULA_COMPRAS_VENTAS:
                return [True, [
                    111,
                    symbol,
                    amount,
                    price,
                    price * amount,
                    datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                    'Filled',
                    'buy',
                    'market',
                    fee,
                    True
                ], ("{'info': 'SIMULA_COMPRAS_VENTAS'}")]
            else:
                order = await exchange.create_limit_buy_order(symbol, amount, price)

    except ccxt.InsufficientFunds as e:
        logger.error(e)
        print('create_order() failed – not enough funds')
        print(e)
        await exchange.close()
        return [False, e, order]
    except Exception as e:
        logger.error(e)
        print('create_order() failed')
        print(e)
        await exchange.close()
        return [False, e, order]

    await exchange.close()

    if order['fee']:
        fee = order['fee']['cost']

    return [True, (
        order['id'],
        order['symbol'],
        order['amount'],
        order['price'],
        order['cost'],
        order['datetime'],
        order['status'],
        order['side'],
        order['type'],
        fee,
        order['filled']
    ), (str(order))]

def get_parser():
    """
    Creates a new argument parser.
    """
    parser = argparse.ArgumentParser('balances')
    version = '%(prog)s ' + __version__
    parser.add_argument('--version', '-v', action='version', version=version)
    parser.add_argument('--exchange', '-e', help='Default: binance')
    parser.add_argument('--symbol',
                        '-s',
                        help = 'Simbolo, ejemplo: ETH/USDT.',
                        default = 'binance',
                        required = True
                        )
    parser.add_argument('--side',
                        '-sd',
                        help='Ejemplo: buy, sell, OCO',
                        default = 'buy',
                        required=False
                        )
    parser.add_argument('--type',
                        '-t',
                        help='Ejemplo: Market or Stop or StopLimit.',
                        default = 'market',
                        required=False
                        )
    parser.add_argument('--amount',
                        '-a',
                        help='Cantidad, ejemplo: 1000, 100, ...',
                        required=False
                        )
    parser.add_argument('--amount_dolars',
                        '-ad',
                        help='Cantidad a comprar en dolares, ejemplo: 1000, 100, ...',
                        required=False
                        )
    parser.add_argument('--price',
                        '-p',
                        help='Precio.',
                        required=False
                        )
    return parser

def main(args=None):
    """
    Main entry point for your project.
    Args:
        args : list
            A of arguments as if they were input in the command line. Leave it
            None to use sys.argv.
    """

    parser = get_parser()
    args = parser.parse_args(args)

    # loop = asyncio.get_event_loop()
    # loop.run_until_complete(balance(kraken))
    asyncio_loop = asyncio.get_event_loop()
    salida = asyncio_loop.run_until_complete(create_order(args))
    if salida:
        print("Order")
        print('-----')
        print('id: '      , salida[0])
        print('symbol: '  , salida[1])
        print('amount: '  , salida[2])
        print('cost: '    , salida[3])
        print('datatime: ', salida[4])
        print('status: '  , salida[6])
        print('side: '    , salida[7])
        print('type: '    , salida[8])
        print('fee: '     , salida[9])
        print('filled: '  , salida[10])


if __name__ == '__main__':
    main()
