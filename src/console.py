#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## @file console
## @brief funciones para consola.
##
## https://en.wikipedia.org/wiki/ANSI_escape_code#Colors

def style(s, style):
    return style + str(s) + '\033[0m'

def green(s):
    return style(s, '\033[92m')

def blue(s):
    return style(s, '\033[94m')

def cero(s):
    return style(s, '\033[98m')

def red(s):
    return style(s, '\033[91m')

def orange(s):
    return style(s, '\033[33m')
def greenup(s):
    return style(s, '\033[36m')
def pink(s):
    return style(s, '\033[95m')

def bold(s):
    return style(s, '\033[1m')

def underline(s):
    return style(s, '\033[4m')

def barra(s):
    style = 0
    fg = 30
    bg = 47
    format = ';'.join([str(style), str(fg), str(bg)])
    s1 = '\x1b[%sm%s\x1b[0m' % (format, s)
    return s1

def print_format_table():
    """
    prints table of formatted text format options
    """
    for style in range(8):
        for fg in range(30, 38):
            s1 = ''
            for bg in range(40, 48):
                format = ';'.join([str(style), str(fg), str(bg)])
                s1 += '\x1b[%sm %s \x1b[0m' % (format, format)
            print(s1)
        print('\n')

def color_result(result, concatenamos='', invertido=False):

    if not result:
        result = 0
    if not concatenamos:
        concatenamos = ''

    result = round(result, 2)

    if invertido:
        if result > 0:
            return red(str(result) + concatenamos)
        else:
            return green(str(result) + concatenamos)
    else:
        if result > 0:
            return green(str(result) + concatenamos)
        elif result < 0:
            return red(str(result) + concatenamos)
        else:
            return cero(str(result) + concatenamos)

def colores(num, text):
    list = [
        'green',
        'blue',
        'orange',
        'pink',
        'greenup',
    ]
    if not text:
        text = 0
    if num > (len(list) - 1):
        num = num - (len(list) + 1)
        if num > (len(list) - 1):
            num = num - (len(list) + 1)
    f = str(list[num]) + '("' + str(text) + '")'
    s = eval(f)
    return s

if __name__ == '__main__':
    print_format_table()
