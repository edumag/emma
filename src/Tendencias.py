#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# @file Tendencia.py
# @brief Mostrar tendencia del mercado.

import pandas as pd
import pandas_ta as ta
from base import *

class Tendencias():

    def __init__(self, ohlcv):

        self.ohlcv = ohlcv
        self.df = pd.DataFrame(ohlcv, columns = ['time', 'open', 'high', 'low', 'close', 'volume'])

        i = len(self.df) - 1

        if i < 2:
            print('Error dataframe menor a 5, len:' + str(i))
            return False

    def media_movil(self):
        # Recoger media movil exponencial de 100 periodos y calcular dirección.

        df = self.df[['close']]
        i = len(df) - 1

        if i < 100:
            raise TendenciasException(self, 'Dataframe sin datos suficientes')
            #raise Exception('Datos insuficientes')
        df.reset_index(level=0, inplace=True)
        df.columns = ['ds', 'y']
        tend = df.y.ewm(span=100, adjust=False).mean()
        # Último punto.
        tend_ultima = tend[i]
        # Cogemos 10 periodos anteriores para obtener una linea que nos de más
        # información de su dirección.
        tend_anterior = tend[i-10]
        tend_max = tend.max()
        tend_min = tend.min()

        # Tendencia general.
        # De 6 a 9 sube, más alto el número más potencia de subida.
        # De 0 a 4 baja, más bajo el número más potencia de bajada.
        porcentaje_mm100 = ((tend_ultima * 100) / tend_anterior) - 100

        if tend_ultima > tend_anterior:

            if porcentaje_mm100 > 3:
                tendencia = 9
            elif porcentaje_mm100 > 2:
                tendencia = 8
            elif porcentaje_mm100 > 1:
                tendencia = 7
            elif porcentaje_mm100 > 0.01:
                tendencia = 6
            else:
                tendencia = 5

        elif tend_ultima < tend_anterior:
            porcentaje_mm100 = abs(porcentaje_mm100)
            if porcentaje_mm100 > 4:
                tendencia = 0
            elif porcentaje_mm100 > 3:
                tendencia = 1
            elif porcentaje_mm100 > 2:
                tendencia = 2
            elif porcentaje_mm100 > 1:
                tendencia = 3
            elif porcentaje_mm100 > 0.01:
                tendencia = 4
            else:
                tendencia = 5

        else:
            tendencia = 0

        # print('----------------------------------------')
        # print('porcentaje_mm100: ' + str(porcentaje_mm100))
        # print('Última: ' + str(tend_ultima) + ' Anterior: ' + str(tend_anterior))
        # print('Mínima: ' + str(tend_min) + ' Máxima: ' + str(tend_max))
        # print('Resultado: ' + str(tendencia))
        # print('----------------------------------------')

        return tendencia

    def macd(self):

        df = self.df[['close']]
        df.reset_index(level=0, inplace=True)
        df.columns=['ds','y']
        exp1 = df.y.ewm(span=12, adjust=False).mean()
        exp2 = df.y.ewm(span=26, adjust=False).mean()
        macd = exp1-exp2
        signal = macd.ewm(span=9, adjust=False).mean()
        histogram = macd - signal
        i = len(macd) - 1

        if i == -1:
            logger.info('Sin macd no podemos calcular')
            return 5 # Valor neutral ni sube ni baja.

        # Señales de compra.
        #
        # 9: El histograma acaba de empezar a subir desde abajo.
        # 8: LLeva subiendo desde abajo entre 2 periodos y 4.
        #
        # 7: El histograma acaba de empezar a subir desde arriba.
        # 6: LLeva subiendo desde arriba más de 2.
        #
        # 5. Ni sube ni baja.
        #
        # 4: Comienza bajada desde arriba ( 1 periodo ).
        # 3: Continua bajada desde arriba ( más de 1 periodo ).
        #
        # 2: Comienza bajada desde abajo ( 1 periodo ).
        # 1: Continua bajada desde abajo ( más de 1 periodo ).

        macd_puntuacion = 5

        # Detectar si es relevante o no el histograma por el tamaño.
        histogram_width = 0
        ultimo_valor = histogram[i]  # last closing price
        media_valor = histogram.mean()
        max_valor = histogram.max()
        if ultimo_valor < 0:
            ultimo_valor = abs(ultimo_valor)

        if ultimo_valor != 0 and max_valor != 0:
            porcentaje_histogram = ((ultimo_valor * 100) / max_valor)
        elif ultimo_valor != 0:
            porcentaje_histogram = ultimo_valor
        else:
            porcentaje_histogram = max_valor

        # Histograma de subida.
        # Mostramos los periodos que lleva la tendencia.
        y = 1
        periocidad = 1
        if histogram[i] > histogram[i - 1]:
            # Buscamos desde que momento esta subiendo.
            while y < 9:
                if histogram[i - y] > histogram[i - y - 1]:
                    y += 1
                    # print(timeframe, ' ', y, ': ', histogram[i-y], '-', histogram[i-y-1])
                else:
                    break
            periocidad = y
        elif i > 10 and histogram[i] < histogram[i - 1]:
            while y < 9:
                if histogram[i - y] < histogram[i - y - 1]:
                    y += 1
                else:
                    break
            periocidad = periocidad - y
        else:
            print('Error: sin información')
            return 5 # Valor neutral ni sube ni baja.

        # Subida.
        if periocidad > 0:

            # Sube desde abajo.
            if histogram[i] < 0:

                if periocidad == 1 and porcentaje_histogram > 5:
                    macd_puntuacion = 9
                else:
                    macd_puntuacion = 8

            # Sube desde arriba.
            else:

                if periocidad == 1 and porcentaje_histogram > 5:
                    macd_puntuacion = 7
                else:
                    macd_puntuacion = 6

        # Bajada.
        else:

            # Bajada desde abajo.
            if histogram[i] < 0:

                if periocidad == -1 and porcentaje_histogram > 5:
                    macd_puntuacion = 3
                else:
                    macd_puntuacion = 4

            # Bajada desde arriba.
            else:

                if periocidad == -1 and porcentaje_histogram > 5:
                    macd_puntuacion = 0
                elif periocidad > -4:
                    macd_puntuacion = 1
                else:
                    macd_puntuacion = 2

        return macd_puntuacion

    def adx(self):
        ### Mostramos el estado de dm+ siempre que el adx este
        ### por encima de 23.

        # ADX
        adx = self.df.ta.adx()

        # ADX
        # Tendencia alcista o bajista
        # ADX_14   DMP_14     DMN_14
        # Última vela.
        adx_14 = adx.iat[-1,0]
        dm_positivo_14 = adx.iat[-1,1]
        dm_negativo_14 = adx.iat[-1,2]
        dm_positivo_14_anterior = adx.iat[-2,1]
        fuerza_subida_dm_positivo = 0

        adx_valor = 0
        if adx_14 != adx_14 or adx_14 < 23:
            adx_valor = 5
        else:

            fuerza_subida_dm_positivo = ((dm_positivo_14 * 100) / dm_positivo_14_anterior) -100

            if fuerza_subida_dm_positivo > 0:
                if fuerza_subida_dm_positivo > 9:
                    adx_valor = 9
                elif fuerza_subida_dm_positivo > 3:
                    adx_valor = 8
                elif fuerza_subida_dm_positivo > 1:
                    adx_valor = 7
                elif fuerza_subida_dm_positivo > 0.1:
                    adx_valor = 6
                else:
                    adx_valor = 5

            else:
                if fuerza_subida_dm_positivo < -9:
                    adx_valor = 0
                elif fuerza_subida_dm_positivo < -5:
                    adx_valor = 1
                elif fuerza_subida_dm_positivo < -3:
                    adx_valor = 2
                elif fuerza_subida_dm_positivo < -1:
                    adx_valor = 3
                elif fuerza_subida_dm_positivo < -0.1:
                    adx_valor = 4
                else:
                    adx_valor = 5

        return adx_valor

    def volumen(self, precio_subiendo):

        # Volumen.
        # De 5 a 9 sube, más alto el número más potencia de subida.
        # De 0 a 4 baja, más bajo el número más potencia de bajada.

        i = len(self.df) - 1
        ultimo_volumen = self.df['volume'][i]  # last closing price
        media_volumen = self.df['volume'].mean()
        # Porcentaje del movimiento del volumen respecto a la media.
        porcentaje_volumen =  round((ultimo_volumen / media_volumen), 5)

        # print("Media de volumen: ", media_volumen)
        # print("Último volumen: ", ultimo_volumen)
        # print("Porcentaje relativo: ", porcentaje_volumen)

        volumen_tendencia = 5

        if precio_subiendo:

            if porcentaje_volumen > 4:
                volumen_tendencia = 9
            elif porcentaje_volumen > 3:
                volumen_tendencia = 8
            elif porcentaje_volumen > 2:
                volumen_tendencia = 7
            elif porcentaje_volumen > 1:
                volumen_tendencia = 6
            elif porcentaje_volumen > 0.5:
                volumen_tendencia = 5

        else:

            porcentaje_volumen = abs(porcentaje_volumen)

            if porcentaje_volumen > 5:
                volumen_tendencia = 0
            elif porcentaje_volumen > 4:
                volumen_tendencia = 1
            elif porcentaje_volumen > 3:
                volumen_tendencia = 2
            elif porcentaje_volumen > 2:
                volumen_tendencia = 3
            elif porcentaje_volumen > 1:
                volumen_tendencia = 4
            elif porcentaje_volumen > 0.5:
                volumen_tendencia = 5

        return volumen_tendencia

    def precio(self):

        # De 5 a 9 sube, más alto el número más potencia de subida.
        # De 0 a 4 baja, más bajo el número más potencia de bajada.

        # .

        # Calcular evolución del precio.
        index = 4  # use close price from each ohlcv candle
        penultimo_precio = self.ohlcv[len(self.ohlcv) - 3][index]  # last closing price
        ultimo_precio = self.ohlcv[len(self.ohlcv) - 1][index]  # last closing price

        # Porcentaje del movimiento del precio respecto a la vela anterior.
        porcentaje =  ((ultimo_precio * 100) / penultimo_precio) -100
        precio_tendencia = 0

        # recogemos el valor del precio en 5m para reflejar si sube o baja.
        precio_subiendo = False

        # print('ultimo_precio: {} penultimo_precio {} porcentaje: {}'.format(
        #     ultimo_precio, penultimo_precio, porcentaje
        # ))

        if ultimo_precio > penultimo_precio:
            precio_subiendo = True
            if porcentaje > 5:
                precio_tendencia = 9
            elif porcentaje > 3:
                precio_tendencia = 8
            elif porcentaje > 1:
                precio_tendencia = 7
            elif porcentaje >= 0.1:
                precio_tendencia = 6
            else:
                precio_tendencia = 5
        else:
            porcentaje = abs(porcentaje)
            if porcentaje > 5:
                precio_tendencia = 0
            elif porcentaje > 3:
                precio_tendencia = 1
            elif porcentaje > 1:
                precio_tendencia = 2
            elif porcentaje >= 0.1:
                precio_tendencia = 3
            elif porcentaje >= 0.01:
                precio_tendencia = 4
            else:
                precio_tendencia = 5

        return precio_tendencia, precio_subiendo, ultimo_precio

class TendenciasException(Exception):

    def __init__(self, tendencias, msg=None):

        if msg is None:
            msg = 'Error en la clase Tendencias'

        super(TendenciasException, self).__init__(msg)
