#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## @file balances.py
## @brief Listado del balance.

__version__ = '0.1.0'
__author__ = u'edu@lesolivex.com'

salida = ''

import os
import sys
import pprint
import ccxt.async_support as ccxt  # noqa: E402
import argparse

sys.path.append('./src')

from base import *
from console import *
from telegram_send_message import *

import asyncio
import csv

# root = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
# sys.path.append(root + '/python')

async def fetch_ticker(exchange, symbol):
    try:

        ticker = await exchange.fetchTicker(symbol)

    except Exception as e:
        print(e)
        return False

    return ticker

BTC = None
btc_price = None

EURUSDT = None
eurusdt_price = None

total_exchanges_usd = 0

async def currency_balance():

    balances = await balance(exchange, False, True)

    total = 0

    for b in balances:
        if b and b != "free" and "free" in b and float(b['free']) > 0:
            asset = str(b['asset'])
            if asset == CURRENCY:
                free   = float(b['free'])
                locked = float(b['locked'])
                total  = free + locked

    return total

async def balance(exchange, show=True, only_return_balances=False):

    total_usd = 0

    global total_exchanges_usd

    salida = "## " + exchange.name + "\n"

    if show:
        print("\n\n## ", exchange, "\n")

    try:
        balances_account = await exchange.fetch_balance()
        if exchange.name == 'Binance':
            balances = balances_account['info']['balances']
        else:
            balances = balances_account
    except ccxt.NetworkError as e:
        print('## ', exchange.id, 'fetch_balance failed due to a network error:', str(e))
        await exchange.close()
        return False, False
    except ccxt.ExchangeError as e:
        print('## ', exchange.id, 'fetch_balance failed due to exchange error:', str(e))
        await exchange.close()
        return False, False
    except Exception as e:
        print('## ', exchange.id, 'fetch_balance failed with:', str(e))
        await exchange.close()
        return False, False

    if only_return_balances:
        await exchange.close()
        return balances

    # colors.dump(colors.green(str(exchange)), 'balance', balances['info'][0])
    for b in balances:

        if exchange.name == 'Binance':
            if b != "free" and "free" in b and float(b['free']) > 0:
                asset = str(b['asset'])
                if asset == CURRENCY:
                    asset_price = 1
                else:
                    symbol = asset + '/' + CURRENCY
                    asset_price = await fetch_ticker(exchange, symbol)
                    if not asset_price:
                        continue
                    asset_price = float(asset_price['close'])

                free = float(asset_price * float(b['free']))
                locked = float(asset_price * float(b['locked']))

                if (free + locked) > 3:
                    total_usd = total_usd + free + locked
                    salida_coin = "{}: {:.2f}$ (locked: {:.2f}$, free: {:.2f}$, price: {:.2f}, euros: {:.2f}€)\n".format(
                        b['asset'],
                        free + locked,
                        locked,
                        free,
                        asset_price,
                        (free + locked) / eurusdt_price
                        )

                    salida += salida_coin

                    if show:
                        print(salida_coin)

        else:  # Kraken

            asset = balances[b]
            if "free" in asset and float(asset['total']) > 0:
                total_asset = float(asset['total'])
                if total_asset > 0:
                    asset = str(b)
                    if asset == CURRENCY:
                        asset_price = 1
                        total_asset_usd = total_asset
                    elif asset == 'EUR':
                        asset_price = eurusdt_price
                        total_asset_usd = total_asset * asset_price
                    else:
                        symbol = asset + '/' + CURRENCY
                        asset_price = await fetch_ticker(exchange, symbol)
                        asset_price = float(asset_price['close'])
                        total_asset_usd = total_asset * asset_price


                    if total_asset_usd > 3:
                        total_usd = total_usd + total_asset_usd
                        salida_formato = "{}: {:.2f}$ (price: {:.2f}, euros: {:.2f}€)\n".format(
                            asset,
                            total_asset_usd,
                            asset_price,
                            total_asset_usd / eurusdt_price
                            )
                        salida += salida_formato
                        if show:
                            print(salida_formato)

    await exchange.close()
    salida_formato = "\n### Total en {}: {:.2f}$ ({:.2f}€)".format(
        exchange.name,
        total_usd,
        total_usd / eurusdt_price
        )

    salida += salida_formato
    if show:
        print(salida_formato)

    total_exchanges_usd = total_exchanges_usd + total_usd

    return total_usd, salida

def view_balances(show=True, telegram=False):

    global salida, exchange, BTC, btc_price, EURUSDT, eurusdt_price

    BTC = asyncio.get_event_loop().run_until_complete(fetch_ticker(exchange, 'BTC/USDT'))
    btc_price = float(BTC['close'])

    EURUSDT = asyncio.get_event_loop().run_until_complete(fetch_ticker(exchange, 'EUR/USDT'))
    if EURUSDT:
        eurusdt_price = float(EURUSDT['close'])
    else:
        eurusdt_price = 1.2


    salida = "# Informe exchanges\n\nPrecio actual BTC: {} \nPrecio del euro respecto al dolar: {}".format(
        btc_price,
        eurusdt_price
    )

    if show:
        print(salida)

    loop = asyncio.get_event_loop()
    total_usd, salida_balance = loop.run_until_complete(balance(exchange, show))
    salida += str(salida_balance)
    # loop.run_until_complete(balance(kraken))

    salida_formato = "\n\n## Total global: {:.2f}$ ({:.2f}€)\n".format(
        total_exchanges_usd,
        total_exchanges_usd / eurusdt_price
    )

    salida += salida_formato
    if show:
        print(salida_formato)

    if telegram:
        tlg_send(salida)

    return salida

def get_parser():
    """
    Creates a new argument parser.
    """
    parser = argparse.ArgumentParser('balances')
    version = '%(prog)s ' + __version__
    parser.add_argument('--version', '-v', action='version', version=version)
    parser.add_argument('--exchange', '-e', help='Exchange, si no definimos todos')
    parser.add_argument('--no_show', '-ns', help='No mostrar resultados por pantalla', action="store_true")
    parser.add_argument('--telegram', '-tel', help='Enviar resultado a telegram', action="store_true")
    parser.add_argument('--currency_balance', '-cb', help='Saldo actual en la moneda de compra', action="store_true")
    return parser

def main(args=None):
    """
    Main entry point for your project.
    Args:
        args : list
            A of arguments as if they were input in the command line. Leave it
            None to use sys.argv.
    """

    parser = get_parser()
    args = parser.parse_args(args)

    show = True
    if args.no_show:
        show = False

    if args.telegram:
        tel = True
    else:
        tel = False

    if args.currency_balance:
        total = asyncio.get_event_loop().run_until_complete(currency_balance())
        print('TOTAL IN {}: {}'.format(CURRENCY, total))
        exit()

    view_balances(show, tel)

    # pprint.pprint(exchange.urls)
    # exit()


if __name__ == '__main__':
    main()
