#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## @file balances.py
## @brief Listado del balance.

__version__ = '0.1.0'
__author__ = u'edu@lesolivex.com'

import sys
import sqlite3
import argparse
import time
import shutil

sys.path.append('./src')

from base import *  # pylint: disable=W0401
from telegram_send_message import *
# @todo sacar de aqui. from tendencia import *  # pylint: disable=W0401
from datetime import datetime

import pdb

EJECUTAR_COMPRAS_AUTOMATICAS_HUMAN = 'compras_activadas'
EJECUTAR_VENTAS_AUTOMATICAS_HUMAN = 'ventas_activadas'
MIN_PROFIT_SELL_HUMAN = 'min_profit_sell'
STOPLOSS_HUMAN = 'stoploss'
FOLLOW_LOSSES_HUMAN = 'follow_losses'
STOPLOSSTRAINIG_HUMAN = 'stoptrailing'
AMOUNT_BUY_HUMAN = 'amount_buy'
MAX_TRADES_HUMAN = 'max_trades'
MAX_LOSSES_TRADES_HUMAN = 'max_losses'
DESCARTAR_VENTAS_HUMAN = 'descartar_ventas'
TIME_TEST_HUMAN = 'time_test'

def db2memo():

    logger.debug('Creamos DB en memoria')

    try:

        # mem = sqlite3.connect('file:cachedb?mode=memory&cache=shared')
        mem = sqlite3.connect(':memory:')
        con = sqlite3.connect(DATABASE)

        query = ''

        for line in con.iterdump():
            if 'estrategias' in line:
                query += "\n" + line
            elif 'senyales' in line:
                query += "\n" + line
            elif 'config' in line:
                query += "\n" + line
            elif 'market' in line:
                query += "\n" + line
            elif 'monitor_test' in line:
                query += "\n" + line

        mem.executescript(query)
        con.close()

    except Exception as e:
        logger.error(e)
        print(e)
        con.close()

    return mem

def monitor_test(precios, tendencias, volumenes, adx, macd, hcondicion, conn=False):

    condicion = human2sql(hcondicion)

    try:
        if conn:
            con = conn
        else:
            con = sqlite3.connect(DATABASE)

        cur = con.cursor()
        sql = """
        INSERT OR REPLACE INTO monitor_test (id, mon_precios, mon_tendencias, mon_volumenes, mon_adx, mon_macd)
        VALUES (?, ?, ?, ?, ?, ?)
        """
        cur.execute(sql, (1, precios, tendencias, volumenes, adx, macd))
        con.commit()
        sql = "SELECT * FROM monitor_test WHERE {}".format(condicion)
        cur.execute(sql)
        rows = cur.fetchall()
        if not conn:
            con.close()
        if len(rows) > 0:
            return True
        else:
            return False
    except Exception as e:
        logger.error(e)
        if not conn:
            con.close()
        if "database is locked" in str(e):
            msg = 'DB bloqueada, esperamos 1s'
            print(msg)
            logger.info(msg)
            time.sleep(1)
            return monitor_test(precios, tendencias, volumenes, adx, macd, hcondicion, conn)
        else:
            msg = "ERROR CON SEÑALES: {}\n{}".format(hcondicion, e)
            eventos('ERROR_SIGNAL', msg)

def get_eid(mark):
    sql = """
    SELECT id FROM estrategias WHERE mark LIKE '{}'
    """.format(mark)
    eids = sql_exec(sql)
    if len(eids) > 0:
        eid = eids[0][0]
    else:
        eid = False
    return eid

def get_strategy(eid):

    sql = """
        SELECT * FROM estrategias WHERE id = {}
        """.format(eid)
    strategy = sql_exec(sql, False, False, True)
    if not strategy or not len(strategy) > 0:
        return False

    return strategy[0]

def get_signals(mark, conn=False):
    try:
        if conn:
            con = conn
        else:
            con = sqlite3.connect(DATABASE)
        con.row_factory = sqlite3.Row
        cur = con.cursor()
        cur.execute("""
        SELECT * FROM senyales s, estrategias e
        WHERE e.id = s.eid AND e.mark LIKE '{}'
        """.format(mark))
        rows = cur.fetchall()
        con.commit()
        if not conn:
            con.close()
        return rows
    except Exception as e:
        if not conn:
            con.close()
        if "database is locked" in str(e):
            msg = 'DB bloqueada, esperamos 5s'
            print(msg)
            logger.info(msg)
            time.sleep(5)
            return get_signals(mark, conn)
        else:
            logger.error(e)
        return False

def format_monitor(data):
    if data and len(data) != 5:
        for x in range(0, 5 - len(data)):
            data = '0' + data
    return data

def db_locks(app, lock=1):
    """
    Dar señales de vida para tener constancia de que esta funcionando.
    @param app Nombre del script.
    @param lock 1/0 Recibimos la señal de funcionando o cerrado.
    """
    date = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    sql = "INSERT OR REPLACE INTO locks (date, app, lock) VALUES ('{}', '{}', {})".format(
        date,
        app,
        lock
        )
    return sql_exec(sql)

def get_lock(app):
    sql = "SELECT * from locks WHERE app like '{}' AND date > datetime('now', 'localtime', '-20 minutes') AND lock == 1".format(
        app
        )
    return sql_exec(sql)

def db_insert_sell(symbol, price_sell, status_sell, monitor_sell, info='Bot', test=False, data_sell=None, fee=None):

    db_insert_market(symbol, price_sell, status_sell, monitor_sell)

    try:

        monitor_sell_org = monitor_sell

        con = sqlite3.connect(DATABASE)
        con.row_factory = sqlite3.Row
        cur = con.cursor()
        date = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        table = 'trades'
        if test:
            table = 'test_trades'

        if not test and SIMULA_COMPRAS_VENTAS:
            info = '[S] ' + info

        if str(symbol).isnumeric():
            sql = "SELECT * FROM " + table + " WHERE id = {}".format(symbol)
        else:
            sql = "SELECT * FROM " + table + " WHERE symbol LIKE '{}' AND date_sell IS NULL".format(symbol)

        ## Si no tenemos la monenda comprada nos volvemos.
        cur.execute(sql)
        rows = cur.fetchall()

        if len(rows) == 0:
            msg = '[db][' + symbol + '] no se ha encontrado compra'
            logger.error(msg)
            print(msg)
            eventos('ERROR_INSERT_SELL', msg)
            return False, False, False

        trade_info = rows[0]

        profit, profit_str = getProfit(trade_info['type'], price_sell, trade_info['price'])

        if not monitor_sell:
            monitor_sell = ['', '', '', '', '']
            logger.error('[{}] sin información de monitor'.format(trade_info['symbol']))
        else:
            monitor_sell = monitor_sell.split()

        sql_where = " WHERE id = {}".format(trade_info['id'])

        sql = """
            UPDATE {} SET
                date_sell = ?,
                info_sell = ?,
                price_sell = ?,
                status_sell = ?,
                mon_precios_sell = ?,
                mon_tendencias_sell = ?,
                mon_volumenes_sell = ?,
                mon_adx_sell = ?,
                mon_macd_sell = ?,
                profit = ?,
                data_sell = ?,
                fee = ?
            """.format(table)

        sql += sql_where

        values = [
            date,
            info,
            price_sell,
            status_sell,
            monitor_sell[0],
            monitor_sell[1],
            monitor_sell[2],
            monitor_sell[3],
            monitor_sell[4],
            profit,
            data_sell,
            fee
            ]

        cur.execute(sql, values)
        # Recoger profit realizado y devolverlo con la respuesta.
        resultat = round(((trade_info['amount'] * trade_info['price']) * profit / 100), 2)
        profit = round(profit, 2)
        con.commit()
        con.close()
        ## @todo Comprobar si esta comprada en test y cerrar si es así.
        if not test:
            buys_test = db_update_open_trade(trade_info['symbol'], price_sell, True, trade_info['eid'])
            if buys_test:
                logger.debug('Cerramos operación en test')
                db_insert_sell(trade_info['symbol'], trade_info['price_sell'], status_sell, monitor_sell_org, 'Close in real', True)

        return [trade_info['id'], profit, resultat]

    except Exception as e:
        logger.error(e)
        raise IOError(e)
        con.close()
        print("Error en db_insert_sell:", e)
        return False, False, False

def db_delete_trades(ids, test=False):

    if ids == 'all':
        condition = ''
    elif ids == 'close':
        condition = ' WHERE date_sell IS NOT NULL'
    elif ids == 'open':
        condition = ' WHERE date_sell IS NULL'
    else:
        condition = ' WHERE id in ({})'.format(ids)

    table = 'trades'
    if test:
        table = 'test_trades'

    sql = 'DELETE FROM {} {}'.format(table, condition)

    try:

        con = sqlite3.connect(DATABASE)
        cur = con.cursor()
        cur.execute(sql)
        con.commit()
        con.close()

    except Exception as e:

        logger.error(e)
        con.close()
        print("Error en db_insert_sell:", e)
        return False

    return True

def is_buy(symbol, test=False, strategy_id=False):
    # saber si una moneda esta comprada.
    # En caso de tener operación abierta en futuros, no abrimos otra.
    # return Objeto sqlite con los campos como claves / False

    request = False

    if strategy_id:
        strategy = get_strategy(strategy_id)
        type = strategy['type']

    try:

        con = sqlite3.connect(DATABASE)
        con.row_factory = sqlite3.Row
        cur = con.cursor()
        table = 'trade_result'
        if test:
            table = 'test_trade_result'

        ## Buscamos si ya tenemos la moneda comprada, si es así no hacemos nada.
        if str(symbol).isnumeric():
            sql = "SELECT * FROM " + table + " WHERE id = " + str(symbol) + " AND date_sell IS NULL"
        else:
            sql = "SELECT * FROM " + table + " WHERE symbol LIKE '" + symbol + "' AND date_sell IS NULL"

        cur.execute(sql)
        rows = cur.fetchall()
        if len(rows) > 0:
            for r in rows:
                if strategy_id and ( strategy_id == r['eid'] ):
                    return rows
                elif not strategy_id:
                    return rows

        con.commit()
        con.close()
        return request

    except Exception as e:
        logger.error(e)
        print(e)
        con.close()
        return False

def db_insert_buy(symbol, amount, price, type=False, leverage=False, status=False, monitor=None, status_market="0", info='Bot', test=False, lock=False, hide=False, data=None):

    if not type:
        type = 'SPOT'
    if not leverage:
        leverage = 1
    if not status:
        status = 'Unknown'
    if not monitor:
        monitor = "0 0 0 0 0"
    if not info:
        info = 'unknown'

    info_org = info

    monitor = monitor.split()

    if len(status.split()) > 1:
        mark = status.split()[1]
    else:
        msg = "Sin estrategia definida añadimos Manual"
        logger.info(msg)
        print(msg)
        mark = 'M'

    eid = get_eid(mark)

    db_insert_market(symbol, price, status, monitor)

    try:

        con = sqlite3.connect(DATABASE)
        cur = con.cursor()
        date = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        table = 'trades'

        if test:
            table = 'test_trades'
            amount = amount * leverage

        if not test and SIMULA_COMPRAS_VENTAS:
            info = '[S] ' + str(info)

        ## Buscamos si ya tenemos la moneda comprada en futuros, si es así no hacemos nada.
        if info_org != 'Manual':
            if is_buy(symbol, test, eid):
                if test:
                    logger.debug('[Test]Moneda [{}] ya comprada en futuros [{}]'.format(symbol, mark))
                else:
                    logger.info('Moneda [{}] ya comprada en futuros [{}]'.format(symbol, mark))
                return False

        if not test:
            logger.info("""Insert OPEN:\neid: {} date: {} symbol: {} amount: {} price: {} info: {} status: {}\nmon_precios: {} mon_tendencias: {} mon_volumenes: {} mon_adx: {} mon_macd: {}\nstatus_market: {} lock: {} hide: {} type: {} leverage: {}""".format(
                eid, date, symbol, amount, price, info, status, monitor[0],
                monitor[1], monitor[2], monitor[3], monitor[4], status_market,
                lock, hide, type, leverage
                )
            )

        sql = "INSERT INTO " + table + """
        (
            eid,
            date,
            symbol,
            amount,
            price,
            info,
            status,
            mon_precios,
            mon_tendencias,
            mon_volumenes,
            mon_adx,
            mon_macd,
            status_market,
            lock,
            hide,
            type,
            leverage,
            data
        )
        VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"""

        cur.execute(sql, (
            eid,
            date,
            symbol,
            amount,
            price,
            info,
            status,
            monitor[0],
            monitor[1],
            monitor[2],
            monitor[3],
            monitor[4],
            status_market,
            lock,
            hide,
            type,
            leverage,
            data
        ))

        con.commit()
        con.close()

        if test:
            logger.debug('[Test][{}][{}][{}][{}][{}][{}][{}]'.format(table, symbol, round(float(amount), 2), price, info, status, " ".join(monitor), status_market))
        else:
            logger.info('[{}][{}][{}][{}][{}][{}][{}][{}]'.format(table, symbol, round(float(amount), 2), price, info, status, " ".join(monitor), status_market))

    except Exception as e:
        logger.error(e)
        raise IOError(e)
        print(e)
        con.close()
        return False

    return True

def db_insert_market(symbol, price, status, monitor):

    msg = ''
    try:
        monitor = monitor.split()
    except Exception as e:
        monitor = [0, 0, 0, 0, 0]

    try:

        con = sqlite3.connect(DATABASE)
        cur = con.cursor()
        date = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

        # ## Solo añadimos registro en caso de que el status haya cambiado.

        # sql = "SELECT * FROM market WHERE symbol LIKE '{}' ORDER BY date desc limit 1".format(symbol)

        # cur.execute(sql)
        # rows = cur.fetchall()
        # if len(rows) > 0:
        #     # print(rows[0])
        #     if rows[0][4] == status:
        #         return
        #     else:
        #         msg = "Cambio de status: {} de {} a {}".format(symbol, rows[0][4], status)

        cur.execute(
            "INSERT INTO market VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
            (
                None,
                date,
                symbol,
                price,
                status,
                format_monitor(monitor[0]),
                format_monitor(monitor[1]),
                format_monitor(monitor[2]),
                format_monitor(monitor[3]),
                format_monitor(monitor[4])
            )
        )

        con.commit()

        con.close()
        return msg

    except Exception as e:
        # raise IOError(e)
        logger.error(e)
        con.close()
        return False

def db_open_trades(symbol, test=False, show_hides=False):

    table = 'trades'
    if test:
        table = 'test_trades'

    if symbol == 'all':
        sql = "SELECT * FROM {} WHERE date_sell IS NULL".format(table)
        if not show_hides:
            sql += " AND hide == 0"
    elif str(symbol).isnumeric():
        sql = "SELECT * FROM {} WHERE id = {} AND date_sell IS NULL".format(table, symbol)
    else:
        sql = "SELECT * FROM {} WHERE symbol like '{}' AND date_sell IS NULL".format(table, symbol)
        if not show_hides:
            sql += " AND hide == 0"

    try:

        con = sqlite3.connect(DATABASE)
        con.row_factory = sqlite3.Row
        cur = con.cursor()
        cur.execute(sql)
        rows = cur.fetchall()
        con.commit()
        con.close()
        return rows

    except Exception as e:
        logger.error(e)
        con.close()
        return False

def db_update_open_trade(symbol, price, test=False, strategy_id=False):

    trades = is_buy(symbol, test)

    if not trades:
        return False

    for trade in trades:

        mark = trade['mark']
        config = get_config(mark)
        buy_price = trade['price']
        buy_profit, buy_profit_str = getProfit(trade['type'], price, buy_price)

        # Registrar nuevo max_profit, min_profit, precio actual y stoploss.
        max_profit = trade['max_profit']
        min_profit = trade['min_profit']
        buy_stoploss = trade['stoploss']

        if not max_profit or buy_profit > max_profit:
            max_profit = round(buy_profit, 2)
        if not min_profit or buy_profit < min_profit:
            min_profit = round(buy_profit, 2)

        # No añadimos stoploss por defecto para que funcione el followlosses en
        # caso de no tener stoploss propio.
        # if not buy_stoploss:
        #     logger.info('[{}] Añadimos stopLoss [{}] de estrategia a trade'.format(
        #         symbol,
        #         config['stoploss']
        #     ))
        #     buy_stoploss = config['stoploss']

        # Si el trade tiene su propio stoptrailing lo utilizamos en vez del
        # de la estrategia.
        msg_info = False
        if trade['stoptrailing']:

            buy_stoploss = calculo_stoptrailing(trade['stoptrailing'], buy_profit, buy_stoploss)

        else:

            buy_stoploss = calculo_stoptrailing(config['stoptrailing'], buy_profit, buy_stoploss)

        if msg_info:
            if test:
                logger.debug(msg_info)
            else:
                logger.info(msg_info)

        db_update_profit(trade['id'], buy_profit, max_profit, min_profit, price, buy_stoploss, test)

    return is_buy(symbol, test, strategy_id)

def db_update_profit(id, profit,  max, min, price, stoploss=None, test=False):

    con = sqlite3.connect(DATABASE)
    cur = con.cursor()

    if not stoploss:
        stoploss = 'Null'

    table = 'trades'

    if test:
        table = 'test_trades'

    sql = "UPDATE {} SET profit = {}, max_profit = {}, min_profit = {}, price_sell = {}, stoploss = {} WHERE id = {} and date_sell IS NULL".format(
        table,
        profit,
        max,
        min,
        price,
        stoploss,
        id
    )

    try:

        cur.execute(sql)
        con.commit()
        con.close()
        return True

    except Exception as e:
        logger.error(e)
        raise IOError(e)
        print('Id: {} max: {} min: {} price: {} stoploss: {} test: {}'.format(
            Id,
            max,
            min,
            price,
            stoploss,
            test
        ))
        con.close()
        return False

    return False

def db_change_stoploss_trade(coin, stoploss, test=False):
    symbol = coin + '/' + CURRENCY
    con = sqlite3.connect(DATABASE)
    cur = con.cursor()
    table = 'trades'
    if test:
        table = 'test_trades'
    sql = "UPDATE {} SET stoploss = '{}' WHERE symbol LIKE '{}' and date_sell IS NULL".format(table, stoploss, symbol)

    try:

        cur.execute(sql)
        con.commit()
        con.close()
        return True

    except Exception as e:
        logger.error(e)
        con.close()
        return False

    return False

def db_change_stoptraining_trade(coin, stoptraining, test=False):
    symbol = coin + '/' + CURRENCY
    con = sqlite3.connect(DATABASE)
    cur = con.cursor()
    table = 'trades'
    if test:
        table = 'test_trades'
    sql = "UPDATE {} SET stoptrailing = '{}' WHERE symbol LIKE '{}' and date_sell IS NULL".format(table, stoptraining, symbol)

    try:

        cur.execute(sql)
        con.commit()
        con.close()
        return True

    except Exception as e:
        logger.error(e)
        con.close()
        return False

    return False

def db_change_strategy_trade(coin, mark, test=False):
    symbol = coin + '/' + CURRENCY
    con = sqlite3.connect(DATABASE)
    cur = con.cursor()
    table = 'trades'
    eid = get_eid(mark)
    if test:
        table = 'test_trades'
    sql = "UPDATE {} SET eid = '{}', stoploss = NULL, stoptrailing = NULL WHERE symbol LIKE '{}' and date_sell IS NULL".format(table, eid, symbol)

    try:

        cur.execute(sql)
        con.commit()
        con.close()
        return True

    except Exception as e:
        logger.error(e)
        con.close()
        return False

    return False

def db_change_status_trade(coin, status, test=False):
    symbol = coin + '/' + CURRENCY
    con = sqlite3.connect(DATABASE)
    cur = con.cursor()
    table = 'trades'
    if test:
        table = 'test_trades'
    sql = "UPDATE {} SET status = '{}' WHERE symbol LIKE '{}' and date_sell IS NULL".format(table, status, symbol)

    try:

        cur.execute(sql)
        con.commit()
        con.close()
        return True

    except Exception as e:
        logger.error(e)
        con.close()
        return False

    return False

def list_fields(table):
    sql = "PRAGMA table_info('{}')".format(table)
    s = ""
    for l in sql_exec(sql):
        s += "\n" + str(l[1])
    return s

def list_tables():
    sql = """SELECT name FROM sqlite_master
    WHERE type='table' or type='view';"""
    s = ""
    for l in sql_exec(sql):
        s += "\n- {}".format(l[0])
    return s

def db_update_trade(args, KWARGS):
    """
    https://docs.python.org/es/3.9/library/argparse.html
    """
    trade_id = args.trade
    con = sqlite3.connect(DATABASE)
    cur = con.cursor()
    table = 'trades'

    if args.test:
        table = 'test_trades'

    sql = "UPDATE {} ".format(table)

    if args.lock:
        sql += "SET lock = {}, ".format(args.lock)

    if args.hide:
        sql += "SET hide = {}, ".format(args.hide)

    if args.monitor:
        sql += "SET monitor = {}, ".format(args.monitor)

    if args.amount:
        sql += "SET amount = {}, ".format(args.amount)

    if args.status:
        sql += "SET status = {}, ".format(args.status)

    if args.precio:
        sql += "SET precio = {}, ".format(args.precio)

    if args.strategy:
        sql += "SET strategy = {}, ".format(args.strategy)

    for a in KWARGS:
        a = a.split('=')
        sql += "SET {} = {}, ".format(a[0], a[1])

    sql = sql.rstrip(', ')

    sql += " WHERE id = {} and date_sell IS NULL".format(trade_id)

    try:

        cur.execute(sql)
        con.commit()
        con.close()
        return True

    except Exception as e:
        logger.error(e)
        con.close()
        return False

    return False
def boolPythonSqlite(value):
    if value:
        return 1
    else:
        return 0

def boolSqlitePython(value):
    if value == 1:
        return True
    else:
        return False

def set_config_general(data=False):
    """
    @brief Construimos configuración de aplicación según configuración y datos de db.
    @param data Dicionario con la información que deseamos cambiar en el
           estado de la aplicación.
    """

    date = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

    dic = {}

    config_general = get_config_general()

    dic['compras_activadas']      = config_general['compras_activadas']
    dic['ventas_activadas']       = config_general['ventas_activadas']
    dic['estrategias_activadas']  = config_general['estrategias_activadas']
    dic['max_trades']             = config_general['max_trades']
    dic['max_losses']             = config_general['max_losses']
    dic['descartar_ventas']       = config_general['descartar_ventas']

    if data:
        for d in data.split():
            key, value = d.split(':')
            dic[key] = value

    sql = 'INSERT INTO config_general (date'

    for x in dic:
        sql += ', ' + x

    sql += ') VALUES ("{}" '.format(date)

    for x in dic:
        sql += ", '{}'".format(str(dic[x]).replace('_', ' '))

    sql += ')'

    con = sqlite3.connect(DATABASE)

    try:

        cur = con.cursor()
        cur.execute(sql)
        con.commit()
        con.close()

    except Exception as e:
        logger.error(e)
        con.close()
        return False

    return dic

def get_config_general():

    sql = '''
    SELECT * FROM config_general ORDER BY id desc LIMIT 1
    '''

    con = sqlite3.connect(DATABASE)
    con.row_factory = sqlite3.Row

    try:

        cur = con.cursor()
        cur.execute(sql)
        rows = cur.fetchall()

        for r in rows:
            return r

        # Si no tenemos niguna configuración añadimos la primera
        return set_config_general("compras_activadas:1 ventas_activadas:1 estrategias_activadas:M")

    except Exception as e:
        if "database is locked" in str(e):
            msg = 'Base de datos bloqueada, esperamos 5s'
            logger.info(msg)
            time.sleep(5)
            return get_config_general()
        else:
            logger.error(e)
        con.close()

    return False

def show_config_general():

    config_general = get_config_general()

    return '''{}:{} {}:{} {}:{} {}:{} {}:{} {}:{}'''.format(
        'compras_activadas', config_general['compras_activadas'],
        'ventas_activadas', config_general['ventas_activadas'],
        'estrategias_activadas', config_general['estrategias_activadas'],
        'max_trades', config_general['max_trades'],
        'max_losses', config_general['max_losses'],
        'descartar_ventas', config_general['descartar_ventas']
    )

def set_config(mark, data=False, new=False, conn=False):
    """
    @brief Construimos configuración de aplicación según configuración y datos de db.
    @param data Dicionario con la información que deseamos cambiar en el
           estado de la aplicación.
    """

    date = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

    dic = {}

    if new:
        eid = get_eid(mark)
        config = False
    else:
        config = get_config(mark)
        if config:
            eid = config['eid']
        else:
            eid = get_eid(mark)

    if config:
        dic['eid']                  = eid
        dic['compras_activadas']    = boolPythonSqlite(config[EJECUTAR_COMPRAS_AUTOMATICAS_HUMAN])
        dic['ventas_activadas']     = boolPythonSqlite(config[EJECUTAR_VENTAS_AUTOMATICAS_HUMAN])
        dic['sell_with_loss']       = boolPythonSqlite(config['sell_with_loss'])
        dic['min_profit_sell']      = config[MIN_PROFIT_SELL_HUMAN]
        dic['stoploss']             = config[STOPLOSS_HUMAN]
        dic['follow_losses']        = config[FOLLOW_LOSSES_HUMAN]
        dic['stoptrailing']         = config[STOPLOSSTRAINIG_HUMAN]
        dic['amount_buy']           = config[AMOUNT_BUY_HUMAN]
        dic['max_trades']           = config[MAX_TRADES_HUMAN]
        dic['max_losses']           = config[MAX_LOSSES_TRADES_HUMAN]
    else:
        dic['eid']                  = eid
        dic['compras_activadas']    = boolPythonSqlite(True)
        dic['ventas_activadas']     = boolPythonSqlite(True)
        dic['sell_with_loss']       = boolPythonSqlite(True)
        dic['min_profit_sell']      = MIN_PROFIT_SELL_DEFAULT
        dic['stoploss']             = STOPLOSS_DEFAULT
        dic['follow_losses']        = FOLLOW_LOSSES_DEFAULT
        dic['stoptrailing']         = STOPLOSSTRAINIG_DEFAULT
        dic['amount_buy']           = AMOUNT_BUY_DEFAULT
        dic['max_trades']           = MAX_TRADES_DEFAULT
        dic['max_losses']           = MAX_LOSSES_TRADES_DEFAULT

    if not eid:
        logger.debug('Cambio de configuración sin eid no guardamos {}'.format(mark))
        return dic

    if data:
        for d in data.split():
            key, value = d.split(':')
            dic[key] = value

    sql = 'INSERT INTO config (date'

    for x in dic:
        sql += ', ' + x

    sql += ') VALUES ("{}" '.format(date)

    for x in dic:
        sql += ", '{}'".format(str(dic[x]).replace('_', ' '))

    sql += ')'

    if conn:
        con = conn
    else:
        con = sqlite3.connect(DATABASE)

    try:

        cur = con.cursor()
        cur.execute(sql)
        con.commit()
        if not conn:
            con.close()

    except Exception as e:
        logger.error(e)
        if not conn:
            con.close()
        return False

    logger.debug('Cambio de configuración para {}'.format(mark))
    setMemCache('config_' + mark, dic)
    return dic

def get_config(mark, conn=False, cache=True, date=False):
    ### @param date configuración en el momento de la fecha.

    if date:
        sql = '''
        SELECT * FROM config c, estrategias e WHERE date < '{}' AND c.eid = e.id AND e.mark LIKE '{}' ORDER BY id desc LIMIT 1
        '''.format(date, mark)
    else:
        sql = '''
        SELECT * FROM config c, estrategias e WHERE c.eid = e.id AND e.mark LIKE '{}' ORDER BY id desc LIMIT 1
        '''.format(mark)

    if not conn and cache:
        config = getMemCache('config_' + mark)

        if config:
            return config

    if conn:
        con = conn
    else:
        con = sqlite3.connect(DATABASE)
    con.row_factory = sqlite3.Row

    try:

        cur = con.cursor()
        cur.execute(sql)
        rows = cur.fetchall()

        # Si no tenemos configuración de estartegia la creamos con
        # valores por defecto.
        if len(rows) == 0:
            return set_config(mark, False, True)
        else:
            for r in rows:
                data = {
                    'eid': r['eid'],
                    'compras_activadas': boolSqlitePython(r['compras_activadas']),  # pylint: disable=W0612
                    'ventas_activadas': boolSqlitePython(r['ventas_activadas']),    # pylint: disable=W0612
                    'sell_with_loss': boolSqlitePython(r['sell_with_loss']),        # pylint: disable=W0612
                    'min_profit_sell': r['min_profit_sell'],                        # pylint: disable=W0612
                    'stoploss': r['stoploss'],                                      # pylint: disable=W0612
                    'follow_losses': r['follow_losses'],                            # pylint: disable=W0612
                    'stoptrailing': r['stoptrailing'],                              # pylint: disable=W0612
                    'amount_buy': r['amount_buy'],                                  # pylint: disable=W0612
                    'max_trades': r['max_trades'],                                  # pylint: disable=W0612
                    'max_losses': r['max_losses'],                                  # pylint: disable=W0612
                }
                setMemCache('config_' + mark, data)
                return r

    except Exception as e:
        logger.error(e)
        if not conn:
            con.close()
        msg = 'Base de datos bloqueada, esperamos 1s'
        logger.info(msg)
        time.sleep(1)
        return get_config(mark)

        return False

    return False

def show_config(mark, conn=False, date=False):

    config = get_config(mark, conn, False, date)

    return '''{}:{} {}:{} {}:{} {}:{} {}:{} {}:{} {}:{} {}:{} {}:{} {}:{}'''.format(
        EJECUTAR_COMPRAS_AUTOMATICAS_HUMAN, boolPythonSqlite(config[EJECUTAR_COMPRAS_AUTOMATICAS_HUMAN]),
        EJECUTAR_VENTAS_AUTOMATICAS_HUMAN,  boolPythonSqlite(config[EJECUTAR_VENTAS_AUTOMATICAS_HUMAN]),
        'sell_with_loss',                   boolPythonSqlite(config['sell_with_loss']),
        MIN_PROFIT_SELL_HUMAN,              config[MIN_PROFIT_SELL_HUMAN],
        STOPLOSS_HUMAN,                     config[STOPLOSS_HUMAN],
        FOLLOW_LOSSES_HUMAN,                config[FOLLOW_LOSSES_HUMAN],
        STOPLOSSTRAINIG_HUMAN,              config[STOPLOSSTRAINIG_HUMAN],
        AMOUNT_BUY_HUMAN,                   config[AMOUNT_BUY_HUMAN],
        MAX_TRADES_HUMAN,                   config[MAX_TRADES_HUMAN],
        MAX_LOSSES_TRADES_HUMAN,            config[MAX_LOSSES_TRADES_HUMAN],
    )

def test_coins():

    coins = []

    con = sqlite3.connect(DATABASE)
    table = 'test_trades'
    sql = "SELECT symbol FROM {} WHERE date_sell IS NULL ORDER BY date".format(table)
    try:

        cur = con.cursor()
        cur.execute(sql)
        rows = cur.fetchall()
        return rows

    except Exception as e:
        logger.error(e)
        con.close()
        return False, False, False, False

    return coins

def delete_old_rows():

    time = "-60 days"
    date = datetime.now().strftime("%Y-%m-%d")
    backup_db = 'backup/' + date + '-' + os.path.basename(DATABASE)

    # Generamos backup.
    try:
        os.mkdir('backup/')
    except OSError:
        logger.debug('Directorio backup ya creado')

    try:
        shutil.copyfile(DATABASE, backup_db)
    except OSError:
        logger.error('No se ha podido realizar backup a [{}], salimos'.format(backup_db))
        exit(1)

    sql_market = "DELETE FROM market WHERE date < datetime('now', '{}') AND id > 1".format(time)
    sql_trades = "DELETE FROM trades WHERE date_sell < datetime('now', 'start of day','{}') AND id > 1".format(time)
    sql_trades_test = "DELETE FROM test_trades WHERE date_sell < datetime('now', 'start of day','{}') AND id > 1".format(time)
    sql_config = "DELETE FROM config WHERE date < datetime('now', 'start of day','{}') AND id > 1".format(time)
    sql_config_general = "DELETE FROM config_general WHERE date < datetime('now', 'start of day','{}') AND id > 1".format(time)
    sql_exec(sql_market)
    sql_exec(sql_trades)
    sql_exec(sql_trades_test)
    sql_exec(sql_config)
    sql_exec(sql_config_general)
    print("Borrado de registro exitoso")

    reindex_db('market')
    reindex_db('trades')
    reindex_db('test_trades')
    reindex_db('config')
    reindex_db('config_general')

def reindex_db(table):
    # Reindexado.

    sql_rows = 'SELECT * FROM {}'.format(table)
    rows = sql_exec(sql_rows)
    count = 0
    new_rows = []
    for row in rows:
        count += 1
        new_rows.append((row[1:]))

    if len(new_rows) < 1:
        return

    values_str = ('?,' * len(new_rows[0])).rstrip(',')
    sql_exec("DELETE FROM {}".format(table))
    sql_exec("REINDEX {}".format(table))
    sql_exec("INSERT INTO {} VALUES (null,{})".format(table, values_str), False, new_rows)
    sql_exec("VACUUM")

def get_parser():
    """
    Creates a new argument parser.
    """
    parser = argparse.ArgumentParser('db')
    version = '%(prog)s ' + __version__
    parser.add_argument('--version', '-v', action='version', version=version)
    parser.add_argument('--coin', '-c', help='Moneda o monedas sobre la que realizar la acción')
    parser.add_argument('--trade', '-t', help='Identificador de operación sobre la que realizar la acción')
    parser.add_argument('--test', '-tt', help='Ejecutamos la acción sobre la tabla de test.', action='store_true')
    parser.add_argument('--insert_buy', '-ib', help='Insertar compra en base de datos', action='store_true')
    parser.add_argument('--insert_sell', '-is', help='Insertar venta en base de datos', action='store_true')
    parser.add_argument('--update_trade', '-ut', help='Modificar operación en base de datos, ejemplo: db.py -t 121 -ut hide=1\nLista de posibles campos:\n{}'.format(list_fields('trades')), nargs='*')
    parser.add_argument('--amount', '-a', help='Cantidad comprada', type=float)
    parser.add_argument('--precio', '-p', help='Precio', type=float)
    parser.add_argument('--status', '-st', help='Status, ejemplo: ["BUY C"]')
    parser.add_argument('--type', '-tp', help='Tipo de trade, (SPOT, LONG o SHORT), default: SPOT', default="SPOT")
    parser.add_argument('--leverage', '-lv', help='En caso de trabajar con futuros definir el apalancamiento, default: 1', default=1)
    parser.add_argument('--strategy', '-stg', help='Definir, modificar estrategia, ejemplo: Modificar estrategia en operación abierta de BTC, db -stg C -c BTC')
    parser.add_argument('--monitor', '-m', help='Datos del monitor "price tend volum adx macd", ejemplo "655555 455555 212221 400004 440940"')
    parser.add_argument('--report_date', '-rd', help='Especificar fecha inicio de informe, por defecto "-24 hours", ejemplo: "-7 days"o "-1 months"', default='-24 hours')
    parser.add_argument('--lock', '-lk', help='Bloquear/Desbloquear trade. Ejemplo "db -lk 1" o "db -lk 0"', default=0)
    parser.add_argument('--hide', '-hd', help='Ocultar/Mostrar trade. Ejemplo "db -hd 1" o "db -hd 0"', default=0)
    parser.add_argument('--change_status', '-ct', help='Change status trade. Ejemplo. -ct "COIN BUY L"')
    parser.add_argument('--change_strategy', '-cs', help='Change strategu trade. Ejemplo. -cs "COIN L"')
    parser.add_argument('--change_stoploss_trade', '-cst', help='Change stoploss trade. Ejemplo. -cst "BTC -5"')
    parser.add_argument('--change_stoptraining_trade', '-cstr', help='Change stoptraining trade. Ejemplo. -cst "BTC 2;2,4;4,7;1"')
    parser.add_argument('--config', '-cf', help='Modificar configuración, ejemplo: ["C amount_buy:100 stoploss:1"]')
    parser.add_argument('--show_config', '-scf', help='Mostrar configuración de estrategia [marca]')
    parser.add_argument('--delete_trades', '-dts', help='Borrar trades de la base de datos: ["id id"], [all], [open] Abiertas o [close] Cerradas')
    parser.add_argument('--delete_trades_test', '-dtst', help='Borrar trades de test: ["id id"], [all], [open] Abiertas o [close] Cerradas')
    parser.add_argument('--config_general', '-cfg', help='Modificar configuración general, ejemplo: [""]')
    parser.add_argument('--show_config_general', '-scfg', help='Mostrar configuración general.', action='store_true')
    parser.add_argument('--delete_old_rows', '-dor', help='Borrar registros viejos. -30 days', action='store_true')
    parser.add_argument('--export_sql', '-eps', help='Exportar a sql', action='store_true')
    parser.add_argument('--export_schema', '-epsc', help='Exportar estructura de la base de datos', action='store_true')
    parser.add_argument('--import_sql', '-ips', help='Importar de sql', action='store_true')
    parser.add_argument('--list_fields_tables', '-lft', help='Listar de campos de tabla o vista, [tables] muestra la lista de tablas', default=False)

    return parser

def main(args=None):
    """
    Main entry point for your project.
    Args:
        args : list
            A of arguments as if they were input in the command line. Leave it
            None to use sys.argv.
    """

    parser = get_parser()
    args = parser.parse_args(args)

    # print('lock: ', args.lock)
    # print('hide: ', args.hide)
    # print('insert_buy: ', args.insert_buy)
    # print('insert_sell: ', args.insert_sell)
    # print('update_trade: ', args.update_trade)
    # print('monitor: ', args.monitor)
    # print('test: ', args.test)

    if args.list_fields_tables:
        print('lt: ' + args.list_fields_tables)
        if args.list_fields_tables == 'tables':
            print(list_tables())
        else:
            print(list_fields(args.list_fields_tables))
        exit()

    if args.export_schema:
        print("@todo exportar\n# sqlite3 data.db .schema > data-schema.sql ")

    if args.export_sql:
        print("@todo exportar\n# sqlite3 data.db .dump > data-dump.sql ")

    if args.import_sql:
        print("@todo importar\n# sqlite3 data.db < data-dump.sql")

    # if args.lock:
    # db_unlock(args.lock)

    if args.insert_buy:
        coin = args.coin.replace('/' + CURRENCY, '') + '/' + CURRENCY
        db_insert_buy(coin, args.amount, args.precio, args.type, args.leverage, args.status, args.monitor, '0', 'Manual', args.test, args.lock, args.hide)

    if args.insert_sell:
        db_insert_sell(args.insert_sell, args.precio, args.status, args.minitor, 'Manual', args.test, args.lock, args.hide)

    if args.update_trade:
        db_update_trade(args, args.update_trade)

    if args.delete_trades_test:
        db_delete_trades(args.delete_trades_test, True)

    if args.delete_trades:
        db_delete_trades(args.delete_trades)

    if args.delete_old_rows:
        delete_old_rows()
        exit()

    if args.show_config_general:
        s = show_config_general()
        print(s)

    if args.config_general:
        set_config_general(args.config_general)

    if args.show_config:
        s = show_config(args.show_config)
        print(s)

    if args.config:
        mark = args.config.split()[0]
        new_config = " ".join(args.config.split()[1:])
        set_config(mark, new_config)

    if args.change_stoploss_trade:
        coin = args.change_stoploss_trade.split()[:1][0]
        stoploss = args.change_stoploss_trade.split()[1:][0]
        db_change_stoploss_trade(coin, stoploss)

    if args.change_stoptraining_trade:
        coin = args.change_stoptraining_trade.split()[:1][0]
        stoptraining = args.change_stoptraining_trade.split()[1:][0]
        db_change_stoptraining_trade(coin, stoptraining)

    if args.change_strategy:
        coin = args.change_strategy.split()[:1][0]
        mark = args.change_strategy.split()[1:]
        mark = " ".join(mark)
        db_change_strategy_trade(coin, mark)

    if args.change_status:
        coin = args.change_status.split()[:1][0]
        status = args.change_status.split()[1:]
        status = " ".join(status)
        db_change_status_trade(coin, status)


if __name__ == '__main__':
    main()
