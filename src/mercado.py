#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## @file mercado.py
## @brief Mostrar tendencia del precio.
##
## sudo pip3 install gi

import os
import sys
import ccxt.async_support as ccxt  # noqa: E402
import argparse
import time
import argparse
import urllib.request, json
import asyncio
import nest_asyncio
nest_asyncio.apply()


sys.path.append('./src')

from base import *
from console import *
# from tendencia import *
import tendencia
import futures
from balances import *
from db import *
from reports import *
from create_order import *
from telegram_send_message import *
from signal import signal, SIGINT
from pprint import pprint
from datetime import datetime, timedelta

FORMATO_SALIDA  = False

# Notificaciones de escritorio.
if NOTIFY_DESKTOP:
    import gi
    gi.require_version('Notify', '0.7')
    from gi.repository import Notify
    Notify.init("EmCcxt")

__version__ = '0.1.0'
__author__ = u'edu@lesolivex.com'

datos_pantalla = {'BTC': ''}
msg = []

# symbols = ['BTC/USDT', 'ETH/USDT']
# symbols = []

if TESTNET:
    SEARCH_MARKET_SYMBOLS = False

symbols = []
symbols_descartados = os.getenv('binance_symbols_descartados').split()

# Avisos trade parado.
aviso_trade_parado = False

# Tiempo para insertar información mercado.
TIME_INSERT_MARKET = (1 * 60)  # 5m = 5 * 60
time_last_insert_market = time.time() - TIME_INSERT_MARKET
INSERT_MARKET = True

VALORACION_MERCADO_GLOBAL = {}

def market_symbols():

    global symbols, symbols_descartados, SEARCH_MARKET_SYMBOLS

    if TESTNET:
        SEARCH_MARKET_SYMBOLS = False
        symbols = os.getenv('testnet_symbols').split()
    else:
        symbols = os.getenv('binance_symbols').split()

    url_list_coins = 'https://api.coingecko.com/api/v3/exchanges/binance/tickers?coin_ids=usdt&page=1'

    # Recogemos simbolos de operaciones en test para no dejarlas huérfanas.
    coins_test = test_coins()
    if coins_test:
        for c in coins_test:
            if c and c[0] and not c[0] in symbols:
                symbols.append(c[0])

    # En caso de tenerlas en trades las quitamos de la lista.
    for s in symbols.copy():
        if is_buy(s):
            symbols.remove(s)

    if SEARCH_MARKET_SYMBOLS and len(symbols) < TOTAL_SYMBOLS:
        try:

            with urllib.request.urlopen(url_list_coins) as url:
                data = json.loads(url.read().decode())
                for d in data['tickers']:
                    if len(symbols) >= TOTAL_SYMBOLS:
                        return
                    s = d['base'].upper() + '/' + CURRENCY
                    if s not in symbols and 'USD' not in d['base'] and s not in symbols_descartados and not is_buy(s):
                        logger.debug("Añadimos moneda de [" + url_list_coins + "] " + s)
                        symbols.append(s)

        except Exception as e:

            msg = 'Error recogiendo listado de monedas del mercado'
            logger.error(msg)
            print(e)
            print(msg)
            return

    # if len(symbols) > TOTAL_SYMBOLS:
    #     symbols = symbols[:TOTAL_SYMBOLS]


async def pantalla(symbol, mostrar_status_market=False):

    config_general = get_config_general()
    strategies = config_general['estrategias_activadas']

    os.system('clear')

    estrategias = len(strategies.split())
    rows, columns = os.popen('stty size', 'r').read().split()
    rows = int(rows)
    columns = int(columns)
    cols_status = 8 * estrategias
    cols_monitor = 51 + cols_status
    cols_colors = 306 #  + (24 * estrategias)
    rows_monitor = len(symbols)
    cols_pantalla = 1
    mon_pantalla = columns / cols_monitor
    # print(estrategias)
    # print(cols_status)
    # print(cols_monitor)
    # print(cols_colors)

    # No tenemos filas suficientes.
    if mon_pantalla > 2:
        cols_pantalla = 2
    if mon_pantalla > 3:
        cols_pantalla = 3
    if mon_pantalla > 4:
        cols_pantalla = 4

    counter_col = 1
    end = "\n"
    while counter_col <= cols_pantalla:
        titles = "{:<5} Precio Tenden Volume ADX    MACD   LastPrice Status".format('Coin ')
        print("{:<{cols}}".format(titles, cols=cols_monitor), end='')
        counter_col += 1

    print("")

    counter_col = 1
    for s in symbols:
        if s in datos_pantalla:
            contenido = "{:<5} {:<{cols}}".format(s.replace('/' + CURRENCY, ''), datos_pantalla[s], cols=cols_colors)
            if s == symbol:
                contenido = bold(contenido)
        else:
            continue
            # contenido = "{:>10} {:<39}".format(s, "...")
        print("{:<{cols}} ".format(contenido, cols=cols_monitor), end='')
        if counter_col == cols_pantalla:
            print("")

        counter_col += 1
        if counter_col > cols_pantalla:
            counter_col = 1

    await pantalla_barramercado(mostrar_status_market, cols_pantalla)

async def pantalla_barramercado(mostrar_status_market=False, columns=1):

    current_col = 1

    barra_informativa = ''

    config_general = get_config_general()
    strategies = config_general['estrategias_activadas']

    total_trades, total_losses, buy_symbols, last_two, total_stoploss_positive = await report_status()
    total_trades_test, total_losses_test, buy_symbols_test, last_two_test, total_stoploss_positive_test = await report_status(False, True)

    if config_general['compras_activadas']:
        compras_activadas_general_str = green('T')
    else:
        compras_activadas_general_str = red('F')

    if config_general['ventas_activadas']:
        ventas_activadas_general_str = green('T')
    else:
        ventas_activadas_general_str = red('F')

    if SIMULA_COMPRAS_VENTAS:
        simulate_compra_venta = bold(green("[SIMULA]"))
        simulate_compra_venta_barra = bold(green("[SIMULA]"))
    else:
        simulate_compra_venta = bold(orange("[REAL]"))
        simulate_compra_venta_barra = ''

    if current_col == columns:
        separacion = "\n"
    else:
        separacion = " | "

    barra_informativa += (
        "\n\nCoins[{}] MaxTrades[{}] MaxLosses[{}] BuyG[{}] SellG[{}] {}{}Trades[{}] Losses[{}] TradesT[{}] LossesT[{}]\n".format(
            len(symbols),
            config_general['max_trades'],
            config_general['max_losses'],
            compras_activadas_general_str,
            ventas_activadas_general_str,
            simulate_compra_venta_barra,
            separacion,
            total_trades,
            color_result(total_losses, False, True),
            total_trades_test,
            color_result(total_losses_test, False, True),
        ))

    color = 1
    current_col = 1
    for mark in strategies.split():

        strategy_id = get_eid(mark)
        if not strategy_id:
            logger.error('ERROR: Estrategia [{}] no existe, se debe borra de estartegias activadas en configuración general.'.format(mark))
            continue

        strategy = get_strategy(strategy_id)
        config = get_config(mark)

        total_trades, total_losses_strategy, buy_symbols, last_two, total_stoploss_positive = await report_status(False, False, False, mark)
        total_trades_test, total_losses_strategy_test, buy_symbols_test, last_two_test, total_stoploss_positive_test = await report_status(False, True, False, mark)

        sst = status_strategy_in_test(mark)

        if sst > 2:
            sst_str = green(round(sst, 1))
        elif sst > 1:
            sst_str= blue(round(sst, 1))
        elif sst > 0:
            sst_str= bold(round(sst, 1))
        else:
            sst_str= red(round(sst, 1))

        # LastTwo
        if last_two_test > 1:
            last_two_test_str = red(last_two_test)
        elif last_two_test > 0:
            last_two_test_str = orange(last_two_test)
        else:
            last_two_test_str = green('0')


        if config['compras_activadas']:
            salida_buy = 'T'
        else:
            salida_buy = 'F'

        if salida_buy == "T":
            salida_buy = green(salida_buy)
        else:
            salida_buy = red(salida_buy)

        if current_col == columns:
            separacion = "\n"
            current_col = 0
        else:
            separacion = " | "

        barra_informativa += (
            "[{}] Amt{} BUY[{}] ".format(
                colores(color, mark),
                "{:<6}".format("[" + str(round(config['amount_buy'])) + "]"),
                salida_buy,
                )
        )

        barra_informativa += (
            "Trades[{:>2}] Losses[{:>2}] SST{:<15} LT[{}] T{:<}{}".format(
                colores(color, total_trades),
                color_result(total_losses_strategy, False, True),
                "[{}]".format(sst_str),
                last_two_test_str,
                "{}".format("[" + strategy['temporalidad'] + "]"),
                separacion
            ))

        current_col += 1
        color += 1

    if not SIMULA_COMPRAS_VENTAS:

        availableBalance, positions = await futures.futures_open_orders()
        balance_usdt = await currency_balance()

        if positions:
            num_positions = len(positions)
        else:
            num_positions = 'F'

        ## Info exchange.
        barra_informativa += "\n{} Spot: {}$ Fut: {}$ NPF: {}".format(
            simulate_compra_venta,
            round(float(balance_usdt), 2),
            round(float(availableBalance), 2),
            num_positions
        )

    print(barra_informativa)

    # Mostramos configuración general.
    # print(show_config_general())

def end(signal_received, frame):
    exit(0)

async def mercado(args):

    global symbols, msg, symbols
    global aviso_trade_parado, time_last_insert_market, INSERT_MARKET

    # Guardar lock de la app.
    db_locks('mercado')

    # Vigilar trade
    if not get_lock('trades'):
        logger.info('Trade parado')
        if not aviso_trade_parado or datetime.now() > aviso_trade_parado:
            tlg_send('**trades parada**\n\nSi solo tenemos mercado en funcionamiento y trade parado no se ejecutarán las ventas')
            aviso_trade_parado = datetime.now() + timedelta(hours = 4)

    config_general = get_config_general()
    strategies = config_general['estrategias_activadas']

    signal(SIGINT, end)
    market_status = {}

    market_symbols()

    for s in symbols:

        coin = s.replace('/' + CURRENCY,'')

        status_marks, monitor_color, monitor, precio = await tendencia.tendencia(s, strategies)

        status_mark = False

        if status_marks:

            for sm in status_marks:

                # Saber si hemos realizado compra en test para guardar market.
                buy_in_test = False

                if status_marks[sm]:
                    status_mark = status_marks[sm]
                    status = status_mark.split()[0]
                    if len(status_mark.split()) > 1:
                        mark = status_mark.split()[1]
                    else:
                        mark = False
                    prices, tendencias, volumen, adx, macd = monitor.split()
                else:
                    status = False

                if status:

                    config = get_config(mark)
                    strategy = get_strategy(get_eid(mark))

                    # Información de la compra.
                    try:
                        buys = db_update_open_trade(s, precio, False, strategy['id'])
                    except Exception as e:
                        buys = False
                        print(e)
                        logger.error(e)
                        # raise IOError(e)

                    try:
                        buys_test = db_update_open_trade(s, precio, True, strategy['id'])
                    except Exception as e:
                        buys_test = False
                        logger.error(e)
                        # raise IOError(e)

                    # Filtros.
                    descartar = False
                    if args.filtro and s != 'BTC/' + CURRENCY:
                        filtros = args.filtro.split()
                        for f in filtros:
                            if f == 'status':
                                if status == 'CLOSE+' or status == 'CLOSE-':
                                    descartar = True
                            if f == '1h':
                                # Solo mostramos monedas con tendencia alcista en 1h.
                                una_hora = int(str(macd)[-4:])
                                if una_hora < 7000:
                                    descartar = True
                            if f == '4h':
                                # Solo mostramos monedas con tendencia alcista en 1h.
                                cuatro_horas = int(str(macd)[-5:])
                                if cuatro_horas < 70000:
                                    descartar = True
                            if f == 'tend':
                                # Solo mostramos monedas con tendencia alcista en d,4h.
                                if tendencias < 500000:
                                    descartar = True
                            if f == 'tend4h':
                                # Solo mostramos monedas con tendencia alcista en d,4h.
                                if tendencias < 554990:
                                    descartar = True
                            if f == 'tend1h':
                                # Solo mostramos monedas con tendencia alcista en d,4h.
                                if tendencias < 554990:
                                    descartar = True

                        if descartar:
                            msg.append("[R]{}, {}".format(s, monitor_color))
                            logger.info("[R]{}, {}".format(s, monitor_color))
                            symbols.remove(s)
                            symbols_descartados.append(s)
                            await pantalla(s)
                            break

                    # print(monitor)
                    datos_pantalla[s] = monitor_color

                    if status == 'OPEN':

                        # Test
                        is_buy_in_test = is_buy(s, True, strategy['id'])
                        logger.debug('is_buy_in_test: {}'.format(str(is_buy_in_test)))
                        if not is_buy_in_test:

                            # Añadimos compra a test.
                            logger.debug('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n[TestCompra] Compramos [' + s + ']')
                            buy_in_test = db_insert_buy(s, (100 / precio), precio, strategy['type'], strategy['leverage'], status_mark, monitor, False, 'Bot test', True, False, False, "{'info': 'Test'}")

                        if not is_buy(s, False, strategy['id']):

                            if VALORACION_MERCADO_GLOBAL:
                                result_strategy, media_market, monitor_market, result_strategy_color = VALORACION_MERCADO_GLOBAL[strategy['temporalidad']]
                            else:
                                result_strategy, media_market, monitor_market, result_strategy_color = [False, False, False, False]

                            if ( config_general['compras_activadas'] and config['compras_activadas'] == 1 ) or SIMULA_COMPRAS_VENTAS:

                                compramos = await tendencia.valorar_compra(coin, mark)

                                if compramos:
                                    if not buys:
                                        logger.info('[Compra] para [' + s + ']')
                                        class Args:
                                            side = 'buy'
                                        argsBuy = Args()
                                        argsBuy.type = strategy['type']
                                        argsBuy.leverage = strategy['leverage']
                                        argsBuy.open = s.replace('/' + CURRENCY,'')
                                        argsBuy.amount_dolars = config['amount_buy']
                                        await ejecutar_compra(argsBuy, 'Bot', status_mark, monitor)
                                    else:
                                        logger.info('[{}] Señal de compra para [{}], pero ya tenemos comprada'.format(strategy['mark'], s))

                    # Comprobar compra y recoger datos en test.
                    if buys_test:

                        for buy_test in buys_test:

                            buy_test_profit, buy_test_profit_str = getProfit(buy_test['type'], precio, buy_test['price'])
                            buy_strategy = get_strategy(buy_test['eid'])
                            if buy_strategy:
                                buy_mark = buy_strategy['mark']
                                if mark == buy_mark:
                                    logger.debug('[Test][CLOSE][{}] [{}/{}] Prof: [{:.2f}] PriBuy: {:.5f}, PriSell: {:.5f}'.format(
                                        s,
                                        mark,
                                        buy_mark,
                                        buy_test_profit,
                                        buy_test['price'],
                                        precio)
                                    )

                                    info = tendencia.valorar_venta(s.replace('/' + CURRENCY,''), buy_test_profit, status, buy_test['stoploss'], buy_mark, False, True)

                                    if info:
                                        # Insertamos venta en test.
                                        id_trade, profit_sell, result_sell = db_insert_sell(buy_test['id'], precio, status_mark, monitor, info, True)
                                        if profit_sell:
                                            logger.debug('[test][{}] Venta, profit: {}'.format(s, profit_sell))
                                        else:
                                            logger.debug('[test][{}] Error con venta.'.format(s))

                            else:
                                logger.error('[Test][{}][{}] No se pudo recoger estrategia de trade'.format(s, buy_test['eid'], buy_test['id']))


                    # Añadimos registro en market cada x minutos.
                    # y solo con la primera estrategia.
                    # if (INSERT_MARKET or status == 'OPEN') and mark == strategies[0]:
                    if INSERT_MARKET or buy_in_test or (status and (status == 'OPEN' and not buys)):
                        db_insert_market(s, precio, status_mark, monitor)
                        logger.debug('[{}] Insertamos información del mercado'.format(s))
                    # else:
                    #     logger.debug('[{}] No Insertamos información del mercado:\nINSERT_MARKET: {} TIME_INSERT_MARKET: {}s time_last_insert_market: {}s now-tlim: {}s'.format(
                    #         s,
                    #         INSERT_MARKET,
                    #         TIME_INSERT_MARKET,
                    #         round(time_last_insert_market),
                    #         round(time.time() - time_last_insert_market)
                    #     ))


                else:
                    msg = "[{}] Sin status St[{}] mon[{}]".format(
                        s,
                        status_mark,
                        monitor
                    )
                    logger.info(msg)
                    print(msg)

        else:
            msg = "[{}] Sin tendencia, posibles problemas en red, St[{}] mon[{}]".format(
                s,
                status_mark,
                monitor
            )
            logger.info(msg)
            print(msg)

        await pantalla(s, True)

    # Fin del bucle.
    msg = []

    # Contador de tiempo para insertar información de mercado.
    now = time.time()
    if INSERT_MARKET == True:
        INSERT_MARKET = False
        time_last_insert_market = time.time()
    elif (now - time_last_insert_market) > TIME_INSERT_MARKET:
        INSERT_MARKET = True

async def ejecutar_compra(args, info='Manual', status=False, monitor=False, confirmar=False):

    global VALORACION_MERCADO_GLOBAL

    if confirmar and FORMATO_SALIDA == 'telegram':
        confirmar = False

    temporalidad = False
    leverage = False
    type = False
    mark = False

    if hasattr(args, 'strategy') and args.strategy:
        mark = args.strategy
    elif status:
        mark = status.split()[1]
    else:
        mark = 'M'

    strategy = get_strategy(get_eid(mark))

    if args.type:
        type = args.type
    else:
        type = strategy['type']

    if args.leverage:
        leverage = args.leverage
    else:
        leverage = strategy['leverage']

    temporalidad = strategy['temporalidad']

    for coin in args.open.split():

        symbol = coin + '/' + CURRENCY

        # Si no tenemos información de la tendencia actual la generamos.
        if not monitor:
            status_marks, monitor_color, monitor, precio = await tendencia.tendencia(symbol)
            if not status and mark in status_marks:
                status = status_marks[mark]

        if not status:
            status = 'Man ' + mark

        VALORACION_MERCADO_GLOBAL = tendencia.valoracion_mercado()
        if VALORACION_MERCADO_GLOBAL:
            result_strategy, media_market, monitor_market, result_strategy_color = VALORACION_MERCADO_GLOBAL[temporalidad]
        else:
            monitor = False
            monitor_market = 'UnkNown'

        class Args:
            side = 'buy' # Hold

        argsBuy = Args()

        argsBuy.symbol = symbol
        argsBuy.order = coin # Futures
        argsBuy.leverage = leverage
        argsBuy.amount = False

        if args.amount_dolars and float(args.amount_dolars) > 0:
            argsBuy.amount_dolars = args.amount_dolars
            amountStr =  str(args.amount_dolars) + '$'
        else:
            if args.amount and float(args.amount) > 0:
                argsBuy.amount = float(args.amount)
                amountStr = args.amount
            else:
                print('Error: Es necesario pasar el monto en la moneda a comprar o en ' + CURRENCY)
                await exchange.close()
                exit(1)


        if confirmar:
            print('Abrir operación con [{}]'.format(coin))
            print('Info: {}'.format(info))
            print('Strategy: {}'.format(mark))
            print('Leverage: {}'.format(argsBuy.leverage))
            print('Temporalidad: {}'.format(temporalidad))
            print('Type: {}'.format(type))
            print('Amount: {}'.format(argsBuy.amount))
            print('Amount in {}: {}'.format(CURRENCY, argsBuy.amount_dolars))
            print('Status: {}'.format(status))
            print('monitor: {}'.format(monitor))
            print('monitor_market: {}'.format(monitor_market))
            opcion = input('Continuar (s/n): ')
            if opcion != 's':
                return


        exec_order = False
        try:
            if type == 'SPOT':

                exec_order = await create_order(argsBuy)

            else:
                if type == 'LONG':
                    argsBuy.side = 'BUY'
                elif type == 'SHORT':
                    argsBuy.side = 'SELL'
                else:
                    msg = 'Tipo operación [' + type + '] desconocido'
                    logger.error(msg)
                    eventos('ERROR_TIPO_OPERACION', msg, 60)
                    return False

                exec_order = await futures.create_order_future(argsBuy, True, False)

        except Exception as e:

            print(e)
            msg = 'Error with buy' + "\n" + symbol + ' Amount: ' + amountStr + "\n" + str(e)
            eventos('ERROR_IN_BUY', msg)
            await exchange.close()
            # return False
            continue


        if exec_order and exec_order[0]:

            logger.info(exec_order)
            orderId, symbol, amount, price, cost, datatime, status_order, side, type_order, fee, filled  = exec_order[1]
            data = exec_order[2]


            if mark == 'M':
                db_insert_buy(symbol, amount, price, type, leverage, 'MAN M', monitor, monitor_market, info, False, False, False, data)
            else:
                db_insert_buy(symbol, amount, price, type, leverage, status, monitor, monitor_market, info, False, False, False, data)

            msg = "OPEN: {} {} {} {}[{}]".format(symbol.replace('/' + CURRENCY,''), amountStr, mark, type, leverage)
            eventos('OPEN', msg)

        else:

            msg = 'Error in buy [{}] [{}] [{}]'.format(symbol, amountStr, status)
            eventos('ERROR_IN_BUY', msg)

    # await exchange.close()

async def ejecutar_venta(sellArgs, info='Manual', status=False, monitor=False, confirmar=False):
    """
    @return Profit de la última moneda vendida.
    """

    type = sellArgs.type

    if confirmar and FORMATO_SALIDA == 'telegram':
        confirmar = False

    config_general = get_config_general()
    strategies = config_general['estrategias_activadas']

    profit_sell = False

    # Primero definimos las operaciones a realizar.
    operaciones_cerrar = []
    operaciones_canceladas = []

    for symbol in str(sellArgs.close).split():

        if str(symbol).isnumeric():
            operaciones_cerrar.append(symbol)
        else:
            symbol = symbol + '/' + CURRENCY
            trades = db_open_trades(symbol, False, True)
            if len(trades) > 0:
                for trade in trades:
                    operaciones_cerrar.append(trade['id'])
            else:
                if confirmar:
                    print('Sin operaciones abiertas para [{}]'.format(symbol))

    if len(operaciones_cerrar) == 0:
        msg = 'No se ha encontrado operación con [{}]'.format(sellArgs.close)
        print(msg)
        return

    if confirmar:
        ### Presentamos antes de ejecutar.

        for trade_id in operaciones_cerrar:
            sql = "SELECT * FROM trade_result WHERE id = {} AND data_sell IS NULL".format(trade_id)
            trade = sql_exec(sql, False, False, True)
            if not trade:
                print('No hay operación cerrado con el id [{}]'.format(trade_id))
                operaciones_canceladas.append(trade_id)
                continue
            trade = trade[0]
            asset_price = await fetch_ticker(exchange, trade['symbol'])
            # Comprobar asset_price.
            if not asset_price:
                msg = "[{}][{}] Error con fetch_ticker".format(symbol, exchange.name)
                logger.error(msg)
                print(msg)
                continue
            asset_price = float(asset_price['close'])
            profit, profit_str = getProfit(trade['type'], float(asset_price), float(trade['price']))
            print("- {:<9} {:<5} {:<6} Amount:{:>8}$ Precio compra:{:<10} Precio actual: {:<10} Profit:{:<10}".format(
                trade['id'],
                trade['symbol'],
                trade['type'],
                round((trade['amount'] * asset_price), 2),
                trade['price'],
                asset_price,
                color_result(round(profit,3))
                )
            )

        await exchange.close()

        if confirmar:
            confirmar_manual = input("\nConfirmar venta (s/n): ")
            if confirmar_manual != 's':
                print("\nCancelamos\n")
                return False

    logger.info('[{}] operaciones a cerrar: [{}]'.format(sellArgs.close, operaciones_cerrar))
    profit_total = 0
    for trade_id in operaciones_cerrar:

        if trade_id in operaciones_canceladas:
            print('Operación [{}] cancelada, seguimos'.format(trade_id))
            continue

        sql = "SELECT * FROM trade_result WHERE id = {}".format(trade_id)
        trade = sql_exec(sql, False, False, True)
        trade = trade[0]
        asset_price = await fetch_ticker(exchange, trade['symbol'])
        await exchange.close()
        # Comprobar asset_price.
        if not asset_price:
            msg = "[{}][{}] Error con fetch_ticker".format(symbol, exchange.name)
            logger.error(msg)
            print(msg)
            continue
        asset_price = float(asset_price['close'])

        class Args:
            side = 'sell'

        args = Args()

        args.symbol = trade['symbol']
        args.leverage = trade['leverage']
        args.amount = trade['amount']
        args.amount_dolars = (trade['amount'] * asset_price)

        # Si pasamos una cantidad concreta la recogemos.
        if hasattr(sellArgs, 'amount') and sellArgs.amount:
            args.amount = sellArgs.amount

        if hasattr(sellArgs, 'amount_dolars') and sellArgs.amount_dolars:
            args.amount_dolars = sellArgs.amount_dolars

        if trade['type'] == 'SPOT':
            exec_order = await create_order(args)
        else:
            args.order = trade['symbol'].replace('/' + CURRENCY, '')
            if trade['type'] == 'LONG':
                args.side = 'BUY'
            elif trade['type'] == 'SHORT':
                args.side = 'SELL'
            else:
                logger.error('Tipo operación [' + trade['type'] + '] desconocido')
                return False
            if SIMULA_COMPRAS_VENTAS:
                exec_order = await futures.create_order_future(args, False, False)
            else:
                exec_order = await futures.close_position(trade['symbol'].replace('/' + CURRENCY,''), info, confirmar, trade['leverage'])

        try:

            if exec_order and exec_order[0]:

                logger.info(exec_order)
                orderId, symbol, amount, price, cost, datatime, status_order, side, type_order, fee, filled  = exec_order[1]
                pprint(exec_order)
                data_sell = exec_order[2]

                # Si no tenemos información de la tendencia actual la generamos.
                if not monitor or not status:
                    if hasattr(sellArgs, 'tipo_sesion') and sellArgs.tipo_sesion:
                        tipo_sesion = sellArgs.tipo_sesion
                    else:
                        tipo_sesion = strategies.split()[0]
                    status_marks, monitor_color, monitor, precio = await tendencia.tendencia(symbol, tipo_sesion)

                sell_db =  db_insert_sell(trade['id'], price, status_marks[tipo_sesion], monitor, info, False, data_sell, fee)

                if not sell_db:

                    msg = 'ERROR guardando venta en base de datos: [{}] Id:{}'.format(symbol, trade['id'])
                    print(msg)
                    eventos('ERROR_IN_CLOSE', msg)

                else:

                    id_trade, profit_sell, result_sell = sell_db
                    profit_total += profit_sell
                    print('Profit: ' + str(profit_sell) + '%' + ' Result: ' + str(result_sell) + '$')
                    msg = 'Close: {} {} Result: {}$ Type: {}[{}]'.format(symbol.replace('/' + CURRENCY,''), trade['mark'], str(result_sell), trade['type'], trade['leverage'])
                    eventos('CLOSE', msg)

            else:

                # Cerrar operación en flaso.
                id_trade, profit_sell, result_sell = db_insert_sell(trade['id'], asset_price, 'FALSE', False, 'unknown', False, None, 0)

                msg = 'Cerramos operación en falso: [{}] Id:{} Profit:{} Result:{}'.format(symbol, id_trade, profit_sell, result_sell)
                print(msg)
                eventos('ERROR_IN_CLOSE', msg)

        except Exception as e:

            raise IOError(e)
            msg="ERROR al ejecutar venta: {}\nexec_order: {}".format(e, str(exec_order))
            print(msg)
            eventos('ERROR_IN_CLOSE', msg)

    return profit_total

def get_parser():
    """
    Creates a new argument parser.
    """
    parser = argparse.ArgumentParser('mercado')
    version = '%(prog)s ' + __version__
    parser.add_argument('--type', '-tp', help='Tipo de trade, (SPOT, LONG o SHORT)', default=False)
    parser.add_argument('--strategy', '-sy', help='Estrategia, en caso de Manual especificar el tipo, ejemplo -tp LONG -sy M', default=False)
    parser.add_argument('--leverage', '-lv', help='En caso de trabajar con futuros definir el apalancamiento', default=False)
    parser.add_argument('--version', '-v',
                        action='version',
                        version=version
                        )
    parser.add_argument('--symbol',
                        '-s',
                        help='Simbolo, ejemplo ETH/USDT',
                        required=False
                        )
    parser.add_argument('--list',
                        '-l',
                        help='Lista de simbolos, ejemplo: "ETH/USDT BTC/USDT"',
                        required=False
                        )
    parser.add_argument('--refresh',
                        '-r',
                        help='Tiempo de refresco en segundos, por defecto 30',
                        required=False,
                        default=1
                        )
    parser.add_argument('--nosearchsymbols', '-ns',
                        help='No ir a buscar symbolos del mercado',
                        action="store_true",
                        required=False
                        )
    parser.add_argument('--close',
                        '-cl',
                        help='Cerramos operación, podemos indicar monedas o identificadores, ejemplo: --close [BTC|123]',
                        required=False
                        )
    parser.add_argument('--open',
                        '-op',
                        help='Abrimos operación, ejemplo: --open "BTC ETH" --type SHORT -lv 5 -ad 100',
                        required=False
                        )
    parser.add_argument('--amount_dolars',
                        '-ad',
                        help='Cantidad a comprar en dolares.',
                        required=False
                        )
    parser.add_argument('--amount',
                        '-a',
                        help='Cantidad a comprar en la misma moneda.',
                        required=False
                        )
    parser.add_argument('--filtro',
                        '-f',
                        help='Filtramos monedas por [status|1h|4h|tend|tend4h|tend1h].',
                        required=False
                        )
    parser.add_argument('--telegram',
                        '-tg',
                        help='Formato de salida para telegram',
                        required=False,
                        default=False,
                        action="store_true"
                        )
    return parser


def main(args=None):

    global symbols, SEARCH_MARKET_SYMBOLS, FORMATO_SALIDA

    logger.debug('Iniciamos mercado')

    parser = get_parser()
    args = parser.parse_args(args)

    if args.telegram:
        FORMATO_SALIDA = 'telegram'

    if args.nosearchsymbols:
        SEARCH_MARKET_SYMBOLS = False

    if args.symbol:
        symbols = [args.symbol]
        symbols_descartados = []
        SEARCH_MARKET_SYMBOLS = False

    if args.list:
        symbols = args.list.split()
        symbols_descartados = []
        SEARCH_MARKET_SYMBOLS = False

    if args.open:
        loop = asyncio.get_event_loop()
        ejecucion = loop.run_until_complete(ejecutar_compra(args, 'Manual', False, False, True))
        if ejecucion:
            exit(0)
        else:
            exit(1)

    if args.close:
        loop = asyncio.get_event_loop()
        ejecucion = loop.run_until_complete(ejecutar_venta(args, 'Manual', False, False, True))
        if ejecucion:
            print('Profit total. {}'.format(ejecucion))
            exit(0)
        else:
            exit(1)

    os.system('clear')

    loop = asyncio.get_event_loop()

    while True:
        loop.run_until_complete(mercado(args))
        time.sleep(int(args.refresh))

if __name__ == '__main__':
    main()
