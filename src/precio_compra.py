#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## @file precio_compra.py
## @brief Recogemos precio de compra según par y fecha.

import os
import sys
import ccxt.async_support as ccxt  # noqa: E402
import argparse

sys.path.append('./src')

from console import *

import asyncio

__version__ = '0.1.0'
__author__ = u'edu@lesolivex.com'

salida = ''

async def precio_compra(exchange, symbol, date):
    global salida
    candles = await exchange.fetch_ohlcv(symbol, '1m', exchange.parse8601(date))
    salida = candles[0][4] # Precio de cierre.
    return salida

def get_parser():
    """
    Creates a new argument parser.
    """
    parser = argparse.ArgumentParser('precio_compra')
    version = '%(prog)s ' + __version__
    parser.add_argument('--version', '-v', action='version', version=version)
    # parser.add_argument('--exchange', '-e', help='Exchange')
    parser.add_argument('--symbol', '-s', help='Simbolo, ejemplo ETH/USDT', required=True)
    parser.add_argument('--date', '-d', help='Fecha del precio a buscar', required=True)
    return parser

def main(args=None):
    """
    Main entry point for your project.
    Args:
        args : list
            A of arguments as if they were input in the command line. Leave it
            None to use sys.argv.
    """

    global salida

    parser = get_parser()
    args = parser.parse_args(args)

    exchange = ccxt.binance()

    loop = asyncio.get_event_loop()
    loop.run_until_complete(precio_compra(exchange, args.symbol, args.date))
    loop.run_until_complete(exchange.close())

    print(salida)


if __name__ == '__main__':
    main()
