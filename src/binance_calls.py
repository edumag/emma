#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## Instalar dependencias.
## pip install python-binance

__version__ = '0.1.0'
__author__ = u'edu@lesolivex.com'

import sys
import argparse

sys.path.append('./src')

from base import *  # EE:W0401
from binance.client import Client
from precio_compra import precio_compra

async def last_buy(symbol):

    global exchange

    if TESTNET:
        client = Client(os.environ.get('TESTNET_APIKEY'), os.environ.get('TESTNET_SECRETKEY'))
        client.API_URL = 'https://testnet.binance.vision/api'
        client.testnet = True
        """
          'dapiPrivate': 'https://testnet.binancefuture.com/dapi/v1',
          'dapiPublic': 'https://testnet.binancefuture.com/dapi/v1',
          'fapiPrivate': 'https://testnet.binancefuture.com/fapi/v1',
          'fapiPrivateV2': 'https://testnet.binancefuture.com/fapi/v2',
          'fapiPublic': 'https://testnet.binancefuture.com/fapi/v1',
          'private': 'https://testnet.binance.vision/api/v3',
          'public': 'https://testnet.binance.vision/api/v3',
          'v1': 'https://testnet.binance.vision/api/v1',
          'v3': 'https://testnet.binance.vision/api/v3'
        """
        return False
    else:
        client = Client(os.getenv('binance_api'), os.getenv('binance_secret'))

    orders = client.get_all_orders(symbol=symbol.replace("/", ""), limit=10)

    last_order = {'time': 0, 'price': 0, 'orderId': 0, 'executedQty': 0}

    for o in orders:
        # print("\n\n-------")
        # print("\n\n------\n", o)
        # print("symbol: ", o['symbol'])
        # print("price: ", o['price'])
        # print("status: ", o['status'])
        # print("type: ", o['type'])
        # print("side: ", o['side'])
        # print("time: ", o['time'])

        if o['side'] == 'BUY' and o['status'] == 'FILLED':
            if int(o['time']) > int(last_order['time']):
                last_order = o

    if float(last_order['price']) == 0:

        price = True;

        try:
            last_order['price'] = round(float(last_order['origQuoteOrderQty']) / float(last_order['executedQty']), 8)
            # print('Calculo precio: ' , last_order['price'])
        except:
            price = False;

        # @todo En caso de obtener el precio buscarlo por fecha.
        if (last_order['price']) == 0:
            exchange = ccxt.binance()
            dt_object = datetime.fromtimestamp(int(last_order['time']) / 1000)
            last_order['price'] = await precio_compra(exchange, symbol, dt_object)
            await exchange.close()
        # print('Precio compra: ' , last_order['price'])

    # print("\n-- Last Order ---\n", last_order)

    return last_order

async def change_to_isolated(symbol):

    client = Client(os.getenv('binance_api'), os.getenv('binance_secret'))

    try:
        await client.futures_change_margin_type(symbol=symbol, marginType='ISOLATED')
    except Exception as e:
        if e.code == -4046:
            print('Isolated ya activado')
            return True
        return False

    return True

def get_parser():
    """
    Creates a new argument parser.
    """
    parser = argparse.ArgumentParser('balances')
    version = '%(prog)s ' + __version__
    parser.add_argument('--version', '-v', action='version', version=version)
    parser.add_argument('--symbol',
                        '-s',
                        help='Simbolo, ejemplo ETH/USDT',
                        required=True
                        )
    return parser

def main(args=None):
    parser = get_parser()
    args = parser.parse_args(args)

    l = last_buy(args.symbol)
    print(l)


if __name__ == '__main__':
    main()
