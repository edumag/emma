#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## Instalar dependencias.
## pip install python-binance

__version__ = '0.1.0'
__author__ = u'edu@lesolivex.com'

import sys
import asyncio
import pprint
sys.path.append('./src')
from base import *  # EE:W0401

async def load_markets():

    global exchange

    markets = await exchange.load_markets()
    await exchange.close()
    return markets

def main():

    loop = asyncio.get_event_loop()
    markets = loop.run_until_complete(load_markets())
    ms = []
    print('Count:', len(markets))
    for m in markets:
        # pprint.pprint(m)
        if CURRENCY in m:
            print(m.replace('/' + CURRENCY, ''), end=' ')


if __name__ == '__main__':
    main()
