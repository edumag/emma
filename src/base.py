#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import sys
import shutil
import ccxt.async_support as ccxt  # noqa: E402
import argparse
import json
import logging
import time
import sqlite3
from logging.handlers import RotatingFileHandler
import asyncio
import csv
from dotenv import load_dotenv
# Variables de entorno. (.env)
load_dotenv()
from distutils.util import strtobool

sys.path.append('./src')
sys.path.append('./')

from datetime import datetime, timedelta

REGISTRO_TIEMPOS_EVENTOS = {}

### Configuration.

DEBUG = strtobool(os.getenv('DEBUG', False))
SIMULA_COMPRAS_VENTAS = strtobool(os.getenv('SIMULA_COMPRAS_VENTAS', 'False'))
NOTIFY_DESKTOP = strtobool(os.getenv('NOTIFY_DESKTOP', 'False'))
TELEGRAM_CHAT_ID = os.getenv('TELEGRAM_CHAT_ID', False)
TELEGRAM_TOKEN = os.getenv('TELEGRAM_TOKEN', False)
CURRENCY = os.getenv('CURRENCY', 'USDT')
DATABASE = os.getenv('DATABASE', 'db/data.db')
TEMPORALIDADES = os.getenv('TEMPORALIDADES', '1d 4h 1h 15m 5m 1m' ).split()
TESTNET = strtobool(os.getenv('TESTNET', 'False'))
TOTAL_SYMBOLS = int(os.getenv('TOTAL_SYMBOLS', 20))
SEND_ALERT = os.getenv('SEND_ALERT', '').split()
SEARCH_MARKET_SYMBOLS = strtobool(os.getenv('SEARCH_MARKET_SYMBOLS', 'False'))

### Para configuraciones automaticas con reports.py -ce -mc -tc
MIN_MAX_TRADES_STRATEGY = os.getenv('MIN_MAX_TRADES_STRATEGY', 1)
MIN_MAX_LOSSES_STRATEGY = os.getenv('MIN_MAX_LOSSES_STRATEGY', 1)
DEF_MAX_TRADES_STRATEGY = os.getenv('DEF_MAX_TRADES_STRATEGY', 2)
DEF_MAX_LOSSES_STRATEGY = os.getenv('DEF_MAX_LOSSES_STRATEGY', 1)
MAX_MAX_TRADES_STRATEGY = os.getenv('MAX_MAX_TRADES_STRATEGY', 4)
MAX_MAX_LOSSES_STRATEGY = os.getenv('MAX_MAX_LOSSES_STRATEGY', 2)

### Antes de comprar comprobar.
FOLLOW_LAST_TWO =  os.getenv('FOLLOW_LAST_TWO', 1)
FOLLOW_STATUS_IN_TEST =  os.getenv('FOLLOW_STATUS_IN_TEST', 1)

### Compras en test.
AMOUNT_BUY_DEFAULT = 100
### Máximo número de trades.
MAX_TRADES_DEFAULT = 3
### Si llegamos a MAX_LOSSES_TRADES no compramos más.
MAX_LOSSES_TRADES_DEFAULT = 3
MIN_PROFIT_SELL_DEFAULT = 2                  # Si llegamos al 3% activamos venta.
FOLLOW_LOSSES_DEFAULT = -0.1                 # Si llegamos a FOLLOW_LOSSES vendemos en SELL-.
STOPLOSS_DEFAULT = -2                        # En StopLoss vendemos sino status no es BUY.
STOPLOSSTRAINIG_DEFAULT = '0.5;0.5,1;1,2;2'  # Si llegamos a STOPLOSSTRAINIG subimos STOPLOSS a (profit;distancia)
### Estrategia por defecto.
### @todo Asegurarnos que la tenemos.
DEFAULT_SESION = 'M'

if TESTNET:
    DATABASE = 'db/datatest.db'

if strtobool(os.getenv('PYTEST', 'False')):
    DATABASE = '/tmp/datatest.db'
    shutil.copyfile('db/data.db', DATABASE)
    print('PYTEST DATABASE:', DATABASE)

# Notificaciones de escritorio.
if NOTIFY_DESKTOP:
    import gi
    gi.require_version('Notify', '0.7')
    from gi.repository import Notify
    Notify.init("EmCcxt")

### Otros.

EJECUTAR_COMPRAS_AUTOMATICAS_HUMAN = 'compras_activadas'
EJECUTAR_VENTAS_AUTOMATICAS_HUMAN = 'ventas_activadas'
MIN_PROFIT_SELL_HUMAN = 'min_profit_sell'
STOPLOSS_HUMAN = 'stoploss'
FOLLOW_LOSSES_HUMAN = 'follow_losses'
STOPLOSSTRAINIG_HUMAN = 'stoptrailing'
AMOUNT_BUY_HUMAN = 'amount_buy'
MAX_TRADES_HUMAN = 'max_trades'
MAX_LOSSES_TRADES_HUMAN = 'max_losses'
DESCARTAR_VENTAS_HUMAN = 'descartar_ventas'

TIME_TEST = {
    '15m': '-1 hours',
    '1h':  '-4 hours',
    '2h':  '-8 hours',
    '4h':  '-24 hours',
    '1d':  '-3 days',
}

MEMCACHE = {}

## terminal/telegram
FORMATO_SALIDA = "terminal"

# logging.basicConfig(filename='logs/emma.log', level=logging.INFO)

if not os.path.exists('logs'):
    os.mkdir('logs')

rfh = logging.handlers.RotatingFileHandler(
    filename='logs/emma.log',
    mode='a',
    maxBytes=5*1024*1024,
    backupCount=2,
    encoding=None,
    delay=0
)

logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s %(levelname)s %(funcName)s() %(message)s",
    datefmt="%y-%m-%d %H:%M:%S",
    handlers=[
        rfh
    ]
)

logger = logging.getLogger('main')
if DEBUG:
    logger.setLevel(logging.DEBUG)
logger.debug("---------------------------------")

# root = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
# sys.path.append(root + '/python')

exchanges = {
    'binance': [os.getenv('binance_api'), os.getenv('binance_secret')],
    'testnet': [os.getenv('TESTNET_APIKEY'), os.getenv('TESTNET_SECRETKEY')],
    'kraken': [os.getenv('kraken_api'), os.getenv('kraken_secret')]
    }

if TESTNET:
    exchange = ccxt.binance({
        'apiKey': os.getenv('TESTNET_APIKEY'),
        'secret': os.getenv('TESTNET_SECRETKEY'),
        'verbose': False,  # switch it to False if you don't want the HTTP log
        'options': {
            'adjustForTimeDifference': False,
            'createMarketBuyOrderRequiresPrice': False
            }
    })
    exchange.set_sandbox_mode(True)
    exchange.urls['api'] = exchange.urls['test']
else:
    exchange = ccxt.binance({
        'apiKey': exchanges['binance'][0],
        'secret': exchanges['binance'][1],
        'verbose': False,  # switch it to False if you don't want the HTTP log
        'options': {
            'adjustForTimeDifference': False,
            'createMarketBuyOrderRequiresPrice': False
            }
    })

exchange_futures = ccxt.binance({
    'apiKey': exchanges['binance'][0],
    'secret': exchanges['binance'][1],
    'verbose': False,  # switch it to False if you don't want the HTTP log
    'options': {
        'createMarketBuyOrderRequiresPrice': False,
        'defaultType': 'future',
        'adjustForTimeDifference': True,
        'enableRateLimit': True,
        'rateLimit': 250
        }
})

from console import *
from open_orders import *
from telegram_send_message import *

async def fetch_ticker(exchange, symbol):
    try:
        ticker = await exchange.fetchTicker(symbol)
    except Exception as e:
        print('Error: Sin conexión')
        ticker = False
    return ticker

BTC = None
btc_price = None

EURUSDT = None
eurusdt_price = None

total_exchanges_usd = 0

logger.debug('DATABASE: ' + str(DATABASE))

def getCache(key):

    # Borrar ficheros de cache antiguos.
    # find ~/.cache/emma/ -mtime +1 -exec rm {} \; Más de una hora.
    # find ~/.cache/emma/ -mmin +1 -exec rm {} \;  Más de un minuto.
    cmd = "find ~/.cache/emma/  -mmin +10 -type f -exec rm '{}' \\+"
    os.system(cmd)

    key = key.replace("/", "")
    path = os.getenv("HOME") + '/.cache/emma/'
    if not os.path.exists(path):
        os.mkdir(path)

    if not os.path.isfile(path + key):
        return False

    f = open(path + key, 'r')
    mensaje = json.loads(f.read())
    f.close()
    return mensaje

def setCache(key, value):

    key = key.replace("/", "")
    path = os.getenv("HOME") + '/.cache/emma/'
    if not os.path.exists(path):
        os.mkdir(path)

    f = open(path + key, 'a')
    f.write(json.dumps(value))
    f.close()

def getMemCache(key):

    global MEMCACHE

    TIME_CACHE = 60 ## (10 * 60)  # 5m = 5 * 60

    if key in MEMCACHE:
        if (time.time() - MEMCACHE[key]['timestamp']) > TIME_CACHE:
            return False
        return json.loads(MEMCACHE[key]['value'])

    return False

def setMemCache(key, value):

    MEMCACHE[key] = {
        'value': json.dumps(value),
        'timestamp': time.time()
        }

    return getMemCache(key)

def human2sql(human, monitor=True):

    sql = ""
    prefix = ""

    if monitor:
        prefix = "mon_"

    def convert(condicion):
        add = ''
        if condicion[-1:] == ')':
            add = ')'
        subsql = condicion.replace('[', '')
        subsql = subsql.replace(']', '')
        subsql = subsql.replace(')', '')
        campo, operador, value = subsql.split(':')
        len_value = len(value)
        if len_value == 6:
            subsql = "cast(" + prefix + campo + " as integer ) " + operador + " " + value
        else:
            subsql = "cast(substr(" + prefix + campo + ", -" + str(len_value) + ") as integer) " + operador + " " + value
        return subsql + add

    try:

        for h in human.split():
            if h[0] == '[':
                sql += " " + convert(h)
            elif h[0] == '(':
                sql += "(" + convert(h[1:])
            else:
                sql += " " + h

    except Exception as e:
        m = "ERROR (Condición invalidada): ({})".format(human)
        print(m)
        print(e)
        logger.error(e)
        exit(1)
        return False

    return sql

def sql_exec(sql, conn=False, values=False, row=False):
    ###
    ### @param values Valores en la consulta sql.
    ### @param row    Devolver array con los nombres de los campos.

    # print('SQL: ' + sql)  # @DEV
    if conn:
        con = conn
    else:
        con = sqlite3.connect(DATABASE)
    try:
        if row:
            con.row_factory = sqlite3.Row
        cur = con.cursor()
        if values:
            cur.executemany(sql, values)
        else:
            cur.execute(sql)
        rows = cur.fetchall()
        con.commit()
        # con.close()
        return rows
    except Exception as e:
        if not conn:
            con.close()
        if "database is locked" in str(e):
            msg = 'DB bloqueada, esperamos 1s'
            logger.info(msg)
            time.sleep(1)
            return sql_exec(sql, conn, values)
        else:
            print(e)
            logger.error(e)
            logger.error('SQL: ' + sql)
        return False

    return False

def calculo_stoptrailing(stoptrailing, profit, stoploss=False):
    ### Sistema de stoptrailing variable;
    ### Ejemplos: "2;1" "4;4,10;1" "1;1,2;2,3;3,5;1"
    ### El stoploss colocado debe ser siempre mayor al actual.

    stoploss_original = False
    stoploss_actual = False

    if stoploss is not False and stoploss:
        stoploss_original = float(stoploss)

    if profit is not False:
        profit = float(profit)

    if stoptrailing:

        # Recorremos lista de stoptrailing el que nos de el stoploss más
        # alto lo devolvemos.
        for st in str(stoptrailing).split(','):
            st_array = st.split(';')
            if len(st_array) > 1:
                try:

                    stop, distance, maxstop = st_array
                    stop     = float(stop)
                    distance = float(distance)
                    maxstop  = float(maxstop)

                except Exception as e:
                    msg = 'StopTrailing mal definido: [{}]'.format(stoptrailing)
                    logger.error(msg)
                    print(msg)
                    eventos('ERROR', msg)
                    return stoploss_original

                if profit >= stop:
                    if (profit - distance) >= maxstop:
                        stoploss_actual = maxstop
                    else:
                        stoploss_actual = ( profit - distance )

            else:
                msg = 'StopTrailing mal definido: [{}]'.format(stoptrailing)
                logger.error(msg)
                eventos('ERROR', msg)
                return stoploss_original

    if stoploss_actual is not False and ( stoploss_original is False  or stoploss_actual >= stoploss_original ):
        return stoploss_actual

    return stoploss_original

def min2hours(min):
    hours = min // 60
    minutes = min % 60
    return "{:2d}:{:02d}".format(hours, minutes)

def eventos(evento, mensaje, tiempo=False):

    global REGISTRO_TIEMPOS_EVENTOS, TELEGRAM_TOKEN, TELEGRAM_CHAT_ID

    # Si no tenemos evento en lista de SEND_ALERT salimos.
    if evento in SEND_ALERT:
        logger.info('EVENTO:: [{}] [{}]'.format(evento, mensaje))
    else:
        logger.info('EVENTO SIN ALERTA:: [{}] [{}]'.format(evento, mensaje))
        return

    now = datetime.now()
    if tiempo:
        if not evento in REGISTRO_TIEMPOS_EVENTOS:
            REGISTRO_TIEMPOS_EVENTOS[evento] = now

        if datetime.now() > REGISTRO_TIEMPOS_EVENTOS[evento]:
            try:
                logger.debug('Enviamos evento: ' + mensaje)
                tlg_send(mensaje)
                if NOTIFY_DESKTOP:
                    Notify.Notification.new(mensaje).show()
            except Exception as e:
                logger.error(e)

            REGISTRO_TIEMPOS_EVENTOS[evento] = datetime.now() + timedelta(minutes = tiempo)

        else:
            logger.debug('Esperamos hasta {}, evento: {}'.format(REGISTRO_TIEMPOS_EVENTOS[evento], mensaje))
    else:
        logger.debug('EVENTO SIN TIEMPO DEFINIDO: [{}], enviamos sin tener en cuenta tiempos'.format(evento))
        tlg_send(mensaje)
        if NOTIFY_DESKTOP:
            Notify.Notification.new(mensaje).show()

def getProfit(side, last_price, buy_price):
    ###
    ### Si side es igual a SHORT hay que invertir el profit.
    ###

    profit = ((last_price * 100) / float(buy_price)) - 100

    if (profit > 0 and ( side == 'LONG' or side == 'SPOT' )) or (profit < 0 and side == 'SHORT'):
        profit_actual = abs(profit)
        profit_str = green(str(round(profit_actual, 2)) + '%')
    else:
        profit_actual = abs(profit) - (abs(profit) * 2)
        profit_str = red(str(round(profit_actual, 2)) + '%')

    return profit_actual, profit_str
