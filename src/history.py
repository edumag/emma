#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
La función fetch_orders con kraken no  nos da permiso a no ser que tengamos permisos
para realizar retiradas desde la API, lo cual sería un fallo de seguridad importante.
Descartamos el código hasta que kraken lo solucione.
"""
import os
import sys
import ccxt.async_support as ccxt  # noqa: E402
import argparse
import pprint

sys.path.append('./src')

from base import *
from console import *
from precio_compra import *

import asyncio

__version__ = '0.1.0'
__author__ = u'edu@lesolivex.com'

salida = ''

async def history(exchange, symbol=None, show=False, lastsell=False):


    if symbol is not None:
        SYMBOLS = [symbol]
    elif exchange.id == 'binance':
        SYMBOLS = os.getenv('binance_symbols')
    else:
        SYMBOLS = os.getenv('kraken_symbols')

    since = exchange.milliseconds () - (86400000 * 90 ) # 86400000 = 1 day in milliseconds.

    for code in SYMBOLS:

        try:

            if show:
                print("\n## CURRENCY: ", code)
            if exchange.id == 'binance' and exchange.has['fetchOrders']:
                orders = await exchange.fetch_orders(code, since, 1000)

            elif exchange.has['fetchLedger']:
                orders = await exchange.fetch_ledger(code, None, 20)
            else:
                raise Exception (exchange.id + ' does not have the fetchLedger method')
                return False

        except Exception as e:

            print(e)
            return False


        last_order = {'timestamp': 0}
        formato = "{:<26} {:<8} {:<4} {:<18} {:<10} {:<10}"
        print(formato.format('Date', 'Coin', 'Side', 'Type', 'Price', 'Quantity'))
        for x in range(orders.__len__()):
            d = orders[x-1]
            if show and not lastsell:

                # print("symbol:", d['info']['symbol'])
                # print("origQty:", d['info']['origQty'])
                # print("cummulativeQuoteQty:", d['info']['cummulativeQuoteQty'])
                # print("status:", d['info']['status'])
                # print("side:", d['info']['side'])
                # print("stopPrice:", d['info']['stopPrice'])
                # print("datetime:", d['datetime'])
                # print("type:", d['info']['type'])
                # print("cost:", d['cost'])
                # print("amount:", d['amount'])
                # print("price:", d['price'])
                # print('---------')

                print(formato.format(
                    d['datetime'],
                    d['info']['symbol'],
                    d['info']['side'],
                    d['info']['type'],
                    round(float(d['price']), 2),
                    round(float(d['info']['cummulativeQuoteQty']), 2)
                    )
                )

            else:

                if d['side'] == 'sell' and d['status'] == 'closed':
                    if int(d['timestamp']) > int(last_order['timestamp']):
                        last_order = d

        if lastsell and show and last_order['timestamp'] != 0:
            print("symbol:", last_order['info']['symbol'])
            print("origQty:", last_order['info']['origQty'])
            print("cummulativeQuoteQty:", last_order['info']['cummulativeQuoteQty'])
            print("status:", last_order['info']['status'])
            print("side:", last_order['info']['side'])
            print("stopPrice:", last_order['info']['stopPrice'])
            print("datetime:", last_order['datetime'])
            print("type:", last_order['info']['type'])
            print("cost:", last_order['cost'])
            print("amount:", last_order['amount'])
            print("price:", last_order['price'])
            print('---------')
        else:
            if last_order['timestamp'] != 0:
                return last_order
            else:
                return False

    return orders

def get_parser():
    """
    Creates a new argument parser.
    """
    parser = argparse.ArgumentParser('history')
    version = '%(prog)s ' + __version__
    parser.add_argument('--version', '-v', action='version', version=version)
    # parser.add_argument('--exchange', '-e', help='Exchange')
    parser.add_argument('--symbol', '-s', help='Simbolo, ejemplo ETH/USDT', required=False)
    parser.add_argument('--last_sell', '-ls', help='Última venta de [symbol]', required=False)
    # parser.add_argument('--date', '-d', help='Fecha del precio a buscar', required=True)
    return parser


def main(args=None):
    """
    Main entry point for your project.
    Args:
        args : list
            A of arguments as if they were input in the command line. Leave it
            None to use sys.argv.
    """

    global salida, exchange

    parser = get_parser()
    args = parser.parse_args(args)

    if args.last_sell:
        loop = asyncio.get_event_loop()
        loop.run_until_complete(history(exchange, args.last_sell, True, True))
        loop.run_until_complete(exchange.close())
    else:
        loop = asyncio.get_event_loop()
        loop.run_until_complete(history(exchange, args.symbol, True))
        loop.run_until_complete(exchange.close())
        # loop.run_until_complete(history(kraken, args.symbol))
        # loop.run_until_complete(kraken.close())


    print(salida)


if __name__ == '__main__':
    main()
