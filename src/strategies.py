#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## @file estrategias.py
## @brief Gestionar estrategias.

__version__ = '0.1.0'
__author__ = u'edu@lesolivex.com'

import sys
import sqlite3
import argparse
import os
import csv

sys.path.append('./src')

from base import *  # pylint: disable=W0401
from db import *  # pylint: disable=W0401

FORMATO_SALIDA  = False

def strategy_add(name, mark, description, type, leverage, temporalidad, eid=False):

    if eid:
        sql = "UPDATE estrategias SET name = '{}', mark = '{}', descripcion = '{}', type = '{}', leverage = {}, temporalidad = '{}' WHERE id = {}".format(
            name,
            mark,
            description,
            type,
            leverage,
            temporalidad,
            eid
        )
    else:
        sql = "INSERT INTO estrategias (name, mark, descripcion, type, leverage, temporalidad) VALUES ('{}', '{}', '{}', '{}', {}, '{}')".format(
            name,
            mark,
            description,
            type,
            leverage,
            temporalidad
        )

    # Añadimos datos de inicio.
    try:

        con = sqlite3.connect(DATABASE)
        cur = con.cursor()
        cur.execute(sql)
        if not eid:
            eid = cur.lastrowid
        con.commit()
        con.close()
        return eid

    except Exception as e:
        logger.error(e)
        print(e)
        con.close()
        return

def strategy_del(eid, onlycontent=True):

    if not onlycontent:
        print("!!!Cuidado, no borres una estrategia que ha sido usada porque puede dar problemas al no encontrala")
        option = input("\nContinuar (s/n): ")
        if option != 's':
            return

    sql_signals  = "DELETE FROM senyales WHERE eid = {}".format(eid)
    sql_config   = "DELETE FROM config WHERE eid = {}".format(eid)
    sql_strategy = "DELETE FROM estrategias WHERE id = {}".format(eid)
    logger.debug(sql_signals)
    logger.debug(sql_config)
    logger.debug(sql_strategy)

    try:

        con = sqlite3.connect(DATABASE)
        cur = con.cursor()
        cur.execute(sql_signals)
        cur.execute(sql_config)
        if not onlycontent:
            cur.execute(sql_strategy)
        con.commit()
        con.close()

    except Exception as e:
        raise IOError(e)
        logger.error(e)
        print(e)
        con.close()
        return

def signal_add(eid, accion, condicion, sid=False):

    if sid:
        # Update
        sql = "UPDATE senyales SET eid = {}, accion = '{}', condicion = '{}' WHERE id = {}".format(
            eid,
            accion,
            condicion,
            sid
        )
    else:
        # Cretae
        sql = "INSERT INTO senyales (eid, accion, condicion) VALUES ('{}', '{}', '{}')".format(
            eid,
            accion,
            condicion
        )
    # Añadimos datos de inicio.
    try:

        con = sqlite3.connect(DATABASE)
        cur = con.cursor()
        cur.execute(sql)
        con.commit()
        con.close()

    except Exception as e:
        logger.error(e)
        print(e)
        con.close()
        return

def signal_del(id):

    sql_signals  = "DELETE FROM senyales WHERE id = {}".format(id)
    sql_exec(sql_signals)
    return

def strategy_create(eid=False):

    # Pedir nombre de estrategia.
    name = input("Name: ")
    # Marca.
    mark = input("Mark: ")
    type = input("Type: [SPOT, LONG o SHORT]")
    leverage = input("Leverage: ")
    # Descripción.
    description = input("Description: ")
    # Añadir a DB.
    strategy_add(name, mark, description, type, leverage, eid)

def strategy_show(eid):
    strategy = get_strategy(eid)
    if strategy:

        print("\n{:<5} {} Type: {}[{}] Name: {}\n\n{}".format('[' + str(strategy['id']) + ']', strategy['mark'], strategy['type'], strategy['leverage'], strategy['name'], strategy['descripcion']))
        # Mostrar señales actuales.
        signals = get_signals(strategy['mark'])
        print("\nSignals:\n")
        for s in signals:
            print("{:<5} {:<5} {}".format('[' + str(s['id']) + ']', s['accion'], s['condicion']))
        print("\nConfig:")
        print(show_config(strategy['mark']))

    else:

        print("Estrategia [{}] no encontrada".format(eid))
        return False

def strategy_show_inline(eid):
    strategy = get_strategy(eid)
    print("{:<5} {} {} {} {} {}".format('[' + str(strategy['id']) + ']', strategy['mark'], strategy['type'], strategy['temporalidad'], strategy['name'], strategy['descripcion']))

def strategies_list(show=True, mark=False):

    sql = "SELECT * FROM estrategias"
    try:

        con = sqlite3.connect(DATABASE)
        con.row_factory = sqlite3.Row
        cur = con.cursor()
        cur.execute(sql)
        strategies = cur.fetchall()
        if not show:
            return strategies

        if mark == 'all':
            for strategy in strategies:
                strategy_show(strategy['id'])
        elif not mark:
            for strategy in strategies:
                strategy_show_inline(strategy['id'])

        else:
            strategy_show(get_eid(mark))
            # print(show_config(mark))

        con.commit()
        con.close()

    except Exception as e:
        logger.error(e)
        print(e)
        con.close()
        return

def strategy_menu():

    salir = False
    while not salir:

        # List.
        strategies_list()
        # Acción.
        eid = input("\nSelect id strategy or [C]reate [V]er todas: ")
        if eid == '':
            return
        elif eid == 'V':
            strategies_list(True, True)
        elif eid == 'C':
            strategy_create()
        else:
            strategy_show(eid)
            opcion = input("\n[Enter] Sale, [D]elete, [U]pdate [S]ignal [C]onfig: ")
            if opcion == 'D':
                print('Borramo estrategia ' + str(eid))
                strategy_del(eid, False)
            elif opcion == 'C':
                config_strategy(eid)
            elif opcion == 'U':
                strategy_create(eid)
            elif opcion == 'S':
                signals_menu(eid)
            else:
                return

def signals_menu(eid):
    salir = False
    while not salir:
        strategy_show(eid)
        option = input("\n[ENTER] Sale, [C]reate [U]pdate [D]elete: ")
        if option == 'C':
            signal_create(eid)
        elif option == 'U':
            signal_update(eid)
        elif option == 'D':
            id_signal = input("\nId de señal: ")
            signal_del(id_signal)
        else:
            return

def config_strategy(eid):

    strategy = get_strategy(eid)
    print(show_config(strategy['mark']))
    new_config = input("New config: ")
    if set_config(strategy['mark'], new_config):
        print("Configuración guardada")
    else:
        print("Configuración invalidada")
    print(show_config(strategy['mark']))

def signal_create(eid=False):

    salir = False

    if not eid:
        # Pedir marca de estartegia, acción, patron.
        mark = input("Marca de estrategia: ")
        # Buscar estrategia y presentar nombre.
        eid = get_eid(mark)

    while not salir:
        strategy_show(eid)
        new_accion = input("Acción: ")
        new_condicion = input("Condición: ")
        signal_add(eid, new_accion, new_condicion)
        strategy_show(eid)
        option = input("\n[N]ew [E]xit: ")
        if option != 'N':
            salir = True

def change_active(mark, new_active):

    eid = get_eid(mark)
    sql = "UPDATE senyales SET accion = 'TOPEN' WHERE eid = {} AND accion = 'OPEN'".format(eid)
    sql_exec(sql)
    sql = "UPDATE senyales SET accion = 'OPEN' WHERE eid = {} AND condicion = '{}'".format(eid, new_active)
    sql_exec(sql)

def signal_update(eid=False):

    salir = False
    while not salir:

        if not eid:
            # Pedir marca de estartegia, acción, patron.
            mark = input("[ENTER] Sale o marca de estrategia: ")
            if mark == '':
                return
            # Buscar estrategia y presentar nombre.
            eid = get_eid(mark)
            strategy_show(eid)
        sid = input("[Enter] sale o Id de señal: ")
        if sid == '':
            return
        new_accion = input("Acción: ")
        new_condicion = input("Condición: ")
        signal_add(eid, new_accion, new_condicion, sid)
        strategy_show(eid)

def strategy_import(mark, force=False):

    update = False

    if force:
        print('Forzamos importación de [{}]'.format(mark))

    new_mark = mark

    # @todo
    # Comprobar si la estrategia existe.
    # Preguntar si la quiere reemplazar o añadir con otra marca.
    eid = get_eid(mark)
    if eid:
        if force:
            update = True
        else:
            print('Ya tenemos una estrategia con la misma marca')
            option = input('[R]emplazar [I]mportar con otra marca: ')
            if option == "R":
                update = True
            elif option == "I":
                new_mark = input("Nueva marca: ")
                eid = False
            else:
                print("Sin opción adecuada, salimos")
                return

    if update:
        # Borramos contenido de la DB.
        strategy_del(eid, True)

    with open('strategies/' + mark + '/strategy.csv', mode='r') as csvfile:
        spamreader = csv.reader(csvfile)
        for row in spamreader:
            print(row)
            name, mark_old, descripcion, type, leverage, temporalidad = row

        try:
            eid = strategy_add(name, new_mark, descripcion, type, leverage, temporalidad, eid)
        except Exception as e:
            print("ERROR: Añadiendo estrategia [{}]".format(new_mark))
            print(e)
            exit()

    if not eid:
        print('ERROR al añadir/modificar estrategia [{}]'.format(mark))
        exit(1)

    print('eid: ' + str(eid))

    with open('strategies/' + mark + '/config.csv', mode='r') as csvfile:
        spamreader = csv.reader(csvfile)
        for row in spamreader:
            # print(row)
            (
                eid_old, date, compras_activadas, ventas_activadas,
                min_profit_sell, stoploss, follow_losses, stoptrailing,
                amount_buy, max_trades, max_losses, sell_with_loss
            ) = row

        data = "compras_activadas:{} ventas_activadas:{} min_profit_sell:{} stoploss:{} follow_losses:{} stoptrailing:{} amount_buy:{} max_trades:{} max_losses:{} sell_with_loss:{}".format(
            compras_activadas,
            ventas_activadas,
            min_profit_sell,
            stoploss,
            follow_losses,
            stoptrailing,
            amount_buy,
            max_trades,
            max_losses,
            sell_with_loss
        )
        set_config(new_mark, data)

    with open('strategies/' + mark + '/signals.csv', mode='r') as csvfile:
        spamreader = csv.reader(csvfile)
        for row in spamreader:
            # print(row)
            eid_old, accion, condicion = row
            signal_add(eid, accion, condicion)

    print("Estrategia [{}] importada correctamente.".format(new_mark))
    strategy_show(eid)

def strategy_export(mark):

    eid = get_eid(mark)
    try:

        con = sqlite3.connect(DATABASE)
        cur = con.cursor()
        try:
            os.mkdir('strategies/' + mark)
        except OSError:
            print("Destino: " + mark)
        result_sql = cur.execute("SELECT * FROM estrategias WHERE mark like '{}'".format(mark))
        with open('strategies/' + mark + '/strategy.csv', mode='w') as out_csv_file:
            csv_out = csv.writer(out_csv_file)
            for result in result_sql:
                print('strategy:', result[1:])
                csv_out.writerow(result[1:])
        result_config = cur.execute("SELECT * FROM config WHERE eid = {} ORDER BY id desc LIMIT 1".format(eid))
        with open('strategies/' + mark + '/config.csv', mode='w') as out_csv_file:
            csv_out = csv.writer(out_csv_file)
            for result in result_config:
                print('config:', result[1:])
                csv_out.writerow(result[1:])
        result_senyales = cur.execute("SELECT * FROM senyales WHERE eid = {}".format(eid))
        with open('strategies/' + mark + '/signals.csv', mode='w') as out_csv_file:
            csv_out = csv.writer(out_csv_file)
            for result in result_senyales:
                print('signals:', result[1:])
                csv_out.writerow(result[1:])

        con.commit()
        con.close()

    except Exception as e:
        logger.error(e)
        print(e)
        con.close()
        return
def get_parser():
    """
    Creates a new argument parser.
    """
    parser = argparse.ArgumentParser('strategies')
    version = '%(prog)s ' + __version__
    parser.add_argument('--version', '-v', action='version', version=version)
    parser.add_argument('--create', '-c', help='Insertar estragia', action='store_true')
    parser.add_argument('--create_signal', '-cs', help='Insertar señal', action='store_true')
    parser.add_argument('--menu', '-m', help='Menú de estrategias', action='store_true')
    parser.add_argument('--export', '-e', help='Exportar estrategias, ejemplo. -e [L], [all]')
    parser.add_argument('--importar', '-i', help='Importar estrategias, ejemplo. -i [L], [all]')
    parser.add_argument('--show', '-s', help='Ver estrategia, [all] todas.Ejemplo --show C')
    parser.add_argument('--telegram',
                        '-tg',
                        help='Formato de salida para telegram',
                        required=False,
                        default=False,
                        action="store_true"
                        )

    return parser

def main(args=None):
    """
    Main entry point for your project.
    Args:
        args : list
            A of arguments as if they were input in the command line. Leave it
            None to use sys.argv.
    """

    global FORMATO_SALIDA

    parser = get_parser()
    args = parser.parse_args(args)

    if args.telegram:
        FORMATO_SALIDA = 'telegram'

    if args.show:
        strategies_list(True, args.show)
        exit()

    if args.importar:
        if args.importar == 'all':
            estrategias = os.listdir('strategies/')
            for e in estrategias:
                print("Importamos estrategia [{}]".format(e))
                strategy_import(e, True)
        else:
            strategy_import(args.importar)

    if args.export:
        if args.export == 'all':
            estrategias = sql_exec('SELECT mark FROM estrategias')
            for e in estrategias:
                strategy_export(str(e[0]))
        else:
            strategy_export(args.export)

    if args.menu:
        strategy_menu()

    if args.create_signal:
        signal_create()


if __name__ == '__main__':
    main()
