# TODO

## Aplicar tiempo en operaciones.

Ejemplo:

[date:>:60] and [profit:<:1.5]:

Fecha de compra pasa los 60 minutos y el profit es menor 1.5


## Mercado

- Separar base de datos con datos de mercado de la DB de la app.
- Websockets.


## Estrategias


## DB

- Validar stoploss y stoptrailing cuando lo añadimos manualmente.

## Reports

- Añadir fecha inicio y final para los informes.

## Varios

### Acciones

Realizar acciones cuando se cumpla cierta condición.

Ejemplo: Si macd en 4h es mayor a 8 ejecutar compra.

Una manera podría ser concatenando ordenes, ejemplo:

```
./condicion.py -c BTC [precios:<:50] and [precios:<:5] && ./src/mercado.py -bc BTC -ad 100 -sy A -lv 10 
```

## Calcular comisiones

En modo SIMULA se debería añadir un porcentaje pequeño en el cierre ya que em modo REAL en el tiempo
en que se manda la orden y se recibe puede tardar unos segundos y haber un cambio de precio.
