# Develop

## Installation with venv

### Ubuntu or debian based.

#### python 3.8 in ubuntu 18.04

sudo apt install software-properties-common
sudo add-apt-repository ppa:deadsnakes/ppa
sudo apt-get install python3.8 python3.8-venv

#### python 3.8 in ubuntu 20.04

sudo apt install python3.8-venv

### App

git clone git@gitlab.com:edumag/emma.git
cd emma
python3.8 -m venv env
source env/bin/activate
python -m pip install --upgrade pip
pip install -r requirements.txt

## Examples in python

https://github.com/ccxt/ccxt/tree/master/examples/py


## Modifications from the server

To avoid having to enter the password every time we update the code.

```
ssh-agent
ssh-add ~/.ssh/id_rsa_gitlab
```
## Download database to analyze data

Example:

```
scp server_ip:/home/ubuntu/emma/data.db ./
```

## pytest

```
pytest -v -s
```

### Deploy with git.

```
$ cat .git/hooks/pre-push
./scripts/tests
```
## Debug

https://docs.python.org/es/3/library/pdb.html

Import pdb into the file we want to debug.

```
import pdb
```

Add breakpoint in the line of code that we want to inspect.

```
pdb.set_trace()
```