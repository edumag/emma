# Estrategias

## Comparar resultados con diferentes configuraciones

./src/reports.py -rd '-24 hours' -st A -ts -tc

-st:  Test estrategia.
-ts:  Testear señales, Añade configuraciones con mucho margen para poder
      estudiar resultados.
-tc:  Testear configuraciones.

Opcionalmente:

-vv: Muestra los detalles de cada operación simulada.

```
Estrategia: A time:  -24 hours

[5]   A Type: SHORT[5] Name: Short 15m

Entramos con precios arriba esperando que bajen

Signals:

[31]  SHORT [precios:>:50000] and [precios:>:50000] and [precios:>:5000] and [precios:>:500] and [precios:<:50]

Config:
compras_activadas:1 ventas_activadas:1 sell_with_loss:1 min_profit_sell:0.5 stoploss:-1.0 follow_losses:-0.5 stoptrailing:0;1;0.2,2;1;1,3;2;2,5;2;3,7;4;4,10;3;100 amount_buy:100.0 max_trades:2 max_losses:1

Estrategia: A Type: SHORT Leverage: 5 Resultado en USDT: -16.34$ Tiempo: -24 hours 
min_profit_sell:0.5 stoploss:-1.0 follow_losses:-0.5 stoptrailing:0;1;0.2,2;1;1,3;2;2,5;2;3,7;4;4,10;3;100
Operaciones:11 Positivas:0 Negativas:0 
Ventas: False:11 
Totales por tipo de ventas
False :  8.03 %
MaxProfit:1.77% MinProfit:-0.08%
SinVentas: 8.03 StopSinVentas: -3.27
ResultCurrency: -16.34

Estrategia: A Type: SHORT Leverage: 5 Resultado en USDT: -16.34$ Tiempo: -24 hours 
min_profit_sell:0.5 stoploss:-1.0 follow_losses:-0.5 stoptrailing:0;1;0.2,2;1;1,3;2;2,5;2;3,7;4;4,10;3;100
Operaciones:11 Positivas:0 Negativas:0 
Ventas: False:11 
Totales por tipo de ventas
False :  8.03 %
MaxProfit:1.77% MinProfit:-0.08%
SinVentas: 8.03 StopSinVentas: -3.27
ResultCurrency: -16.34

Estrategia: A Type: SHORT Leverage: 5 Resultado en USDT: -43.33$ Tiempo: -24 hours 
min_profit_sell:1.0 stoploss:-2.0 follow_losses:-1.0 stoptrailing:0;2;0.2,1;0.5;30
Operaciones:11 Positivas:0 Negativas:0 
Ventas: False:11 
Totales por tipo de ventas
False :  8.03 %
MaxProfit:1.77% MinProfit:-0.08%
SinVentas: 8.03 StopSinVentas: -8.67
ResultCurrency: -43.33

Estrategia: A Type: SHORT Leverage: 5 Resultado en USDT: -120.83$ Tiempo: -24 hours 
min_profit_sell:1.0 stoploss:-3.0 follow_losses:-1.0 stoptrailing:0;3;0.2,0;3;30
Operaciones:11 Positivas:0 Negativas:0 
Ventas: False:11 
Totales por tipo de ventas
False :  8.03 %
MaxProfit:1.77% MinProfit:-0.08%
SinVentas: 8.03 StopSinVentas: -24.17
ResultCurrency: -120.83

Estrategia: A Type: SHORT Leverage: 5 Resultado en USDT: -230.83$ Tiempo: -24 hours 
min_profit_sell:1.0 stoploss:-5.0 follow_losses:-1.0 stoptrailing:0;5;0.5,3;3;6,6;4;10,10;6;90
Operaciones:11 Positivas:0 Negativas:0 
Ventas: False:11 
Totales por tipo de ventas
False :  8.03 %
MaxProfit:1.77% MinProfit:-0.08%
SinVentas: 8.03 StopSinVentas: -46.17
ResultCurrency: -230.83

Estrategia: A Type: SHORT Leverage: 5 Resultado en USDT: -65.83$ Tiempo: -24 hours 
min_profit_sell:1.0 stoploss:-2.0 follow_losses:-1.0 stoptrailing:0;2;30
Operaciones:11 Positivas:0 Negativas:0 
Ventas: False:11 
Totales por tipo de ventas
False :  8.03 %
MaxProfit:1.77% MinProfit:-0.08%
SinVentas: 8.03 StopSinVentas: -13.17
ResultCurrency: -65.83

Estrategia: A Type: SHORT Leverage: 5 Resultado en USDT: -505.83$ Tiempo: -24 hours 
min_profit_sell:0.1 stoploss:-50.0 follow_losses:-0.1 stoptrailing:0;10;10
Operaciones:11 Positivas:0 Negativas:0 
Ventas: False:11 
Totales por tipo de ventas
False :  8.03 %
MaxProfit:1.77% MinProfit:-0.08%
SinVentas: 8.03 StopSinVentas: -101.17
ResultCurrency: -505.83

Estrategia: A Type: SHORT Leverage: 5 Resultado en USDT: -230.83$ Tiempo: -24 hours 
min_profit_sell:3.0 stoploss:-5.0 follow_losses:-3.0 stoptrailing:0;5;10
Operaciones:11 Positivas:0 Negativas:0 
Ventas: False:11 
Totales por tipo de ventas
False :  8.03 %
MaxProfit:1.77% MinProfit:-0.08%
SinVentas: 8.03 StopSinVentas: -46.17
ResultCurrency: -230.83

## Mejores resultados

Estrategia: A Type: SHORT Leverage: 5 Resultado en USDT: -16.34$ Tiempo: -24 hours  Configuración actual [Si]
min_profit_sell:0.5 stoploss:-1.0 follow_losses:-0.5 stoptrailing:0;1;0.2,2;1;1,3;2;2,5;2;3,7;4;4,10;3;100
Operaciones:11 Positivas:0 Negativas:0 
Ventas: False:11 
Totales por tipo de ventas
False :  8.03 %
MaxProfit:1.77% MinProfit:-0.08%
SinVentas: 8.03 StopSinVentas: -3.27
MaxProfitStrategies: 1.77
MinProfitStrategies: -0.08
```

Tener en cuenta que muchas operaciones terminan en falso por no tener
información de mercado más alla de la hora actual.

Los resultados obtenidos para estas operaciones no son exactos ya que al no
tener la operación cerrada recogemos resultado el stoploss del momento, dando
resultados más negativos de lo que sería realmente, pero nos sirve como
referencia ya que un giro brusco del mercado puede darse perfectamente.

Opcionalmente podemos añadir -vv para ver los detalles de cada operación
sumilada.

## Comparar resultados con diferentes seáles de entrada

Ejemplo:

Añadimos señal de entrada para test.

```
$ cat strategies/A/signals.csv 
5,SHORT,[precios:>:50000] and [precios:>:50000] and [precios:>:5000] and [precios:>:500] and [precios:<:50]
5,TSHORT,[precios:>:50000] and [precios:>:50000] and [precios:>:5000] and [precios:<:600] and [precios:<:30]
5,CLOSE,[precios:>:600] and [precios:>:60] and [macd:>:60]
```

Importamos estargegia:

```
$ ./src/strategies.py -i A
Ya tenemos una estrategia con la misma marca
[R]emplazar [I]mportar con otra marca: R
['Short 15m', 'A', 'Entramos con precios arriba esperando que bajen', 'SHORT', '5', '15m']
eid: 5
Estrategia [A] importada correctamente.

[5]   A Type: SHORT[5] Name: Short 15m

Entramos con precios arriba esperando que bajen

Signals:

[40]  SHORT [precios:>:50000] and [precios:>:50000] and [precios:>:5000] and [precios:>:500] and [precios:<:50]
[41]  TSHORT [precios:>:50000] and [precios:>:50000] and [precios:>:5000] and [precios:<:600] and [precios:<:30]
[42]  CLOSE [precios:>:600] and [precios:>:60] and [macd:>:60]

Config:
compras_activadas:1 ventas_activadas:1 sell_with_loss:1 min_profit_sell:1.0 stoploss:-2.0 follow_losses:-1.0 stoptrailing:0;2;0.2,1;0.5;30 amount_buy:100.0 max_trades:2 max_losses:1
```

Ejecutamos test:


```
$ ./src/reports.py -rd '-12 hours' -st A -ts


Estrategia: A time:  -12 hours

[5]   A Type: SHORT[5] Name: Short 15m

Entramos con precios arriba esperando que bajen

Signals:

[40]  SHORT [precios:>:50000] and [precios:>:50000] and [precios:>:5000] and [precios:>:500] and [precios:<:50]
[41]  TSHORT [precios:>:50000] and [precios:>:50000] and [precios:>:5000] and [precios:<:600] and [precios:<:30]
[42]  CLOSE [precios:>:600] and [precios:>:60] and [macd:>:60]

Config:
compras_activadas:1 ventas_activadas:1 sell_with_loss:1 min_profit_sell:1.0 stoploss:-2.0 follow_losses:-1.0 stoptrailing:0;2;0.2,1;0.5;30 amount_buy:100.0 max_trades:2 max_losses:1

[SHORT][precios:>:50000] and [precios:>:50000] and [precios:>:5000] and [precios:>:500] and [precios:<:50]


[Config]min_profit_sell:1.0 stoploss:-2.0 follow_losses:-1.0 stoptrailing:0;2;0.2,1;0.5;30


Estrategia: A Type: SHORT Leverage: 5 Resultado en USDT: -14.08$ Tiempo: -12 hours 
Señal de entrada: [precios:>:50000] and [precios:>:50000] and [precios:>:5000] and [precios:>:500] and [precios:<:50]
Señal de cierre: False
min_profit_sell:1.0 stoploss:-2.0 follow_losses:-1.0 stoptrailing:0;2;0.2,1;0.5;30
Operaciones:10 Positivas:5 Negativas:5 
Ventas: False:5% StopLossTrade:2% TakeProfit:3% 
Totales por tipo de ventas
 False:2.7%
 StopLossTrade:-1.82%
 TakeProfit:3.42%
Media general:0.16%
MaxProfit:1.52% MinProfit:-2.82%
SinVentas: 2.70 StopSinVentas: -5.10

[TSHORT][precios:>:50000] and [precios:>:50000] and [precios:>:5000] and [precios:<:600] and [precios:<:30]


[Config]min_profit_sell:1.0 stoploss:-2.0 follow_losses:-1.0 stoptrailing:0;2;0.2,1;0.5;30


Estrategia: A Type: SHORT Leverage: 5 Resultado en USDT: -3.13$ Tiempo: -12 hours 
Señal de entrada: [precios:>:50000] and [precios:>:50000] and [precios:>:5000] and [precios:<:600] and [precios:<:30]
Señal de cierre: False
min_profit_sell:1.0 stoploss:-2.0 follow_losses:-1.0 stoptrailing:0;2;0.2,1;0.5;30
Operaciones:1 Positivas:0 Negativas:1 
Ventas: FollowLosses:1% 
Totales por tipo de ventas
 FollowLosses:-1.6%
Media general:-1.6%
MaxProfit:0% MinProfit:-3.24%
SinVentas: 0.00 StopSinVentas: 0.00

## Mejores MinProfit

Estrategia: A Type: SHORT Leverage: 5 Resultado en USDT: -3.13$ Tiempo: -12 hours  Configuración actual [Si]
Señal de entrada: [TSHORT][precios:>:50000] and [precios:>:50000] and [precios:>:5000] and [precios:<:600] and [precios:<:30]
Señal de cierre: False
min_profit_sell:1.0 stoploss:-2.0 follow_losses:-1.0 stoptrailing:0;2;0.2,1;0.5;30
Operaciones:1 Positivas:0 Negativas:1 
Ventas: FollowLosses:1% 
Totales por tipo de ventas
 FollowLosses:-1.6%
Media general:-1.6%
MaxProfit:0% MinProfit:-3.24%
SinVentas: 0.00 StopSinVentas: 0.00
MinProfitStrategies: -3.24%

## MaxPositivas

Estrategia: A Type: SHORT Leverage: 5 Resultado en USDT: -14.08$ Tiempo: -12 hours  Configuración actual [Si]
Señal de entrada: [SHORT][precios:>:50000] and [precios:>:50000] and [precios:>:5000] and [precios:>:500] and [precios:<:50]
Señal de cierre: False
min_profit_sell:1.0 stoploss:-2.0 follow_losses:-1.0 stoptrailing:0;2;0.2,1;0.5;30
Operaciones:10 Positivas:5 Negativas:5 
Ventas: False:5% StopLossTrade:2% TakeProfit:3% 
Totales por tipo de ventas
 False:2.7%
 StopLossTrade:-1.82%
 TakeProfit:3.42%
Media general:0.16%
MaxProfit:1.52% MinProfit:-2.82%
SinVentas: 2.70 StopSinVentas: -5.10
MaxProfitStrategies: 1.52%
MinProfitStrategies: -2.82%

## Mejores resultados

Estrategia: A Type: SHORT Leverage: 5 Resultado en USDT: -3.13$ Tiempo: -12 hours  Configuración actual [Si]
Señal de entrada: [TSHORT][precios:>:50000] and [precios:>:50000] and [precios:>:5000] and [precios:<:600] and [precios:<:30]
Señal de cierre: False
min_profit_sell:1.0 stoploss:-2.0 follow_losses:-1.0 stoptrailing:0;2;0.2,1;0.5;30
Operaciones:1 Positivas:0 Negativas:1 
Ventas: FollowLosses:1% 
Totales por tipo de ventas
 FollowLosses:-1.6%
Media general:-1.6%
MaxProfit:0% MinProfit:-3.24%
SinVentas: 0.00 StopSinVentas: 0.00
MinProfitStrategies: -3.24%

```
