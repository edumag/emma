# Develop

## Desarrollo basado en ccxt

https://github.com/ccxt/ccxt

## Instalación con venv

### Ubuntu or debian based.

#### python 3.8 in ubuntu 18.04

sudo apt install software-properties-common
sudo add-apt-repository ppa:deadsnakes/ppa
sudo apt-get install python3.8 python3.8-venv

#### python 3.8 in ubuntu 20.04

sudo apt install python3.8-venv

### App

git clone git@gitlab.com:edumag/emma.git
cd emma
python3.8 -m venv env
source env/bin/activate
python -m pip install --upgrade pip
pip install -r requirements.txt


## Ejemplos en python

https://github.com/ccxt/ccxt/tree/master/examples/py


## Modificaciones desde el servidor

Para evitar tener que poner la contraseña cada vez que actualizamos el código.

```
ssh-agent
ssh-add ~/.ssh/id_rsa_gitlab

## Bajar base de datos para analizar datos

Ejemplo:

```
scp server_ip:/home/ubuntu/emma/data.db ./
```

## pytest

```
pytest -v -s
```

### Deploy con git.

```
$ cat .git/hooks/pre-push
./scripts/tests
```
## Depurar

https://docs.python.org/es/3/library/pdb.html

Importar pdb en el fichero que deseamos depurar.

```
import pdb
```

Añadir punto de ruptura en la linea de código que queramos inspeccionar.

```
pdb.set_trace()
