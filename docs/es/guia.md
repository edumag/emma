# Guía de usuario

## Inicio

Para comenzar necesitaremos mercado.py que se encarga de guardar la información del mercado en la base de datos y de leer las señales de las estrategias.

En caso de que una estrategia de señales de compra es el que ejecuta las compras.

Por otro lado trades.py hace el seguimiento de las operaciones abiertas y se encarga de cerrarlas en caso de que la estrategia le de señal de cierre.

## Operaciones manuales

### Compra de una moneda.

#### Compra en SPOT

$ ./src/mercado.py --open BCH -ad 200 --type SPOT

```
200.0$
buy: BCH/USDT Amount: 0.65552 200.0$ Price: 305.1
```

#### Compra en futuros

$ ./src/mercado.py --open LTC -ad 200 -tp SHORT -lv 5

```
# Orden en futuros
symbol: LTC/USDT
type: market
price: 100.28
amount: 1.994
amount_dolars: 200
side: SELL
leverage: 1

Confirmar [S/N]: S
```

### Ventas o cierres.

$ ./src/mercado.py --close "BCH BTC"

Cerramos todas las operaciones de las monedas que pasamos

$ ./src/mercado.py --close 125

Tambien podemos especificar el identificador de la operación.



### Ejecutar

Ver operaciones en curso.

```
./src/trades.py
```

Ver estado del mercado.

```
./src/mercado.py
```

### Interpretar tendencia

Ejemplo:

    Symbol   Prec. Tend. MACD  LastPrice Status
    BTC/USDT 34444 54544 80222 54125.23  SELL

#### Precios

Indica la diferencia entre el precio actual y el precio de cierre la vela anterior.

#### Tendencia

Indica la diferencia entre la posición de la media móvil de 100 actual y el de la vela anterior.

#### Volumen

Mostramos promedio del movimiento del volumen respecto a la media.

#### ADX

#### MACD

##### Códigos:

- 9: El histograma acaba de empezar a subir desde abajo.
- 8: Lleva subiendo desde abajo entre 2 periodos y 4.
- 7: El histograma acaba de empezar a subir desde arriba.
- 6: El histograma sube desde arriba.
- 5: Ni sube ni baja.
- 4: Lleva subiendo desde arriba desde hace más de 5 periodos.
- 3: Comienza bajada desde abajo ( 1 periodo ).
- 2: Continua bajada desde abajo ( más de 1 periodo ).
- 1: Continua bajada desde arriba ( más de 1 periodo ).
- 0: Comienza bajada desde arriba ( 1 periodo ).

##### OPEN

Señal de apertura.

##### CLOSE+

Cierre si estamos en positivo, si estamos en negativo no vende.

##### CLOSE o CLOSE-

Venta si estamos en negativo o positivo.

##### HOLD

Mantener operación.

## Valorar ventas

Es importante conocer todos los factores que se tienen en cuenta a la
hora de determinar la venta de una moneda.

1. Si ventas_activadas en la estrategia de la operación es negativo no se vende.
2. Si ventas_activadas en la configuración general es negativo no se vende.
3. Si la operación tiene su propio stoploss y es inferior al profit actual vendemos. [StopLossTrainig]
4. Si no tenemos stoploss propio, sell_with_loss de estrategia esta activado y el stoploss es mayor a profit vendemos. [StopLoss]
5. Si no tenemos stoploss propio, follow_losses de estrategia mayor a profit y status de operación es SELL- y sell_with_loss positivo, vendemos [FollowLosses]
6. Si min_profit_sell de estrategia es menor a profit, status SELL+ o SELL- vendemos. [TakeProfit]


## Valorar compras

Se tiene en cuenta la configuración general como las de cada estrategia.

### max_trades

En caso de tener operaciones abiertas sin un stoploss en positivo, se tendrán
en cuenta para no superar en numero especificado.

### max_losses

Tanto a nivel general como de cada estrategia se puede especificar el numero
máximo que permitimos de operaciones en negativo.

### Valoración de mercado

#### LAST_TWO_SIZE_VALUE

Observamos en las operaciones en test que se agrupan las operaciones perdedoras y 
ganadoras, es decir que podemos tener una lista de 10 operaciones seguidas
perdedoras y después otras 10 de ganadoras.

Este valor nos indica si las dos últimas operaciones de la estrategia han sido
ganadoras o no, en caso positivo hay más probabilidades de que la siguiente sea
positiva.

## Informes

### Últimas operaciones

```
./src/db.py -t all / -tt all para test.
```

```
Id   Symbol     DateSell        Amount   Amount $      Price  Profit  Result DateBuy    MaxPr. MinProf.
1    XVS/USDT   18/05 19:10      9.878     1000.0    101.235  25.21% 252.07$ 18/05 17:24 42.3 -0.55
2    BTC/USDT   18/05 19:19      0.023     999.97   43401.49  -0.02%  -0.19$ 18/05 19:16 0.07 0.07
6    SOL/USDT   18/05 19:46     18.005     1000.0     55.541   1.24%  12.37$ 18/05 19:38 0.64 -0.3
Total profit. 26.42%, Total resultado: 264.25$

Día         Profit AVG  Result
2021-05-18       8.81% 264.24$
```

### Operaciones abiertas

```
./src/db.py -rt / -rtt para test.
```

```
@todo
```

### Detalle de trade

```
./src/db.py -td [id] / -tdt para test.
```

```
$ ./src/db.py -td 2
Id   Symbol         Amount   Amount $  Profit  Result   MaxPr. MinProf.
2    MATIC/USDT      517.1     999.83   2.15%  21.52$   -99.62   -99.63

Status   Price Tende Volum ADX   MACD  Date        Price
SELL- C  66544 75555 11112 56700 48541 18/05 04:15 1.84114    -4.78
SELL+ C  66545 75555 11111 56700 48540 18/05 04:21 1.86       -3.8
SELL- C  66544 75555 11222 56700 48540 18/05 04:23 1.83873    -4.9
ADX- C   77555 75555 11221 56705 48546 18/05 04:25 1.88435    -2.54
SELL+ C  66545 75555 12222 56705 48540 18/05 04:30 1.85571    -4.03
ADX- C   77555 75555 12211 56700 48546 18/05 04:31 1.87976    -2.78
ADX+ C   88655 75555 12222 56866 48545 18/05 04:38 1.92504    -0.44
ADX- C   88655 75555 12211 57800 48545 18/05 04:46 1.9421     0.44
SELL- C  88654 75555 12221 57950 45541 18/05 04:50 1.95769    1.25
ADX+ C   88755 76555 12222 57950 45544 18/05 04:52 1.982      2.51
SELL- C  88654 75555 12222 57950 45541 18/05 04:54 1.96414    1.58
ADX+ C   88755 75555 12222 57950 45541 18/05 04:59 1.96941    1.86
SELL- C  88754 75555 12221 57950 45540 18/05 05:00 1.96736    1.75
SELL- C  88544 75555 12211 57000 45413 18/05 05:09 1.93354    Manual
SELL+ C  88445 75555 12121 57000 48410 18/05 05:10 1.9367     0.16
SELL- C  88444 75555 12121 57000 48410 18/05 05:11 1.92891    -0.24
ADX- C   88455 75555 12111 57000 45400 18/05 05:16 1.94134    0.4
SELL- C  88454 75555 12111 57000 45402 18/05 05:20 1.94903    0.8
ADX- C   88455 75555 12111 57000 45402 18/05 05:20 1.95492    1.11
SELL- C  88454 75555 12111 57000 45402 18/05 05:20 1.94866    0.78
ADX- C   88455 75555 12111 57000 45402 18/05 05:24 1.94996    0.85
SELL- C  88454 75555 12111 57000 45402 18/05 05:25 1.94818    0.76
ADX- C   88455 75555 12111 57000 45402 18/05 05:30 1.94218    0.45
ADX+ C   88555 75555 12111 57055 45409 18/05 05:34 1.97019    1.9
SELL- C  88454 75555 12111 57050 45403 18/05 05:35 1.96157    1.45
ADX+ C   88455 75555 12111 57050 45402 18/05 05:40 1.96505    1.63
SELL- C  88544 76555 12211 57000 45418 18/05 05:45 1.98118    2.46
SELL- C   18/05 05:45 1.97516    Manual
ADX- C   88555 76555 12211 57000 45418 18/05 05:45 1.99141    2.99
SELL- C  88554 76555 12211 57000 45413 18/05 05:50 1.98973    2.91
SELL+ C  88445 75555 12221 57000 45412 18/05 05:55 1.96207    1.48
ADX- C   88555 76555 12221 57000 45419 18/05 05:57 1.98515    2.67
ADX+ C   99555 76555 12222 68556 45456 18/05 05:58 2.03187    5.09

```

### Mostrar coincidencias en los datos del mercado.

```
$ ./src/reports.py -sr "[precios:<:500000] and [precios:>:50000] and [precios:>:5000]  and [precios:>:500]  and [precios:>:50]"
DATABASE: db/data.db
Id       Symbol     Date                    Price     Profit Status  Prices Tenden Volume ADXs   MACDs
11428    BCH/USDT   2022-04-13 09:04:36        304.4                0 HOLD  A 365555 324664 555555 385585 860496
11570    BCH/USDT   2022-04-13 09:13:34        304.3                0 HOLD  A 355556 324664 555557 385509 860497
11614    BCH/USDT   2022-04-13 09:16:14        304.5                0 HOLD  A 365666 324664 555555 385519 860486
11639    BCH/USDT   2022-04-13 09:17:50        304.8                0 HOLD  A 466664 324664 555553 385592 860486
11664    BCH/USDT   2022-04-13 09:19:27        304.5                0 HOLD  A 365663 324664 555554 385591 860481
11689    BCH/USDT   2022-04-13 09:21:02        304.6                0 HOLD  A 366655 324664 555555 385555 860480
11741    BCH/USDT   2022-04-13 09:24:17        304.6                0 HOLD  A 366654 324664 555555 385553 860481
16387    SOL/USDT   2022-04-13 14:01:12       101.06                0 HOLD  A 255554 434444 555555 245212 404334
16390    BTC/USDT   2022-04-13 14:02:23     39766.02                0 SCLOS A 366665 434444 555565 535559 464938
```

### Mostrar resultados de un patrón.

```
 $ ./src/reports.py -vr "[precios:<:500000] and [precios:>:50000] and [precios:>:5000]  and [precios:>:500]  and [precios:>:50]"

# View results
 
  cast(mon_precios as integer ) < 500000 
 and cast(substr(mon_precios, -5) as integer) > 50000 
 and cast(substr(mon_precios, -4) as integer) > 5000 
 and cast(substr(mon_precios, -3) as integer) > 500 
 and cast(substr(mon_precios, -2) as integer) > 50 

Id       Symbol     Date                    Price     Profit Status  Prices Tenden Volume ADXs   MACDs
31477    ADA/USDT   2022-04-14 02:55:24        0.974                0 LONG  D 355666 334665 555555 432255 860987
11741    BCH/USDT   2022-04-13 09:24:17        304.6                0 HOLD  A 366654 324664 555555 385553 860481
17479    BTC/USDT   2022-04-13 15:37:48     40084.14                0 SCLOS A 466666 434446 555668 515554 966666
35691    ETH/USDT   2022-04-14 06:04:05      3117.87                0 HOLD  J 455555 646664 555555 543555 861981
17688    SOL/USDT   2022-04-13 15:47:37       103.16                0 SCLOS A 477663 434446 555564 215771 479661

 ## pair: ADA/USDT date_search: 2022-04-14 02:55:24 buy_price: 0.974 

31477    ADA/USDT   2022-04-14 02:55:24        0.974    0.0% SCLOS A 355666 334665 555555 432255 860987 MaxPr[0] Stop[-0.6] BUYHERE
31481    ADA/USDT   2022-04-14 02:55:30        0.974    0.0% SCLOS A 335666 334665 555555 432255 860987 MaxPr[0] Stop[-0.6] 
31486    ADA/USDT   2022-04-14 02:55:37        0.974    0.0% SCLOS A 555666 334665 555555 432255 860987 MaxPr[0] Stop[-0.6] 
31491    ADA/USDT   2022-04-14 02:55:47        0.974    0.0% SCLOS A 555666 334665 555555 432255 860987 MaxPr[0] Stop[-0.6] 
...
17996    SOL/USDT   2022-04-13 16:01:53        104.2   1.01% SCLOS A 686665 434456 555567 215228 476664 MaxPr[1.13] Stop[0.1] TakeProfit
17999    SOL/USDT   2022-04-13 16:02:06       104.24   1.05% SCLOS A 686665 434466 555565 215213 476664 MaxPr[1.13] Stop[0.1] TakeProfit
18002    SOL/USDT   2022-04-13 16:02:16       104.15   0.96% SCLOS A 686664 434466 555565 215213 476664 MaxPr[1.13] Stop[0.1] 
18005    SOL/USDT   2022-04-13 16:02:26       104.12   0.93% SCLOS A 686664 434456 555565 215213 476614 MaxPr[1.13] Stop[0.1] 
18008    SOL/USDT   2022-04-13 16:02:35       104.04   0.85% SCLOS A 675553 434456 555564 215212 476614 MaxPr[1.13] Stop[0.1] 
18011    SOL/USDT   2022-04-13 16:02:45       103.97   0.79% SCLOS A 675553 434456 555543 215211 476614 MaxPr[1.13] Stop[0.1] 
18016    SOL/USDT   2022-04-13 16:02:52       103.88    0.7% SCLOS A 674443 434456 555542 215210 478614 MaxPr[1.13] Stop[0.1] 
18019    SOL/USDT   2022-04-13 16:02:57       103.88    0.7% SCLOS A 674443 434456 555542 215210 478614 MaxPr[1.13] Stop[0.1] 
18022    SOL/USDT   2022-04-13 16:03:02       103.82   0.64% SCLOS A 673334 434456 555545 215213 478614 MaxPr[1.13] Stop[0.1] 
18026    SOL/USDT   2022-04-13 16:03:13        103.6   0.43% SCLOS A 673333 434456 555545 215111 478614 MaxPr[1.13] Stop[0.1] 
18029    SOL/USDT   2022-04-13 16:03:26        103.3   0.14% SCLOS A 673333 434456 555533 215100 478614 MaxPr[1.13] Stop[0.1] 
18032    SOL/USDT   2022-04-13 16:03:34       103.25   0.09% SCLOS A 573333 434456 555532 215100 478614 MaxPr[1.13] Stop[0.1] StopLossTrade
18035    SOL/USDT   2022-04-13 16:03:44       103.34   0.17% SCLOS A 673333 434456 555531 215100 478614 MaxPr[1.13] Stop[0.1] 
18038    SOL/USDT   2022-04-13 16:03:52       103.41   0.24% SCLOS A 673333 434456 555531 215100 478614 MaxPr[1.13] Stop[0.1] 


# Patron:  [precios:<:500000] and [precios:>:50000] and [precios:>:5000]  and [precios:>:500]  and [precios:>:50]
## Config:

compras_activadas:1 ventas_activadas:1 sell_with_loss:1 min_profit_sell:1.0 stoploss:-1.0 follow_losses:-0.5 stoptrailing:0;0.6;0.1,2;1;1 amount_buy:100.0 max_trades:3 max_losses:3'


## Totales

### Tiempo: -7 days
### Estrategia: A
### Operaciones: 5
### trades: 5
### positivas: 4
### negativas: 0
### resultado: 4.57%

## Ventas
### TakeProfit :  4
### False :  1

## Totales por tipo de ventas
### TakeProfit :  4.57 %
### False :  -0.37 %

## Media por tipo de ventas
### TakeProfit :  0.87 %
### False :  -2.68 %
### Media general: 1.14
```

Ejemplos con salida a formato html para mejorar su lectura:

```
edu@edumag:~/desarrollo/emma$ ./src/reports.py -vr "[volumenes:>:600] and [tendencias:>:600] and [macd:>:900] and [macd:>:60]" |  aha  > /tmp/1.html
```

```
./src/reports.py -st H -ts -rd '-15 days' | aha > /tmp/A.html && brave-browser /tmp/A.html
```

### Mostrar evolución simulando compra en mercado.

```
$ ./src/reports.py -id 17688
DATABASE: db/data.db
Id       Symbol     Date                    Price     Profit Status  Prices Tenden Volume ADXs   MACDs
13878    SOL/USDT   2022-04-13 11:47:54       103.14  -0.02% HOLD  A 433666 434644 555555 325559 461498
13884    SOL/USDT   2022-04-13 11:49:33       102.97  -0.18% HOLD  A 332665 434644 555555 325551 461446
13913    SOL/USDT   2022-04-13 11:50:54        103.2   0.04% HOLD  A 433666 434644 555555 325559 461486
13945    SOL/USDT   2022-04-13 11:52:22       103.17   0.01% HOLD  A 433664 434644 555555 325553 461486
13981    SOL/USDT   2022-04-13 11:53:53       103.35   0.18% SCLOS A 633665 434644 555556 325558 461986
13989    SOL/USDT   2022-04-13 11:54:10       103.44   0.27% BUY   G 633665 434644 555555 325554 461986
...
```


### Mejores resultados

Mostrar mejores resultados según los datos del mercado.

Es necesario pasar el mínimo en porcentaje que debe buscar.

```
$ ./src/reports.py -br 1
DATABASE: db/data.db
977444 334766 579255 983135 866248 HOLD  A 988776 334766 579999 998899 866766 SCLOS A   1.15% BCH/USDT   17853  2022-04-13 15:55:23
986635 336766 585655 992321 866244 HOLD  A 997776 336766 587997 998990 866766 SCLOS A    1.8% BCH/USDT   19561  2022-04-13 17:22:25
744444 347666 555555 343212 814614 SCLOS A 766776 347666 555789 366899 867676 SCLOS A   1.09% BCH/USDT   35709  2022-04-14 06:04:57
973333 236666 564555 923335 962444 HOLD  A 987777 336666 567999 988995 967776 SCLOS A   2.09% C98/USDT   4648  2022-04-12 19:45:27
923333 236644 535455 972535 960494 HOLD  A 936666 236644 535765 972515 960496 HOLD  A    1.0% C98/USDT   5007  2022-04-12 21:05:32
777634 236666 556745 578913 866661 SCLOS A 888776 236666 556999 589989 866666 SCLOS A   1.69% C98/USDT   10787  2022-04-13 06:22:28
888766 236666 556955 589931 866666 SCLOS A 999987 236666 569999 999998 866666 SCLOS A   3.39% C98/USDT   10799  2022-04-13 06:25:15
999983 236666 569994 999992 866661 SCLOS A 999987 236666 569999 999994 866667 SCLOS A    1.2% C98/USDT   10811  2022-04-13 06:28:18
```

## Probar estrategias

```
$ ./src/reports.py --strategy_test D  |  aha  > /tmp/1.html && brave-browser /tmp/1.html
DATABASE: db/data.db

Estrategia: D time:  -2 hours

# View results
 
  cast(substr(mon_volumenes, -3) as integer) > 500 
 and cast(substr(mon_tendencias, -3) as integer) > 600 
 and cast(substr(mon_macd, -3) as integer) > 900 
 and cast(substr(mon_macd, -2) as integer) > 60 

Id       Symbol     Date                    Price     Profit Status  Prices Tenden Volume ADXs   MACDs
40318    ADA/USDT   2022-04-14 09:30:00        0.971                0 LONG  D 336655 334646 555555 622510 864961
40317    BCH/USDT   2022-04-14 09:29:55        343.8                0 LONG  D 626645 347645 545555 632953 814962
39955    BTC/USDT   2022-04-14 09:14:43     41192.44                0 SCLOS A 635555 436644 555555 533515 864986
40675    C98/USDT   2022-04-14 09:44:54        1.625                0 HOLD  G 536666 266644 555555 521557 814966
38915    ETH/USDT   2022-04-14 08:30:00      3107.42                0 HOLD  J 334655 646644 555555 522555 864966
40314    SOL/USDT   2022-04-14 09:29:43       106.14                0 LONG  D 763645 434664 555555 845155 861981

 ## pair: ADA/USDT date_search: 2022-04-14 09:30:00 buy_price: 0.971 

40318    ADA/USDT   2022-04-14 09:30:00        0.971    0.0% LONG  D 336655 334646 555555 622510 864961 MaxPr[0] Stop[-0.6] BUYHERE
40323    ADA/USDT   2022-04-14 09:30:08        0.972    0.1% HOLD  D 336666 334646 555555 622532 864867 MaxPr[0.1] Stop[-0.5] 
40327    ADA/USDT   2022-04-14 09:30:14        0.971    0.0% HOLD  D 336555 334646 555555 622532 864862 MaxPr[0.1] Stop[-0.5] 
40334    ADA/USDT   2022-04-14 09:30:28        0.972    0.1% HOLD  D 336666 334646 555555 622532 864867 MaxPr[0.1] Stop[-0.5]
...
40736    SOL/USDT   2022-04-14 09:47:33       107.01   0.82% HOLD  D 776664 434666 555555 845855 867866 MaxPr[0.88] Stop[0.1] 
40741    SOL/USDT   2022-04-14 09:47:51          107   0.81% HOLD  D 776664 434666 555555 845855 867866 MaxPr[0.88] Stop[0.1] 
40748    SOL/USDT   2022-04-14 09:48:09       106.95   0.76% HOLD  D 776665 434666 555555 845854 867860 MaxPr[0.88] Stop[0.1] 
40751    SOL/USDT   2022-04-14 09:48:13       106.91   0.73% HOLD  D 776664 434666 555555 845853 861860 MaxPr[0.88] Stop[0.1] 
40757    SOL/USDT   2022-04-14 09:48:27       106.92   0.73% HOLD  D 776665 434666 555555 845853 861860 MaxPr[0.88] Stop[0.1] 

# Patron:  [volumenes:>:500] and [tendencias:>:600] and [macd:>:900] and [macd:>:60]
## Config:

compras_activadas:1 ventas_activadas:1 sell_with_loss:1 min_profit_sell:1.0 stoploss:-1.0 follow_losses:-0.5 stoptrailing:0;0.6;0.1,2;1;1 amount_buy:100.0 max_trades:3 max_losses:3'


## Totales

### Tiempo: -2 hours
### Estrategia: D
### Operaciones: 6
### trades: 6
### positivas: 0
### negativas: 0
### resultado: 0%

## Ventas
### False :  6

## Totales por tipo de ventas
### False :  2.25 %

## Media por tipo de ventas
### False :  2.67 %

Strategy: D Profit: 0% Time: -2 hours
Positivas:0 Negativas:0
Tipo de ventas: (False:6 )
Total en tipo de ventas: (False:2.25% )
min_profit_sell:1.0 stoploss:-1.0 follow_losses:-0.5 stoptrailing:0;0.6;0.1,2;1;1
Media tipos: (False:2.67% )
Media general:0

## Mejores resultados

### Estrategia:D Profit:0% Actual:True time:-2 hours
Positivas:0 Negativas:0
Tipo de ventas: (False:6 )
Total en tipo de ventas: (False:2.25% )
min_profit_sell:1.0 stoploss:-1.0 follow_losses:-0.5 stoptrailing:0;0.6;0.1,2;1;1
Media tipos: (False:2.67% )
Media general:0
```

## ./src/reports.py -sa

Automatizar activación/desactivación de estrategias según resultados.

```
./src/reports.py -sa

Estrategia: G time:  -3 hours

Strategy: G Profit: 0% Time: -3 hours
Positivas:0 Negativas:0
Tipo de ventas: (False:6 )
Total en tipo de ventas: (False:3.19% )
min_profit_sell:2.0 stoploss:-2.0 follow_losses:-0.5 stoptrailing:0;0.6;0.1,2;1;1
Media tipos: (False:1.88% )
Media general:0

## Mejores resultados

### Estrategia:G Profit:0% Actual:True time:-3 hours
Positivas:0 Negativas:0
Tipo de ventas: (False:6 )
Total en tipo de ventas: (False:3.19% )
min_profit_sell:2.0 stoploss:-2.0 follow_losses:-0.5 stoptrailing:0;0.6;0.1,2;1;1
Media tipos: (False:1.88% )
Media general:0

Estrategia: E time:  -10 hours
...
### Estrategia:J Profit:0% Actual:True time:-10 days
Positivas:0 Negativas:0
Tipo de ventas: (False:1 )
Total en tipo de ventas: (False:0.0% )
min_profit_sell:2.0 stoploss:-2.0 follow_losses:-0.5 stoptrailing:0;5;10
Media tipos: (False:0 )
Media general:0

## Activación / desactivación de compras


### Estrategias activas en configuración general: A B C D E F G H I J
Estrategia: A

compras_activadas:0 ventas_activadas:1 sell_with_loss:1 min_profit_sell:1.0 stoploss:-1.0 follow_losses:-0.5 stoptrailing:0;0.6;0.1,2;1;1 amount_buy:100.0 max_trades:3 max_losses:3'

Estrategia: B

compras_activadas:0 ventas_activadas:1 sell_with_loss:1 min_profit_sell:1.0 stoploss:-1.0 follow_losses:-0.5 stoptrailing:0;2;0.2,3;1;5 amount_buy:100.0 max_trades:3 max_losses:3'
...
Estrategia: J

compras_activadas:1 ventas_activadas:1 sell_with_loss:1 min_profit_sell:2.0 stoploss:-2.0 follow_losses:-0.5 stoptrailing:0;5;10 amount_buy:100.0 max_trades:3 max_losses:3'
```

## Estudiar estrategias

### Revisar resultados de estrategias con diferentes configuraciones.

$ ./src/reports.py -ce -mc -tc

-ce Compara estrategias.
-mc Make changes, cambiar configuración.
-tc Test de estrategia con diferentes configuraciones.

Nota:
  No es recomendado utilizar el cambio automático de configuración, los test
  con diferentes valores son de orientación.


### Revisar estrategia en detalle.

./src/reports.py -st A -tc -rd '-3 days'

o

./src/reports.py -st A -tc -ts -vv | aha > /tmp/1.html && brave-browser /tmp/1.html

Para mostrar los resultados en html

#### Ejemplos

##### $ ./src/reports.py -st A -rd '-2 hours'

```
Estrategia: A time:  -2 hours

[9]   A Type: SHORT[5] Name: Short 1h

Entramos con precios arriba esperando que bajen

Signals:

[313] SHORT [precios:>:50000] and [precios:>:50000] and [precios:>:5000] and [precios:>:500] and [precios:<:50]

Config:
compras_activadas:1 ventas_activadas:1 sell_with_loss:1 min_profit_sell:1.0 stoploss:-2.0 follow_losses:-1.0 stoptrailing:0;5;0.5,3;3;6,6;4;10,10;6;90 amount_buy:100.0 max_trades:3 max_losses:3

Estrategia: A Resultado: 0% Tiempo: -2 hours
min_profit_sell:1.0 stoploss:-2.0 follow_losses:-1.0 stoptrailing:0;5;0.5,3;3;6,6;4;10,10;6;90
Operaciones:7 Positivas:0 Negativas:0
Ventas: False:7
Totales por tipo de ventas
False :  15.56 %
MaxProfit:4.69% MinProfit:-0.35%

## Mejores resultados

Estrategia: A Resultado: 0% Tiempo: -2 hours  Configuración actual [Si]
min_profit_sell:1.0 stoploss:-2.0 follow_losses:-1.0 stoptrailing:0;5;0.5,3;3;6,6;4;10,10;6;90
Operaciones:7 Positivas:0 Negativas:0
Ventas: False:7
Totales por tipo de ventas
False :  15.56 %
MaxProfit:4.69% MinProfit:-0.35%
```

Nos muestra los resultados con los datos optenidos del mercado de las 2 horas
anteriores a la actual.

##### $ ./src/reports.py -st A -tc -rd '-2 hours'

```
Estrategia: A time:  -2 hours

[9]   A Type: SHORT[5] Name: Short 1h

Entramos con precios arriba esperando que bajen

Signals:

[313] SHORT [precios:>:50000] and [precios:>:50000] and [precios:>:5000] and [precios:>:500] and [precios:<:50]

Config:
compras_activadas:1 ventas_activadas:1 sell_with_loss:1 min_profit_sell:1.0 stoploss:-2.0 follow_losses:-1.0 stoptrailing:0;5;0.5,3;3;6,6;4;10,10;6;90 amount_buy:100.0 max_trades:3 max_losses:3

Estrategia: A Resultado: 0% Tiempo: -2 hours
min_profit_sell:1.0 stoploss:-2.0 follow_losses:-1.0 stoptrailing:0;5;0.5,3;3;6,6;4;10,10;6;90
Operaciones:7 Positivas:0 Negativas:0
Ventas: False:7
Totales por tipo de ventas
False :  15.56 %
MaxProfit:4.69% MinProfit:-0.35%

Estrategia: A Resultado: 0% Tiempo: -2 hours
min_profit_sell:1.0 stoploss:-5.0 follow_losses:-1.0 stoptrailing:0;3;0.2,0;3;30
Operaciones:7 Positivas:0 Negativas:0
Ventas: False:7
Totales por tipo de ventas
False :  15.56 %
MaxProfit:4.69% MinProfit:-0.35%

Estrategia: A Resultado: 6.66% Tiempo: -2 hours
min_profit_sell:1.0 stoploss:-5.0 follow_losses:-1.0 stoptrailing:0;2;0.2,1;0.5;30
Operaciones:7 Positivas:6 Negativas:0
Ventas: StopLossTrade:6 False:1
Totales por tipo de ventas
StopLossTrade :  6.66 %
False :  1.38 %
Media general:0.95%
MaxProfit:3.25% MinProfit:-0.35%

Estrategia: A Resultado: 0% Tiempo: -2 hours
min_profit_sell:1.0 stoploss:-5.0 follow_losses:-1.0 stoptrailing:0;5;0.5,3;3;6,6;4;10,10;6;90
Operaciones:7 Positivas:0 Negativas:0
Ventas: False:7
Totales por tipo de ventas
False :  15.56 %
MaxProfit:4.69% MinProfit:-0.35%

Estrategia: A Resultado: 0% Tiempo: -2 hours
min_profit_sell:1.0 stoploss:-5.0 follow_losses:-1.0 stoptrailing:0;2;30
Operaciones:7 Positivas:0 Negativas:0
Ventas: False:7
Totales por tipo de ventas
False :  15.56 %
MaxProfit:4.69% MinProfit:-0.35%

## Mejores resultados

Estrategia: A Resultado: 6.66% Tiempo: -2 hours  Configuración actual [No]
min_profit_sell:1.0 stoploss:-5.0 follow_losses:-1.0 stoptrailing:0;2;0.2,1;0.5;30
Operaciones:7 Positivas:6 Negativas:0
Ventas: StopLossTrade:6 False:1
Totales por tipo de ventas
StopLossTrade :  6.66 %
False :  1.38 %
Media general:0.95%
MaxProfit:3.25% MinProfit:-0.35%
```

Hacemos una comparación entre diferentes configuración de estrategia.

##### ./src/reports.py -st A -rd '-2 hours' -vv

```
Result: 0 MaxProfit: 2.99 MinProfit: -0.35

 ## pair: NEAR/USDT date_search: 2022-04-22 14:38:00 buy_price: 15.57

180392   NEAR/USDT  2022-04-22 14:38:00        15.57    0.0% SHORT A 366633 964456 555555 359555 396041 MaxPr[0] Stop[-5.0] BUYHERE
180444   NEAR/USDT  2022-04-22 14:40:03        15.52   0.32% HOLD  A 333355 964446 555555 359555 396034 MaxPr[0.32] Stop[-4.68]
180499   NEAR/USDT  2022-04-22 14:42:23       15.541   0.19% HOLD  A 355465 964456 555555 359555 396038 MaxPr[0.32] Stop[-4.68]
180547   NEAR/USDT  2022-04-22 14:44:26       15.538   0.21% HOLD  A 355464 964455 555555 359555 396038 MaxPr[0.32] Stop[-4.68]
180597   NEAR/USDT  2022-04-22 14:46:29       15.537   0.21% HOLD  A 344444 964455 555555 359555 396148 MaxPr[0.32] Stop[-4.68]
180648   NEAR/USDT  2022-04-22 14:48:29       15.518   0.33% HOLD  A 333335 964444 555555 359555 396144 MaxPr[0.33] Stop[-4.67]
180698   NEAR/USDT  2022-04-22 14:50:33       15.539    0.2% HOLD  A 355455 964445 555555 359555 396148 MaxPr[0.33] Stop[-4.67]
180746   NEAR/USDT  2022-04-22 14:52:27       15.554    0.1% HOLD  A 355565 964445 555555 359555 396196 MaxPr[0.33] Stop[-4.67]
180798   NEAR/USDT  2022-04-22 14:54:37        15.54   0.19% HOLD  A 355456 964444 555555 359555 396146 MaxPr[0.33] Stop[-4.67]
180848   NEAR/USDT  2022-04-22 14:56:35       15.556   0.09% HOLD  A 366565 964445 555555 359555 396196 MaxPr[0.33] Stop[-4.67]
180900   NEAR/USDT  2022-04-22 14:58:40       15.551   0.12% HOLD  A 355565 964445 555555 359555 396197 MaxPr[0.33] Stop[-4.67]

```

Con -vv nos muestra los detalles de las operaciones.
