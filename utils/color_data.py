#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
sys.path.append('./src')
from tendencia import *

precio = 97677
tendencia = 55555
volumen = 57899
adx = 2013
macd = 44651

precio_color = color_monitor(precio, 'precios')
tendencia_color = color_monitor(tendencia, 'tendencias')
volumen_color = color_monitor(volumen, 'volumenes')
adx_color = color_monitor(adx, 'adx')
macd_color = color_monitor(macd, 'macd')

todo = color_monitor(
    str(precio) + ' ' + str(tendencia) + ' ' + str(volumen) + ' ' + str(adx) + ' ' + str(macd)
)

print('precio: ', precio_color)
print('tendencia: ', tendencia_color)
print('Volumen: ', volumen_color)
print('adx: ', adx_color)
print('macd: ', macd_color)

print('Todo: ', todo)
