import sys
sys.path.append('./src')
from base import *

stoptrailings = [
    "1;1,2;2,3;3,10;1",
    "1;1,10;1",
    "0;1,3;3,10;1",
    "0;1",
]
profits = [
    -1,
    -0.3,
    0,
    0.1,
    1,
    -1,
    1.5,
    -1.5,
    2.3,
    4.5,
    6.5,
    8.5,
    12.2
]

for stoptrailing in stoptrailings:
    stoploss_actual = False
    for profit in profits:
        stoploss_anterior = stoploss_actual
        stoploss_actual = calculo_stoptrailing(stoptrailing, profit, stoploss_actual)
        if stoploss_actual:
            stoploss_actual = round(stoploss_actual, 2)
        print("Profit: {:<4}  StopLossAnterior: {}  StopLossActual: {} StopTrailing: {}".format(profit, stoploss_anterior, stoploss_actual, stoptrailing))
