#!/bin/bash

# # Patron:  
#     [precios:<:500000]
# and [precios:<:50000]
# and [precios:<:5000]
# and [precios:<:500]
# and [precios:<:50]
# and [precios:<:5]
# 
# Con un mercado altamente bajista.
#
# ## Config:
# 
# compras_activadas: 1
# ventas_activadas: 1
# min_profit_sell: 2.0
# stoploss: -1.0
# follow_losses: -0.1
# stoptrailing: 1.0
# stoptrailing_distance: 1.0
# amount_buy: 100.0
# max_trades: 10
# max_losses: 2
# descartar_ventas: ''
## Totales
### Tiempo: -5 days
### Estrategia: C
### Patrones: 14
### trades: 14
### positivas: 7
### negativas: 7
### resultado: 1.81%
## Ventas
###  StopTrainig :  11
###  StopLoss :  3
human="
    [precios:<:500000]
and [precios:<:50000]
and [precios:<:5000]
and [precios:<:500]
and [precios:<:50]
and [precios:<:5]
"

# Tiempo: -14 days
# trades: 9
# positivas: 7
# negativas: 2
# resultado: 110.92%
# 
# DEFAULT_SESION = 'L'
# 
# "min_profit_sell:5 stoploss:-10 follow_losses:-4 stoptrailing:6"
# ./src/db.py -cf "min_profit_sell:32 stoploss:-4 follow_losses:-1 stoptrailing:6"
human="
[macd:>:900000]
and [macd:>:70000]
and [macd:>:7000]
and [macd:>:400]
and [macd:>:44]
"

# ## Config:
# compras_activadas: 0
# ventas_activadas: 0
# min_profit_sell: 2.0
# stoploss: -1.0
# follow_losses: -0.1
# stoptrailing: 1.0
# stoptrailing_distance: 1.0
# amount_buy: 100.0
# max_trades: 6
# max_losses: 2
# descartar_ventas: ''
## Totales
### Tiempo: -5 days
### Estrategia: C
### Patrones: 18
### trades: 18
### positivas: 14
### negativas: 4
### resultado: 34.68%
### Ventas
####  StopLoss :  4
####  StopTrainig :  12
####  False :  2
human="
    [precios:<:500000]
and [precios:<:50000]
and [precios:<:5000]
and [precios:<:500]
and [precios:<:50]
and [precios:<:5]
"


# # Patron:  
# [adx:>:40]
# and [macd:>:900]
# and [macd:>:50]
# ## Config:
# compras_activadas: 0
# ventas_activadas: 0
# min_profit_sell: 2.0
# stoploss: -2.0
# follow_losses: -0.1
# stoptrailing: 1.0
# stoptrailing_distance: 1.0
# amount_buy: 100.0
# max_trades: 6
# max_losses: 2
# descartar_ventas: ''
## Totales
### Tiempo: -5 days
### Estrategia: C
### Patrones: 303
### trades: 303
### positivas: 199
### negativas: 104
### resultado: 207.55%
## Ventas
###  False :  99
###  StopLoss :  35
###  StopTrainig :  169
human="
([adx:>:60] or [adx:>:600] or [volumenes:>:600] or [volumenes:>:50]) and [macd:>:700] and [macd:>:90] and [macd:>:4]
"

# Macd4h
human="[macd:>:500000] and [macd:>:90000] and [macd:>:5000] and [macd:>:500] and [macd:>:50]"

human="
([adx:>:60] or [adx:>:600] or [volumenes:>:600] or [volumenes:>:50]) and [macd:>:700] and [macd:>:90] and [macd:>:4]
"
human="
[adx:>:5000] and [macd:>:9000] and [macd:>:700] and [macd:>:75]
"
human="
[adx:>:50000] and [macd:>:500000] and [macd:>:90000] and [macd:>:5000] and [macd:>:500] and [macd:>:50]
"
human="
([adx:>:700] or [adx:>:50]) and [macd:>:700] and [macd:>:50] and [macd:>:50]
"
# ./src/reports.py -vr "${human}"
# exit

name=`date +"%M-%S"`
./src/reports.py -vr "${human}" -rd '-4 hours' | tee -a | aha > /tmp/${name}.html

/etc/alternatives/x-www-browser /tmp/${name}.html &
# links /tmp/${name}.html
