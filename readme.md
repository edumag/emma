# Readme

![](./docs/img/emma.gif)

Development in python based on the [ccxt](https://github.com/ccxt/ccxt) library.

It allows us to quickly manage our trades in the different exchanges.

**Warning**

This project is under development, it is not recommended to use it in real mode.

## Features

It allows us to have an overview of the cryptocurrency market, showing us the state of the prices, the trend, the volume, the ADX and the MACD in five of the time periods that we choose.

Establish different stop trailing for each trade or strategy.

Set stoploss for each trade or strategy.

Configure different strategies for different temporalities. ([See strategies](./docs/strategies.md))

Real mode and SIMULA mode for testing.

Scripts that allow opening a terminal with tmux on a server.

## Telegram bot

Communication with telegram. (Notices and sending orders)


![](./docs/img/telegram_bot.png)


## Install

### App

```
git clone git@gitlab.com:edumag/emma.git
cd emma
pip install -r requirements.txt
```

### Dependencies

```
sudo apt install colorized-logs tmux ansi2txt python3-pytest
```

### Generate database

```
./scripts/init_db
```

### Setting

```
cp env.example .env
```

Edit .env file and adapt it with your data.

### Strategies

Import strategies.

```
./src/strategies.py -i all
```

Activate

```
./src/db.py -cfg activated_strategies:A_Z
```

## Starting emma

```
./scripts/emma
```
